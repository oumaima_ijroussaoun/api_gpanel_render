from pydantic import BaseModel
from typing import List, Optional
import datetime as d



##################################################################full foyer########################################################

class ContactFoyer(BaseModel):
    date_install_telfix :Optional[d.datetime]
    f_adr1 :str
    longitude :Optional[float]
    latitude:Optional[float]
    id_commune :int
    f_adr2 :Optional[str]


class Equipement(BaseModel):
    nbr_clim :int
    nbr_app_chauf :int
    nbr_refrig :int
    nbr_congel :int
    nbr_four_micro :int
    nbr_cuisiniere :int
    nbr_lave_linge :int
    nbr_lave_vaiss :int
    nbr_aspi :int
    poss_internet :int
    poss_tnt:bool
    source_electricite :int
    nbr_tv :int
    nbr_vehicule :int
    nbr_tel_mob :int
    nbr_tel_fix :int
    nbr_ordi :int
    nbr_radio :int
    nbr_ordi_emp :int
    nbr_home_cinema :int
    nbr_deco_sat :int
    nbr_console_jeux_se :int
    nbr_console_jeux_ae :int 
    nbr_tablette: Optional[int]

class Sociodemo(BaseModel):


    f_type_offre : int
    f_tranche_revenu : int
    type_hab : int
    standing_hab : int
    pos_res_sec : int 
    langue_prin : int
    stand_vie : int



###################################################chef de menage 
class ChefMenage(BaseModel):
    
    profession_cm :int
    csp_cm :int
    niv_diplome_cm:int
    
    class Config:
        orm_mode = True       
###################################################equipement poste
class EquipementPoste(BaseModel):
    
    
    decodeur_sat_num:int
    decodeur_adsl:int
    home_cinema:int
    antenne_parab:int
    type_antenne_parab:int
    reception_adsl:bool
    type_sonde_poste:int
    equipe_tnt:int
    class Config:
        orm_mode = True

###################################################reception

class Reception(BaseModel):
    
    
    
    type_reception:List[int]
    reception_chaine:List[int]
    reception_sat:List[int]
    class Config:
        orm_mode = True        


class GetAllFoyers(BaseModel):
    f_id_mediamat:int
    f_num_con: int
    f_nom: str
    f_etat: int
    f_csp: int
   
    f_date_deb: d.datetime
    
    mode_trait_f: int
    f_date_fin: Optional[d.datetime]

    class Config:
	    orm_mode=True


class GetAllIndividu(BaseModel):
    id_indiv: int
    nom: str
    prenom: str
    sexe: int
    etat: int
    is_cm: bool
    is_menagere: bool
    f_num_con: int
    age_calcl :int
    
    mode_trait_indiv:int

    class Config:
        orm_mode = True



class GetAllPoste(BaseModel):
    id_poste:int
    num_poste: int
    etat_actuel: int
    statut_poste: int
    mode_trait_poste: int
    tv_online: int
    f_num_con :int
    emplacement: int
    date_installation_poste :Optional[d.datetime]
    class Config:
        orm_mode = True


class CreateAudimetreInstalle(BaseModel):
    id_meter :Optional[str]
    sortie_audio :Optional[int] 
    capture_audio :Optional[int]
    nom_installateur :Optional[str]
    
    class Config:
        orm_mode = True

class Audimetre(BaseModel):
    
    
    statut_meter: int
    etat_meter:int
    emplacement_meter:Optional[int]
    
    class Config:
        orm_mode = True