from fastapi import FastAPI

from .gpanel_routers import include_route

from .gpanel_routers import gpanel_post_routes,gpanel_put_routes,gpanel_get_routes,gpanel_get_mapped,gpanel_delete_routes


gpanelsubapi =FastAPI()

include_route(gpanelsubapi)


    