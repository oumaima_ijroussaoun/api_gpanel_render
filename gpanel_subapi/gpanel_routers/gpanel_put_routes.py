from fastapi import APIRouter,HTTPException





from .. import GpanelExtraSchemas as extschem
from ...schemas import SchemaBaseFoyer as schemfoy
from ...schemas import SchemaBaseIndividu as schemind
from ...schemas import SchemaBaseAudimetre as schemau


from ...schemas import SchemaBasePoste as schempo

from ..gpanel_crud import gpanel_put_crud as crud


put_route=APIRouter(prefix="/PutRoute")


################################## update full foyer. my_tels:list[extschem.Tel],
@put_route.put("/FullFoyer/Update")
def update_full_foy(id_foy: int,Foyer:schemfoy.UpdateFoyer,ContactFoyer:extschem.ContactFoyer ,Equipement:extschem.Equipement,Sociodemo:extschem.Sociodemo):
    try:
        return crud.update_full_foyer(id_foy,Foyer,ContactFoyer,Equipement,Sociodemo)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    
  
@put_route.put("/postes/status/Update")
def update_poste(id_foy: int):
    try:
        return crud.update_state_postes(id_foy)
    except:
        raise 

################################## update indiv and cm  
@put_route.put("/IndivAndCm/Update")
def update_indiv_cm(id_ind: int, my_indiv:schemind.UpdateIndividu, my_cm:extschem.ChefMenage):
    try:
        return crud.update_indiv_cm(id_ind, my_indiv, my_cm)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    




################################# update poste
@put_route.put("/FullPoste/Update")
def update_full_poste(id_poste: int,my_updates:extschem.Audimetre , my_poste: schempo.CreatePoste, my_equipement_poste: extschem.EquipementPoste,my_reception: extschem.Reception,my_audimetre : schemau.CreateAudimetreInstalle):
    # try:
        return crud.update_full_poste(id_poste,my_updates, my_poste, my_equipement_poste,my_reception,my_audimetre)
    # except:
        # raise 
        # # HTTPException(status_code=400, detail="verifier les données saisies")    

  
#####################" update meter"    
@put_route.put("/Audimetre/Update")
def update_audimetre(id_meter: str, my_updates: extschem.Audimetre):
    try:
    
        return crud.update_audimetre(id_meter=id_meter,my_meter=my_updates)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    
