


def include_route(app):
    # app.include_router(api_call_route.gpanelroute)
    app.include_router(gpanel_post_routes.post_route)
    app.include_router(gpanel_get_routes.get_route)
    app.include_router(gpanel_get_mapped.get_route_mapped)
    app.include_router(gpanel_put_routes.put_route)
    app.include_router(gpanel_delete_routes.delete_route)