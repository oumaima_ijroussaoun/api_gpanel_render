from fastapi import APIRouter,HTTPException
from typing import List


from fastapi.responses import Response

from ..gpanel_crud import gpanel_delete_crud as crud
from .. import GpanelExtraSchemas as extschem


delete_route=APIRouter(prefix="/Delete")

@delete_route.delete("/Audimetre/installe/{id_meter}/{nom_installateur}",description='')
def delete_aud_installe(id_meter:str,nom_installateur:str,my_updates:extschem.Audimetre):
    try:
        return  crud.delete_audimetre_installe(id_meter,nom_inst=nom_installateur,my_updates=my_updates)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    



@delete_route.delete("/Audimetre/installe/foyer/{id_foyer}/{nom_installateur}",description='')
def delete_aud_installe(id_foyer:int,nom_installateur:str,my_updates:extschem.Audimetre):
    try:
        return  crud.delete_audimetre_installe_foy(id_foyer,nom_inst=nom_installateur,my_updates=my_updates)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    


@delete_route.delete("/Audimetre/installateur/{id_meter}",description='')
def delete_aud_installateur(id_meter:str,my_updates:extschem.Audimetre):
    try:
        return  crud.delete_audimetre_installateur(id_meter,my_updates=my_updates)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies") 

@delete_route.put("/Audimetre/installateur/multiple",description='')
def delete_aud_installateur(id_meters:List[str],my_updates:extschem.Audimetre):
    try:
        return  crud.delete_audimetre_installateur_multiple(id_meters,my_updates=my_updates)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies") 


@delete_route.delete("/ChefMenage/{id_indiv}",description='')
def delete_cm(id_indiv:int):
    try:
        return  crud.delete_cm(id_indiv)
    except:
        raise 