from typing import List
from ...schemas import SchemaBaseAudimetre as schemau
from fastapi import APIRouter, HTTPException




from .. import GpanelExtraSchemas as extschem
from ...schemas import SchemaBaseFoyer as schemfoy
from ...schemas import SchemaBaseIndividu as schemind


from ...schemas import SchemaBasePoste as schempo
from ..gpanel_crud import gpanel_post_crud as crud




post_route=APIRouter(prefix="/PostRoute")


###################### create full foyer 
@post_route.post("/FullFoyer/create")
def create_full_foy(my_foyer:schemfoy.CreateFoyer,my_contact_foyer:extschem.ContactFoyer ,my_equipement:extschem.Equipement,my_sociodemo:extschem.Sociodemo):
    try:
        return crud.create_full_foyer(my_foyer,my_contact_foyer,my_equipement,my_sociodemo)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    


###################### create indiv and cm 
@post_route.post("/IndivAndCm/create") 
def create_ind_cm(my_indiv:schemind.CreateIndividu,mycm:extschem.ChefMenage):
    try:
        return crud.create_indiv(my_ind=my_indiv,my_chef_menage=mycm)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    

###################### create full poste

@post_route.post("/FullPoste/create") 
def create_full_poste(my_poste: schempo.CreatePoste, my_equipement_poste: extschem.EquipementPoste,my_reception: extschem.Reception,my_audimetre : extschem.CreateAudimetreInstalle ):
    try:
        return crud.create_full_poste(my_poste=my_poste,my_equip=my_equipement_poste,my_recep=my_reception,my_audimetre=my_audimetre)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")        

@post_route.post("/AudimetreInstallateur/create") 
def create_audi_insta(id_meters:List[str],nom_installateur:str ):
    
    try:
        return crud.create_audimetre_inst(id_meters,nom_installateur)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")        

@post_route.post("/PanelInstallateur/create") 
def panel_inst(id_meters:List[str],nom_installateur:str ):
    
    try:
        return crud.panel_installateur(id_meters,nom_installateur)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    