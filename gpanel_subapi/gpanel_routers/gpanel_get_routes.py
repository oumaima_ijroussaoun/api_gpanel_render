from fastapi import APIRouter,Query
from typing import List


from fastapi.responses import JSONResponse

from ..gpanel_crud import gpanel_get_crud as crud
from .. import GpanelExtraSchemas as extshem


get_route=APIRouter(prefix="/GetRoute")



##################### get var and modalite 

@get_route.get("/Modalite/get")
def get_modalite(var_name):
    return crud.get_var_modalite(var_name)

@get_route.get("/Modalite/all/get")
def get_modalite():
    return crud.get_all_modalite()    

################################## get full foyer
@get_route.get("/FullFoyer/get")
def get_full_foyer(id_foy:int):
    
    return crud.get_full_foy(id_foy=id_foy) 
################################## get full foyer details
@get_route.get("/FullFoyer/details/get")
def get_full_foyer_details(id_foy:int):
    return crud.get_full_foy_details(id_foy=id_foy)     

####### Zakaria ##############
@get_route.get("/FullFoyer/getFoyerByMediamat")
def get_foyers(mediamat:int):
    return crud.get_foyer(mediamat=mediamat)

@get_route.get("/Indiv/zakaria/get")
def get_indi_anomalie(ids_ind:List[str]=Query(..., alias="ids_ind[]")):
    return crud.get_indiv_anoma(ids_ind) 

@get_route.get("/FullFoyer/zakaria/get")
def get_full_foy_zakaria(id_foy:int):
    return crud.get_full_foy_zakaria(id_foy=id_foy) 


############################## get indiv and cm
@get_route.get("/IndivAndCm/get")
def get_indiv_cm(id_ind:int):
    return crud.get_indiv_cm(id_ind=id_ind)   

################################### get full poste
@get_route.get("/FullPoste/get")
def get_full_poste(id_poste:int):
    return crud.get_full_poste(id_poste=id_poste)   


@get_route.get("/Foyer/all/get/", response_model=List[extshem.GetAllFoyers])
def read_foyers():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_foyers()

@get_route.get("/Individu/all/get/", response_model=List[extshem.GetAllIndividu])

def read_individus():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_individus()


@get_route.get("/Poste/all/get/", response_model=List[extshem.GetAllPoste])

def read_postes():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_postes()


@get_route.get("/count/get")
def get_count():
    return crud.get_count()

@get_route.get("/dashboard/foyer/get")
def get_dashboard_foyer_count():
    return crud.get_count_foy()

@get_route.get("/dashboard/individu/get")
def get_dashboard_ind_count():
    return crud.get_count_ind()




    #,crud.get_count_niv_dip(),crud.get_count_category() 
##############################################""commune#######################################""

@get_route.get("/Commune/get/all",description= "lecture des donn��es de toutes les communes")
def read_communes():
    return crud.get_all_communes()



@get_route.get("/Individu/get/id/touche",description= "recuperation du prochain id individu")
def read_id_indiv(fnco:int=0):
    return crud.get_id_indiv(fnco=fnco)

@get_route.get("/Poste/get/id",description= "recuperation du prochain id poste")
def read_id_poste(id_foy:int):
    return crud.get_poste_id(id_foy=id_foy)


########## installateurs
@get_route.get("/Installateurs/get",description= "recuperation de la liste des installateurs")
def read_installateurs():
    return crud.get_installateur()

#@get_route.get("/test/get/{id_foy}",description= "test")
#def test(id_foy :int):
 #   return crud.get_audimetre_installe_foy( id_foy)



########## meters
@get_route.get("/meter/get",description= "recuperation d'un audimetre")
def read_meters(id_meter:str):
    return crud.get_audimetre(id_meter=id_meter)
@get_route.get("/meters/get",description= "recuperation de la liste des meters")
def read_meters():
    return crud.get_meters_modalite()

@get_route.get("/imei/get",description= "recuperation de la liste des meters imei")
def read_meters():
    return crud.get_imei_modalite()

@get_route.get("/imei_inst/get",description= "recuperation de la liste des meters imei des installateurs")
def read_meters():
    return crud.get_imei_inst_modalite()
@get_route.get("/meters_inst/get",description= "recuperation de la liste des meters des installateurs")
def read_inst_meters():
    return crud.get_meters_inst_modalite()    

@get_route.get("/meters/panel/get",description= "recuperation de la liste des meters desinstale panel")
def read_meters():
    return crud.get_audimetre_panel_inst()

########## villes
@get_route.get("/Ville/get",description= "recuperation de la liste des villes")
def read_ville():
    return crud.get_villes()
