from fastapi import APIRouter
from typing import List


from fastapi.responses import Response

from ..gpanel_crud import gpanel_get_crud as crud
from .. import GpanelExtraSchemas as extshem


get_route_mapped=APIRouter(prefix="/GetRoute/mapped")

#--------------------------------------------------------------------------------- mapped---------------------------------------------------------------

################################## get full foyer details
@get_route_mapped.get("/FullFoyer/details/get",description='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
def get_full_foyer_details(id_foy:int):
    
    return  crud.get_full_foy_mapped(id_foy=id_foy) 


################################# get full foyer details zakaria
@get_route_mapped.get("/FullFoyer/details/zakaria/get/indiv",description='zakaria')
def get_full_foyer_details_zakaria(id_foy:int):
    
    return  crud.get_full_foy_mapped_zakaria(id_foy=id_foy)

@get_route_mapped.get("/FullFoyer/details/zakaria/get/indiv/fnum",description='zakaria')
def get_full_foyer_details_zakaria(id_foy:int):
    
    return  crud.get_full_foy_mapped_zakaria_fnum(id_foy=id_foy)

@get_route_mapped.get("/FullFoyer/details/zakaria/get",description='zakaria')
def get_full_foyer_details_zakaria(id_foy:int):
    
    return  crud.get_foyer_zakaria(id_foy=id_foy) 

@get_route_mapped.get("installateur/commune/get")    
def get_installateur_by_commune():
    return crud.get_installateur_commune()

############################## get indiv and cm
@get_route_mapped.get("/IndivAndCm/get")
def get_indiv_cm(id_ind:int):
    return  crud.get_indiv_cm_mapped(id_ind=id_ind)

################################### get full poste
@get_route_mapped.get("/FullPoste/get")
def get_full_poste(id_poste:int):
    return   crud.get_full_poste_mapped(id_poste=id_poste) 






@get_route_mapped.get("/Foyer/all/get/")
def read_foyers():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return  crud.get_foyers_mapped()

@get_route_mapped.get("/Foyer/incomplets/all/get/")
def read_foyers():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return  crud.get_inc_foyers_mapped()   

@get_route_mapped.get("/Individu/all/get/")

def read_individus():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_individus_mapped()

#update zakaria
@get_route_mapped.get("/Individu/all/appel/get/")

def read_individus_appel():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_individus_mapped_appel()


@get_route_mapped.get("/Poste/all/get/")

def read_postes():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_postes_mapped()


@get_route_mapped.get("/Audimetre/all/get/")

def read_aud():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_aud_mapped()

@get_route_mapped.get("/AudimetreInstalle/all/get/")

def read_aud():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_aud_installe_mapped()    


@get_route_mapped.get("/AudimetreInstallateur/all/get/")

def read_aud():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_aud_installateur_mapped()

@get_route_mapped.get("/Audimetre/Installateur/get/")

def get_audimetre_par_installateur(inst:str):

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_audimetre_par_inst(inst)

############################################ Updated by oumaima 19/09/2023 ########################################################
@get_route_mapped.get("/Audimetre/Installateur/all/get/")

def get_audimetre_par_all_installateur(inst:str):

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_audimetre_par_inst_all(inst)
###################################################################################################################################
@get_route_mapped.get("/AudimetreStock/all/get/")

def read_aud_stock():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
    return crud.get_audimetre_stock_mapped()    

@get_route_mapped.get("/AudimetreGlobInstallateur/all/get/")
def read_aud_glob_inst():

    # db_foyer = bfcrud.get_foyer_foyers()
    # if db_foyer is None:
    #     raise HTTPException(status_code=404, detail="foyers not found")
 

    return crud.get_audimetre_glob_installateur_mapped()        

@get_route_mapped.get("/meters/inst_panel/get",description= "recuperation de la liste des meters desinstale panel")
def read_meters():
    return crud.get_audimetre_panel_inst_mapped()




 ################################## get full foyer panel details
@get_route_mapped.get("/FullFoyer/panel/get")
def get_full_foyer_details_panel():
    return Response(content=crud.get_foyers_panel() , media_type="text/csv")
 

    

############################## get indiv and cm
@get_route_mapped.get("/IndivAndCm/panel/get")
def get_indiv_panel():
    return Response(content=crud.get_indivs_panel(), media_type="text/csv")

    
################################### get full poste
@get_route_mapped.get("/FullPoste/panel/get")
def get_full_poste_panel():
    return Response(content=crud.get_postes_panel(),  media_type="text/csv")



@get_route_mapped.get("/Mouvements/get")
def get_mouvements():
    return crud.get_mouvement()    
