
from ...schemas import SchemaBaseAudimetre
from sqlalchemy.orm import Session


from ...refrence_subapi.crud import  CrudChefMenage as crudcm
from ...refrence_subapi.crud import CrudBaseIndividu as  crudind
from ...refrence_subapi.crud import CrudBaseFoyer as bfcrud 
from ...refrence_subapi.crud import CrudBasePoste as bpcrud 
from ...refrence_subapi.crud import CrudBaseAudimetre as bacrud 

from . import gpanel_get_crud as getcrud
from . import gpanel_delete_crud as delcrud


from ...models import ModelBaseAudimetre as modau
from ...models import ModelChefMenage as modcm
from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseCommune as modco

from ...schemas import SchemaBaseFoyer as schemfoy
from ...schemas import SchemaBaseIndividu as schemind
from ...schemas import SchemaBasePoste as schempo

from .. import GpanelExtraSchemas as extschem

from . import get_db

#####################################################update full foyer########################## 
def update_full_foyer(id_foy: int,my_foyer:schemfoy.UpdateFoyer,my_contact_foyer:extschem.ContactFoyer ,my_equipement:extschem.Equipement,my_sociodemo:extschem.Sociodemo,db: Session = next(get_db())):
     try:
          
          stored_foyer = bfcrud.get_foy(db=db, id_foy=id_foy)
          print(stored_foyer)
          for key, value in my_foyer:
               setattr(stored_foyer, key, value)
          db.flush()
          
          #########
          stored_contact_foyer = bfcrud.get_contact_foy(db=db, id_foy=id_foy)
          for key, value in my_contact_foyer:
               setattr(stored_contact_foyer, key, value)
          db.flush()
          
          #########
          stored_equipement = bfcrud.get_equipement(db=db, id_foy=id_foy)
          for key, value in my_equipement:
               setattr(stored_equipement, key, value)
          db.flush()
          
          #########
          stored_sociodemo = bfcrud.get_sociodemo(db=db, id_foy=id_foy)
          for key, value in my_sociodemo:
               setattr(stored_sociodemo, key, value)
          db.flush()
          
          #########
     except:
         db.rollback()
         db.close()
         raise
     else:

          db.commit()
          return 'updated successfully'



##############################################################update indiiiv

def update_indiv_cm(id_ind: int, my_indiv:schemind.UpdateIndividu, my_cm:extschem.ChefMenage,db: Session = next(get_db())):
     try:
          stored_individu = crudind.get_indiv(db=db,id_ind=id_ind)
          for key, value in my_indiv:
               setattr(stored_individu, key, value)
          db.flush()
          db.refresh(stored_individu)

          stored_chef_menage = crudcm.get_chef_menage(db=db,id_ind=id_ind)
          if(stored_chef_menage!=None):
               for key, value in my_cm:
                    setattr(stored_chef_menage, key, value)
                       

          else:
              db_chef_menage = modcm.ChefMenage(id_indiv=my_indiv.id_indiv,
               profession_cm =my_cm.profession_cm,
               csp_cm =my_cm.csp_cm,niv_diplome_cm=my_cm.niv_diplome_cm)
              db.add(db_chef_menage)
              db.flush()
              db.refresh(db_chef_menage)           
     except:
          db.rollback()
          db.close()
          raise
     else:          
          db.commit()
          return  'updated successfully'
    



############################################# update full poste

def update_audimetre(id_meter: str, my_meter:extschem.Audimetre,db: Session = next(get_db())):
     try:
        
                    
          stored_meter = getcrud.get_audimetre(id_meter,db=db)
          if (stored_meter.etat_meter==3):
                    for key, value in {"statut_meter": stored_meter.statut_meter,"etat_meter": 3,'emplacement_meter':my_meter.emplacement_meter}.items():
                         setattr(stored_meter, key, value)
          else:               
               for key, value in my_meter:
                    setattr(stored_meter, key, value)
     except:
          db.rollback()
          raise
     else:          
          db.commit()
          return  'updated successfully'


def update_full_poste(id_poste:int,my_updates:extschem.Audimetre ,my_poste: schempo.CreatePoste, my_equip: extschem.EquipementPoste,my_recep: extschem.Reception,my_audimetre:extschem.CreateAudimetreInstalle, db: Session = next(get_db())):
     try:
          stored_poste = bpcrud.get_poste(id_poste=id_poste,db=db)
          for key, value in my_poste:
               setattr(stored_poste, key, value)
          db.flush()
          db.refresh(stored_poste)
          stored_equip = bpcrud.get_equipement_poste(id_poste=id_poste,db=db)
          for key, value in my_equip:
               setattr(stored_equip, key, value)
          db.flush()
          db.refresh(stored_equip)

          stored_recep = bpcrud.get_reception(id_poste=id_poste,db=db)
          for key, value in my_recep:
               setattr(stored_recep, key, value)
          db.flush()
          db.refresh(stored_recep)
          if (my_audimetre.nom_installateur!=None)   :  
               stored_audimetre_inst = bacrud.get_audimetre_installe_by_id_poste(id_poste=id_poste,db=db)
               stored_meter = getcrud.get_audimetre(stored_audimetre_inst.id_meter,db=db)

               db_audimetre_inst =  modau.AudimetreInstallateur(id_meter=stored_meter.id_meter,nom_installateur=my_audimetre.nom_installateur)
               db.add(db_audimetre_inst)
               db.flush()
               db.refresh(db_audimetre_inst)

               stored_meter_update_old = getcrud.get_audimetre(stored_meter.id_meter,db=db)
               for key, value in my_updates:
                    setattr(stored_meter_update_old, key, value)
               db.flush()
               db.refresh(stored_meter_update_old)
               

               del_meter_installe=getcrud.get_audimetre_installe(db=db ,id_meter=stored_meter.id_meter)
               db.delete(del_meter_installe)
               db.flush()

               db_audimetre = modau.AudimetreInstalle(id_poste=my_poste.id_poste,id_meter=my_audimetre.id_meter,sortie_audio=my_audimetre.sortie_audio,capture_audio=my_audimetre.capture_audio,nom_installateur=my_audimetre.nom_installateur)
               db.add(db_audimetre)
               db.flush()
               db.refresh(db_audimetre)

               stored_meter_update_new = getcrud.get_audimetre(my_audimetre.id_meter,db=db)
               if (stored_meter_update_new.etat_meter==3):
                    for key, value in {"statut_meter":stored_meter_update_new.statut_meter ,"etat_meter": 3,"emplacement_meter": 1}.items():
                         setattr(stored_meter_update_new, key, value)
                    db.flush()
                    db.refresh(stored_meter_update_new)
               else:
                    for key, value in {"statut_meter": 1,"etat_meter": 1,"emplacement_meter": 1}.items():
                         setattr(stored_meter_update_new, key, value)
                    db.flush()
                    db.refresh(stored_meter_update_new)     

               del_meter_installateur=getcrud.get_audimetre_installateur(db=db ,id_meter=my_audimetre.id_meter)
               db.delete(del_meter_installateur)
               db.flush()
     except:
          db.rollback()
          db.close()
          raise
     else:     
          db.commit()
          
          
     


          
          return  'update done'
def update_state_postes(id_foy:int, db: Session = next(get_db())):
     try:
          stored_poste = bpcrud.get_postes(id_foy=id_foy,db=db)
          for poste in stored_poste:
               for key, value in {'etat_actuel':3,'statut_poste':3}.items():
                         setattr(poste, key, value)
               db.flush()
              
     except:
          db.rollback()
          db.close()
          raise
     else:     
          db.commit()







#############################################database update ##########################################################

def update_database(db: Session = next(get_db())):
     try:
          
          db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
          db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
          db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
          db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
          db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
          db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
          db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
          db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
          db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
          db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
     except:
          db.rollback()
          db.close()
          raise
     else:
          db.commit()     
          db.close()

def terminate_idle_conn(db: Session = next(get_db())):
	try:
		statement="""SELECT pg_terminate_backend(pid)
				FROM pg_stat_activity
				WHERE datname = 'panda_2'
				AND pid <> pg_backend_pid()
				AND state in ('idle', 'idle in transaction (aborted)', 'disabled') 
				AND state_change < current_timestamp - INTERVAL '5' MINUTE;"""
		
	except:
		db.rollback()
		db.close()
		raise
	else:
		db.execute(statement)
		print("ooooook")

	