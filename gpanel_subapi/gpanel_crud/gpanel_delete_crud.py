
from typing import List
from sqlalchemy.orm import Session 
from sqlalchemy import func 
import pandas as pd
from sqlalchemy import desc

from sqlalchemy.sql.expression import  false, or_, true

from ...models import ModelBaseIndividu, ModelChefMenage

from ...models import ModelBaseAudimetre as modau
from . import gpanel_get_crud as getcrud
from . import gpanel_post_crud as postcrud
from . import gpanel_put_crud as putcrud
from ...refrence_subapi.crud import CrudBasePoste as bpcrud

from .. import GpanelExtraSchemas as extschem

import datetime




from . import get_db






def delete_audimetre_installe(id_meter: str,nom_inst:str,my_updates:extschem.Audimetre ,db: Session = next(get_db())):
    try:
        stored_poste=getcrud.get_poste_by_meter(id_meter=id_meter)
        
        for key, value in {'statut_poste':3,'etat_actuel':3}.items():
               setattr(stored_poste, key, value)
        db.flush()
        
        putcrud.update_audimetre(id_meter,my_updates,db=db)
        postcrud.create_audimetre_inst([id_meter],nom_inst,db=db)
        del_meter_installe=getcrud.get_audimetre_installe(db=db ,id_meter=id_meter)
        db.delete(del_meter_installe)
    except:
        db.rollback()
        db.close()
        raise 
    else:   
        db.commit()
        
        return "meter deleted"
    
   
    

    

def delete_audimetre_installe_foy(id_foy: int,nom_inst:str,my_updates:extschem.Audimetre ,db: Session = next(get_db())):
    
    del_meter_installe=getcrud.get_audimetre_installe_foy(db=db ,id_foy=id_foy)
    for meter in del_meter_installe:
        delete_audimetre_installe(meter.id_meter,nom_inst=nom_inst,my_updates=my_updates,db=db)
   
    

    return "meters deleted"    



def delete_audimetre_installateur(id_meter: str,my_updates:extschem.Audimetre ,db: Session = next(get_db())):
    try:
        putcrud.update_audimetre(id_meter,my_updates,db=db)
        del_meter_installateur=getcrud.get_audimetre_installateur(db=db ,id_meter=id_meter)
        db.delete(del_meter_installateur)
    except:
        db.rollback()
        db.close()
        raise 
    else:   
        db.commit()
        
        return "meter deleted from installer"
    
   
    

   


def delete_audimetre_installateur_multiple(id_meters: List[str],my_updates:extschem.Audimetre ,db: Session = next(get_db())):
    for id_meter in id_meters :
      delete_audimetre_installateur(id_meter,my_updates,db)

# def delete_audimetre_installateur_multiple(id_meters: list[str],my_updates:extschem.Audimetre ,db: Session = next(get_db())):
#     for id_meter in id_meters :
#         del_meter_installateur=getcrud.get_audimetre_installateur(db=db ,id_meter=id_meter)
#         db.delete(del_meter_installateur)
#         db.flush()
#         putcrud.update_audimetre(id_meter,my_updates,db=db)
   
    

#     return "meters deleted from installer"

def delete_cm(id_indiv: int , db:Session=next(get_db())):
    try:
        db.query(ModelBaseIndividu.Individu).filter(ModelBaseIndividu.Individu.id_indiv==id_indiv).update({'is_cm':0})
        db.query(ModelChefMenage.ChefMenage).filter(ModelChefMenage.ChefMenage.id_indiv==id_indiv).delete()
       
    except:
        db.rollback()
        db.close()
        raise
    else:
        db.commit()
        return 'cm deleted'    