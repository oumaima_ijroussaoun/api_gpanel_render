
from ast import Return
from sqlalchemy.orm import Session, query, session 
from sqlalchemy import func 
import pandas as pd
from sqlalchemy import desc

from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String

from ...models import ModelModalite as mod
from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelChefMenage as modcm
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseCommune as modco
from ...models import ModelTrancheAge as modtr
from ...models import ModelCroisementModaliteAgeSexe as modcmas

from ...models import ModelBaseAudimetre as modau

import datetime




from . import get_db

def get_installateur_commune(db: Session = next(get_db())):
    installateur =  db.query(modco.InstallateursCommune).all()

    return installateur

def map_modalite(db: Session = next(get_db())):
    modalite_df =  pd.read_sql(db.query(mod.Modalite.nom_var,mod.Modalite.num_modalite,mod.Modalite.label_modalite).statement,db.bind)
    modalite_dict = {k: val.groupby('num_modalite')['label_modalite'].apply(lambda x: x.to_string(index = False)).to_dict()
     for k, val in modalite_df.groupby('nom_var')}
    db.close()
    return modalite_dict

################################get var and modalite

def get_var_modalite(nom_var: str,db: Session = next(get_db())):
    return db.query(mod.Modalite).filter(mod.Modalite.nom_var == nom_var).all()

def get_all_modalite(db: Session = next(get_db())):
    return db.query(mod.Modalite).all()

###########################################"commune#######################""
def get_all_communes(db: Session = next(get_db())):
    return db.query(modco.Commune.id_commune.label('num_modalite'),modco.Commune.nom_commune.label('label_modalite')).all()

  ################################get_full foyer#################################
def get_full_foy(id_foy: int,db: Session = next(get_db())):
    
    return db.query(modfoy.Foyer,modfoy.ContactFoyer,modfoy.Sociodemo,modfoy.Equipement).outerjoin(modfoy.ContactFoyer).outerjoin(modfoy.Sociodemo).outerjoin(modfoy.Equipement).filter(modfoy.Foyer.f_num_con == id_foy).first()
################################get_full_foyer_details#################################
def get_full_foy_details(id_foy: int,db: Session = next(get_db())):
    return db.query(modfoy.Foyer,modfoy.ContactFoyer,modfoy.Sociodemo,modfoy.Equipement,modfoy.EffectifFoyer).outerjoin(modfoy.ContactFoyer).outerjoin(modfoy.Sociodemo).outerjoin(modfoy.Equipement).outerjoin(modfoy.EffectifFoyer).filter(modfoy.Foyer.f_num_con == id_foy).first()
##### Updated By ZAKARIA ##########################################################################################
def get_foyer(mediamat: int,db: Session = next(get_db())):
    return db.query(modfoy.Foyer,modfoy.ContactFoyer,modfoy.Sociodemo,modfoy.Equipement,modfoy.EffectifFoyer,modco.Ville).outerjoin(modfoy.ContactFoyer).outerjoin(modfoy.Sociodemo).outerjoin(modfoy.Equipement).outerjoin(modfoy.EffectifFoyer).outerjoin(modco.Ville,modco.Ville.id_commune == modfoy.ContactFoyer.id_commune).filter(modfoy.Foyer.f_id_mediamat == mediamat).first()


def get_foyer_zakaria(id_foy: int,db: Session = next(get_db())):
    return db.query(modfoy.Foyer,modfoy.ContactFoyer,modfoy.Sociodemo,modfoy.Equipement,modfoy.EffectifFoyer,modco.Ville).outerjoin(modfoy.ContactFoyer).outerjoin(modfoy.Sociodemo).outerjoin(modfoy.Equipement).outerjoin(modfoy.EffectifFoyer).outerjoin(modco.Ville,modco.Ville.id_commune == modfoy.ContactFoyer.id_commune).filter(modfoy.Foyer.f_num_con == id_foy).first()
###################### full foyer by zakaria #####################################
def get_full_foy_zakaria(id_foy: int,db: Session = next(get_db())):
    return db.query(modfoy.Foyer,modfoy.ContactFoyer,modfoy.Sociodemo,modfoy.Equipement,modfoy.EffectifFoyer).outerjoin(modfoy.ContactFoyer).outerjoin(modfoy.Sociodemo).outerjoin(modfoy.Equipement).outerjoin(modfoy.EffectifFoyer).filter(modfoy.Foyer.f_num_con == id_foy).first()




def get_indiv_anoma(ids_ind:list,db: Session = next(get_db())):
    
    return db.query(modind.Individu,modcm.ChefMenage).outerjoin(modcm.ChefMenage,).filter(modind.Individu.id_indiv.in_(ids_ind)).all()
###################################################################################################################

##################################################get indiv and cm
def get_indiv_cm(id_ind: int,db: Session = next(get_db())):
    return db.query(modind.Individu,modcm.ChefMenage).outerjoin(modcm.ChefMenage,).filter(modind.Individu.id_indiv == id_ind).first()
    
############################################# get full poste
def get_full_poste(id_poste: int,db: Session = next(get_db())):
    return db.query(modpo.Poste,modpo.EquipementPoste,modpo.Reception,modau.AudimetreInstalle).outerjoin(modpo.EquipementPoste).outerjoin(modau.AudimetreInstalle).outerjoin(modpo.Reception).filter(modpo.Poste.id_poste == id_poste).first()        

# recuperation de tous les foyers
    
def get_foyers(db: Session = next(get_db())):
    return db.query(modfoy.Foyer).all()


# recuperation de tous les individus

def get_individus(db: Session = next(get_db())):
    return db.query(modind.Individu).all()

# recuperation de tous les postes

def get_postes(db: Session = next(get_db())):
    return db.query(modpo.Poste).all()


#######################################mapped variables modalites

##### full foyer mapped
def get_full_foy_mapped(id_foy: int,db: Session = next(get_db())):
    
    foy_df= pd.read_sql( db.query(modfoy.Foyer).statement.filter(modfoy.Foyer.f_num_con == id_foy),db.bind)
    contact_foy_df= pd.read_sql( db.query(modfoy.ContactFoyer,modco.Commune,modco.Region,modco.Ville).statement.\
        outerjoin(modfoy.Foyer).join(modco.Commune).join(modco.Region).join(modco.Ville).filter(modfoy.Foyer.f_num_con == id_foy),db.bind)
    sociodemo_df= pd.read_sql( db.query(modfoy.Sociodemo).statement.outerjoin(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foy),db.bind)
    equipement_df= pd.read_sql( db.query(modfoy.Equipement).statement.outerjoin(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foy),db.bind)
    effec_foy_df= pd.read_sql( db.query(modfoy.EffectifFoyer).statement.outerjoin(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foy),db.bind)

    indiv_df = pd.read_sql( db.query(modind.Individu).statement.filter(modind.Individu.f_num_con == id_foy),db.bind)
    poste_df = pd.read_sql( db.query(modpo.Poste).statement.filter(modpo.Poste.f_num_con == id_foy),db.bind)


    modalite_dict = map_modalite()
    

    foy_df = foy_df.replace(modalite_dict).fillna('')
    contact_foy_df = contact_foy_df.replace(modalite_dict).fillna('')
    sociodemo_df = sociodemo_df.replace(modalite_dict).fillna('')
    equipement_df = equipement_df.replace(modalite_dict).fillna('')
    effec_foy_df = effec_foy_df.replace(modalite_dict).fillna('')
    indiv_df['is_cm']=indiv_df['is_cm'].astype(int)
    indiv_df = indiv_df.replace(modalite_dict).fillna('')
    poste_df = poste_df.replace(modalite_dict).fillna('')

    indiv_df.sort_values(by='touche_indiv',inplace=True)
    poste_df.sort_values(by='num_poste',inplace=True)

    tables_dict={}

    tables_dict['Foyer'] = foy_df.to_dict("records")[0]
    tables_dict['ContactFoyer'] = contact_foy_df.to_dict("records")[0]
    tables_dict['Equipement'] = equipement_df.to_dict("records")[0]
    tables_dict['EffectifFoyer'] = effec_foy_df.to_dict("records")[0]
    tables_dict['Sociodemo'] = sociodemo_df.to_dict("records")[0]
    tables_dict['Individu'] = indiv_df.to_dict("records")
    
    tables_dict['Poste'] = poste_df.to_dict("records")
    db.close()
    return tables_dict


##### full foyer mapped by zakaria
def get_full_foy_mapped_zakaria(id_foy: int,db: Session = next(get_db())):
    return db.query(modind.Individu).outerjoin(modfoy.Foyer).filter(modfoy.Foyer.f_id_mediamat == id_foy).order_by(modind.Individu.touche_indiv).all()


def get_full_foy_mapped_zakaria_fnum(id_foy: int,db: Session = next(get_db())):
    return db.query(modind.Individu).outerjoin(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foy).order_by(modind.Individu.touche_indiv).all()

######## indiv and cm mapped
def get_indiv_cm_mapped(id_ind: int,db: Session = next(get_db())):
    indiv_df = pd.read_sql( db.query(modind.Individu).statement.filter(modind.Individu.id_indiv == id_ind),db.bind)
    cm_df = pd.read_sql(db.query(modcm.ChefMenage).statement.outerjoin(modind.Individu.f_num_con).filter(modind.Individu.id_indiv == id_ind),db.bind)

    modalite_dict = map_modalite()
    
    indiv_df = indiv_df.replace(modalite_dict).fillna('') 
    cm_df = cm_df.replace(modalite_dict).fillna('') 

    tables_dict={}
    tables_dict['Individu'] = indiv_df.to_dict("records")[0]
    if (tables_dict['Individu']['is_cm'] == 1):
         tables_dict['ChefMenage'] = cm_df.to_dict("records")[0]
    else : tables_dict['ChefMenage'] = {}    

    db.close()
    return tables_dict

############################################# get full poste mapped
def get_full_poste_mapped(id_poste: int,db: Session = next(get_db())):
    poste_df= pd.read_sql( db.query(modpo.Poste).statement.filter(modpo.Poste.id_poste == id_poste) ,db.bind)

    equipement_poste_df= pd.read_sql( db.query(modpo.EquipementPoste).statement.\
        join(modpo.Poste).filter(modpo.Poste.id_poste == id_poste) ,db.bind)

    reception_df= pd.read_sql( db.query(modpo.Reception).statement.\
        join(modpo.Poste).filter(modpo.Poste.id_poste == id_poste) ,db.bind)       

    audimetre_df= pd.read_sql( db.query(modau.AudimetreInstalle).statement.\
        filter(modau.AudimetreInstalle.id_poste == id_poste) ,db.bind)     

    modalite_dict = map_modalite()

    poste_df = poste_df.replace(modalite_dict).fillna('') 
    equipement_poste_df = equipement_poste_df.replace(modalite_dict).fillna('')  
    reception_df = reception_df.replace(modalite_dict).fillna('')  
    audimetre_df = audimetre_df.replace(modalite_dict).fillna('')

    tables_dict={}
    tables_dict['Poste'] = poste_df.to_dict("records")[0]
    tables_dict['EquipementPoste'] = equipement_poste_df.to_dict("records")[0]
    tables_dict['Reception'] = reception_df.to_dict("records")[0]
    tables_dict['Audimetre_installe'] = audimetre_df.to_dict("records")[0]
     
    db.close()
     
    return tables_dict    

# recuperation de tous les foyers mapped 

def get_foyers_mapped(db: Session = next(get_db())):
    foy_df = pd.read_sql(db.query(modfoy.Foyer,modcm.ChefMenage.csp_cm).order_by(desc(modfoy.Foyer.f_date_deb)).statement.outerjoin(modcm.ChefMenage.ind).outerjoin(modind.Individu.foyer) ,db.bind)

        # .order_by(desc(modfoy.Foyer.f_date_deb))
  
    modalite_dict = map_modalite()

    foy_df = foy_df.replace(modalite_dict).fillna('') 
    db.close()
    return foy_df.to_dict("records")
# recuperation des foyer incomplets

def get_inc_foyers_mapped(limite: datetime.date = datetime.date(2018, 1, 1),db: Session = next(get_db())):
    foy_df = pd.read_sql(db.query(modfoy.Foyer,modcm.ChefMenage.csp_cm).statement.outerjoin(modind.Individu,modind.Individu.f_num_con==modfoy.Foyer.f_num_con).outerjoin(modcm.ChefMenage,modcm.ChefMenage.id_indiv==modind.Individu.id_indiv).filter(modind.Individu.id_indiv==None).filter(or_(modfoy.Foyer.f_date_fin > limite\
        ,modfoy.Foyer.f_etat==1,modfoy.Foyer.f_etat==2,modfoy.Foyer.f_etat==3,modfoy.Foyer.f_etat==4)) ,db.bind)
  
    modalite_dict = map_modalite()

    foy_df = foy_df.replace(modalite_dict).fillna('') 
    db.close()
    return foy_df.to_dict("records")
# recuperation de tous les individus mapped

def get_individus_mapped(db: Session = next(get_db())):
    indiv_df = pd.read_sql( db.query(modind.Individu).order_by(desc(modind.Individu.id_indiv)).statement.join(modfoy.Foyer).filter(or_(modfoy.Foyer.f_date_fin > datetime.date(2018, 1, 1)\
        ,modfoy.Foyer.f_etat==1,modfoy.Foyer.f_etat==2,modfoy.Foyer.f_etat==3,modfoy.Foyer.f_etat==4)) ,db.bind) 

        # .order_by(desc(modind.Individu.id_indiv))  

    modalite_dict = map_modalite()

    indiv_df = indiv_df.replace(modalite_dict).fillna('') 
    db.close()
    return indiv_df.to_dict("records")


#update zakaria
def get_individus_mapped_appel(db: Session = next(get_db())):
    #.f_num_con,modind.Individu.id_indiv,modind.Individu.age_calcl,modind.Individu.prenom,modind.Individu.touche_indiv
    indiv_df = db.query(modind.Individu.f_num_con,modind.Individu.id_indiv,modind.Individu.age_calcl,modind.Individu.prenom,modind.Individu.touche_indiv,modfoy.Foyer.f_id_mediamat).outerjoin(modfoy.Foyer).order_by(modind.Individu.touche_indiv).all()

        # .order_by(desc(modind.Individu.id_indiv))  

    #modalite_dict = map_modalite()

    #indiv_df = indiv_df.replace(modalite_dict).fillna('') 
    db.close()
    return indiv_df

# recuperation de tous les postes mapped 

def get_postes_mapped(db: Session = next(get_db())):
    
    poste_df= pd.read_sql( db.query(modpo.Poste,modau.AudimetreInstalle).order_by(desc(modpo.Poste.date_installation_poste)).statement.outerjoin(modau.AudimetreInstalle).outerjoin(modfoy.Foyer).filter(or_(modfoy.Foyer.f_date_fin > datetime.date(2018, 1, 1)\
        ,modfoy.Foyer.f_etat==1,modfoy.Foyer.f_etat==2,modfoy.Foyer.f_etat==3,modfoy.Foyer.f_etat==4)),db.bind)
        # .order_by(desc(modpo.Poste.date_installation_poste))
    modalite_dict = map_modalite()   

    poste_df = poste_df.replace(modalite_dict).fillna('') 
    db.close()
    return poste_df.to_dict("records")
    
# recuperation de tous les audimetres mapped 

def get_aud_mapped(db: Session = next(get_db())):
    
    aud_df= pd.read_sql( db.query(modau.Audimetre,case([modau.AudimetreInstalle.id_meter!=None , func.left(modau.AudimetreInstalle.id_poste.cast(String),-2)],[modau.AudimetreInstallateur.id_meter!= None,modau.AudimetreInstallateur.nom_installateur],else_='5').label('id_emplacement')).statement.outerjoin(modau.AudimetreInstallateur).outerjoin(modau.AudimetreInstalle) ,db.bind)
        # .order_by(desc(modpo.Poste.date_installation_poste))
    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()
    return aud_df.to_dict("records")

# recuperation de tous les audimetres en stock mapped 

def get_audimetre_stock_mapped(db: Session=next(get_db())):
    aud_df= pd.read_sql( db.query(modau.Audimetre).statement.filter(and_(modau.Audimetre.emplacement_meter == 3)) ,db.bind)

    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()
    return aud_df.to_dict("records")

# recuperation de tous les audimetres chez installateur mapped 

def get_audimetre_glob_installateur_mapped(db: Session=next(get_db())):
    aud_df= pd.read_sql( db.query(modau.Audimetre).statement.filter(modau.Audimetre.emplacement_meter == 2) ,db.bind)

    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()
    return aud_df.to_dict("records")

# recuperation de tous les audimetres mapped 

def get_aud_installe_mapped(db: Session = next(get_db())):
    
    aud_df= pd.read_sql( db.query(modau.AudimetreInstalle).statement ,db.bind)
        # .order_by(desc(modpo.Poste.date_installation_poste))
    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()

    return aud_df.to_dict("records")

# recuperation de tous les audimetres mapped 

def get_aud_installateur_mapped(db: Session = next(get_db())):
    
    aud_df= pd.read_sql( db.query(modau.AudimetreInstallateur.id_meter,modau.AudimetreInstallateur.nom_installateur,modau.Audimetre.statut_meter).join(modau.Audimetre).statement ,db.bind)
        # .order_by(desc(modpo.Poste.date_installation_poste))
    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()

    return aud_df.to_dict("records")

def get_audimetre_par_inst(inst:str,db: Session = next(get_db())):
   audi=db.query(modau.AudimetreInstallateur.id_meter).filter(modau.AudimetreInstallateur.nom_installateur==inst).all()
   return list(set([x.id_meter for x in audi ]))


######################################################### Updated by oumaima 19/09/2023 ###########################################
def get_audimetre_par_inst_all(inst:str,db: Session = next(get_db())):
   audi=db.query(modau.AudimetreInstallateur.id_meter).filter(modau.AudimetreInstallateur.nom_installateur!="PANEL").all()
   return list(set([x.id_meter for x in audi ]))
##############################################################################################################################


# audimetre panel mapped
def get_audimetre_panel_inst_mapped(db: Session=next(get_db())):
    aud_df= pd.read_sql( db.query(modau.Audimetre).statement.join(modau.AudimetreInstallateur).filter(modau.AudimetreInstallateur.nom_installateur == 'PANEL') ,db.bind)

    modalite_dict = map_modalite()   

    aud_df = aud_df.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()

    return aud_df.to_dict("records")
  
#--------------------------------------------------dashboard get -----------------------------------------------------------



def get_count(db: Session = next(get_db())):
    
    
     count_foy=db.query(modfoy.Foyer.f_num_con).filter(modfoy.Foyer.f_etat!=5).count()
     count_ind=db.query(modind.Individu).filter(modind.Individu.etat!=5).count()
     count_poste=db.query(modpo.Poste.f_num_con).join(modfoy.Foyer).join(modau.AudimetreInstalle).filter(modfoy.Foyer.f_etat!=5).count()
     db.rollback()
     db.close()

     return count_foy,count_ind,count_poste

def get_count_foy(db: Session = next(get_db())):

    
     modalite_dict = map_modalite()
     dictionnary={}
     csp_count_df=pd.read_sql(db.query(modcm.ChefMenage.csp_cm,func.count(modcm.ChefMenage.csp_cm).label('csp_count'))\
         .statement.join(modind.Individu).join(modfoy.Foyer).group_by(modcm.ChefMenage.csp_cm).filter(modfoy.Foyer.f_etat==1),db.bind)
     csp_count_df=csp_count_df.replace(modalite_dict).fillna('') 
     csp_count_df=csp_count_df.to_dict("records") 
     dictionnary['CSP']=csp_count_df

     taille_count_df=pd.read_sql(db.query(modfoy.EffectifFoyer\
         .f_taille,func.count(modfoy.EffectifFoyer.f_taille).label('taille_count')).\
             statement.join(modfoy.Foyer).group_by(modfoy.EffectifFoyer.f_taille).filter(modfoy.Foyer.mode_trait_f==1).filter(modfoy.Foyer.f_etat==1),db.bind)
     taille_count_df=taille_count_df.replace(modalite_dict).fillna('') 
     taille_count_df=taille_count_df.to_dict("records") 
     dictionnary['taille_foyer']=taille_count_df

     region_count_df=pd.read_sql(db.query(modco.Region.region,\
         func.count(modco.Region.region).label('region_count')).\
             statement.join(modco.Commune).join(modfoy.ContactFoyer).join(modfoy.Foyer).\
                 filter(modfoy.Foyer.f_etat==1).filter(modfoy.Foyer.mode_trait_f==1).group_by(modco.Region.region),db.bind)
     region_count_df=region_count_df.replace(modalite_dict).fillna('') 
     region_count_df=region_count_df.to_dict("records") 
     dictionnary['region']=region_count_df


     langue_count_df=pd.read_sql(db.query(modfoy.Sociodemo.langue_prin,func.count(modfoy.Sociodemo.langue_prin).label('langue_count')).\
         statement.join(modfoy.Foyer).filter(modfoy.Foyer.f_etat==1).filter(modfoy.Foyer.mode_trait_f==1).group_by(modfoy.Sociodemo.langue_prin),db.bind)
     langue_count_df=langue_count_df.replace(modalite_dict).fillna('') 
     langue_count_df=langue_count_df.to_dict("records") 
     dictionnary['langue_principale']=langue_count_df




     pres_enf_count_df=pd.read_sql(db.query(modfoy.EffectifFoyer.f_pres_enf,func.count(modfoy.EffectifFoyer.f_pres_enf).label('pres_enf_count'))\
         .statement.join(modfoy.Foyer).filter(modfoy.Foyer.f_etat==1).filter(modfoy.Foyer.mode_trait_f==1).group_by(modfoy.EffectifFoyer.f_pres_enf),db.bind)
     pres_enf_count_df=pres_enf_count_df.replace(modalite_dict).fillna('')
     pres_enf_count_df=pres_enf_count_df.replace({True:'Oui',False:'Non'})
     pres_enf_count_df=pres_enf_count_df.sort_values(by='f_pres_enf', ascending=False)
     pres_enf_count_df=pres_enf_count_df.to_dict("records") 
     dictionnary['pres_enf']=pres_enf_count_df 

     db.rollback()
     db.close()

     return dictionnary    


def get_count_ind(db: Session = next(get_db())):

    
     modalite_dict = map_modalite()
     dictionnary={}

     niv_dip_count_df=pd.read_sql(db.query(modind.Individu.niv_diplome_indiv,func.count(modind.Individu.niv_diplome_indiv).label('niv_dip_count')).\
         statement.filter(modind.Individu.statut_indiv==1,modind.Individu.etat!=5).group_by(modind.Individu.niv_diplome_indiv),db.bind)
     niv_dip_count_df=niv_dip_count_df.replace(modalite_dict).fillna('') 
     niv_dip_count_df=niv_dip_count_df.to_dict("records")
     dictionnary['niv_diplome']=niv_dip_count_df
     
    
    
     category_count_df=pd.read_sql(db.query(modind.Individu.statut_indiv,func.count(modind.Individu.statut_indiv).label('statut_count')).\
         statement.group_by(modind.Individu.statut_indiv).filter(modind.Individu.etat!=5),db.bind)
     category_count_df=category_count_df.replace(modalite_dict).fillna('') 
     category_count_df=category_count_df.to_dict("records")
     dictionnary['category_indiv']=category_count_df
     
    
     tranche_count_df=pd.read_sql(db.query(func.fun_get_modalite_tranche(modind.Individu.id_indiv,6).label('modalite_tranche_age'),\
         func.fun_get_age_tranche(modind.Individu.id_indiv,6).label('tranche_age'),func.count('tranche_age').label('count_tranche_age')).\
             statement.filter(modind.Individu.statut_indiv==1,modind.Individu.etat!=5).group_by('tranche_age','modalite_tranche_age').\
                 order_by('modalite_tranche_age'),db.bind)
     tranche_count_df=tranche_count_df.replace(modalite_dict).fillna('') 
     tranche_count_df= tranche_count_df.to_dict("records")
     dictionnary['tranche_count']=tranche_count_df
     db.rollback()
     db.close()
     return dictionnary



def get_id_indiv(fnco:int=0,db: Session = next(get_db())):
   
    return db.query(func.max(modind.Individu.id_indiv+1).label('id_indiv'),func.max(case([(modind.Individu.f_num_con==fnco,modind.Individu.touche_indiv+1)],else_=1)).label('touche_indiv')).first() 


def get_poste_id(id_foy:int,db: Session = next(get_db())):
   
    return  db.query(func.concat(id_foy,'0',func.count(modpo.Poste.id_poste)+1).label('id_poste')\
        ,(func.count(modpo.Poste.id_poste)+1).label('num_poste')).filter(modpo.Poste.f_num_con == id_foy).all()






def get_audimetre_installe_foy( id_foy: int,db: Session = next(get_db())):

    return db.query(modau.AudimetreInstalle).join(modpo.Poste).join(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foy).all() 
def get_audimetre_installe( id_meter: str,db: Session = next(get_db())):
   
    return db.query(modau.AudimetreInstalle).filter(modau.AudimetreInstalle.id_meter == id_meter).first() 

def get_audimetre_installateur(id_meter: str,db: Session = next(get_db())):
     
    
    return db.query(modau.AudimetreInstallateur).filter(modau.AudimetreInstallateur.id_meter == id_meter).first() 

def get_audimetre( id_meter: str,db: Session=next(get_db())):
       
    
    return  db.query(modau.Audimetre).filter(modau.Audimetre.id_meter == id_meter).first()

def get_audimetre_stock(db: Session=next(get_db())):

    
    return db.query(modau.Audimetre).filter(modau.Audimetre.emplacement_meter == 3).all()   

def get_audimetre_glob_inst(db: Session=next(get_db())):

    
    return db.query(modau.Audimetre).filter(modau.Audimetre.emplacement_meter == 2).all()  
def get_audimetre_panel_inst(db: Session=next(get_db())):
    
    
    return db.query(modau.Audimetre).outerjoin(modau.AudimetreInstallateur).filter(modau.AudimetreInstallateur.nom_installateur == 'PANEL').all() 

def get_installateur(db: Session=next(get_db())):

    return db.query(modau.Installateurs.nom_installateur.label('num_modalite'),modau.Installateurs.nom_installateur.label('label_modalite')).filter(modau.Installateurs.etat_installateur==1).all() 

def get_meters_modalite(db: Session=next(get_db())):
     
       
    
    return db.query(modau.Audimetre.id_meter.label('num_modalite'),modau.Audimetre.id_meter.label('label_modalite')).filter(modau.Audimetre.emplacement_meter==3).all() 


def get_imei_modalite(db: Session=next(get_db())):

    return db.query(modau.Audimetre.imei.label('num_modalite'),modau.Audimetre.imei.label('label_modalite')).filter(modau.Audimetre.emplacement_meter==3).all() 
def get_imei_inst_modalite(db: Session=next(get_db())):

    return db.query(modau.Audimetre.imei.label('num_modalite'),modau.Audimetre.imei.label('label_modalite')).filter(modau.Audimetre.emplacement_meter==2).all() 
    
def get_meters_inst_modalite(db: Session=next(get_db())):
    
    
    return db.query(modau.Audimetre.id_meter.label('num_modalite'),modau.Audimetre.id_meter.label('label_modalite')).filter(modau.Audimetre.emplacement_meter==2).all() 

def get_villes(db: Session=next(get_db())):
    return db.query(func.distinct(modco.Ville.ville).label('num_modalite'),modco.Ville.ville.label('label_modalite')).all() 

def get_poste_by_meter(id_meter: str,db: Session = next(get_db())):

    return db.query(modpo.Poste).join(modau.AudimetreInstalle).filter(modau.AudimetreInstalle.id_meter==id_meter).first()









###############################################################EXPORT PANEL 

def get_foyers_panel(db: session=next(get_db())):

    foy_df = pd.read_sql(db.query(modfoy.Foyer,modcm.ChefMenage,modfoy.ContactFoyer,modco.Commune,modco.Region,modco.Ville,modfoy.Sociodemo,modfoy.Equipement,modfoy.EffectifFoyer)\
    .order_by(desc(modfoy.Foyer.f_date_deb))\
        .statement.join(modcm.ChefMenage.ind,isouter = True).join(modind.Individu.foyer).join(modfoy.ContactFoyer)\
            .join(modfoy.EffectifFoyer).join(modfoy.Sociodemo,full = True).join(modfoy.Equipement).outerjoin(modco.Commune)\
            .join(modco.Region).join(modco.Ville) ,db.bind)

        # .order_by(desc(modfoy.Foyer.f_date_deb))
    count_poste_df=pd.read_sql('select f_num_con,count(*) as nbr_postes_inst from audimetre_installe join poste using(id_poste) GROUP BY f_num_con' ,db.bind)
    modalite_dict = map_modalite()

    foy_df = foy_df.replace(modalite_dict).fillna('') 
    foy_df=foy_df.merge(count_poste_df,how='left',on='f_num_con')
    db.rollback()
    db.close()    
    # db.close()
    return foy_df.to_csv(index=False,encoding='utf-8-sig',quotechar='"')


def get_indivs_panel(db: session=next(get_db())):

    indiv = pd.read_sql(db.query(modcmas.ExportPanelIndiv).statement ,db.bind)

        # .order_by(desc(modfoy.Foyer.f_date_deb))
  
    modalite_dict = map_modalite()

    indiv = indiv.replace(modalite_dict).fillna('') 
    db.rollback()
    db.close()

    
    # db.close()
    return indiv.to_csv(index=False,encoding='utf-8-sig',quotechar='"')




def get_postes_panel(db: session=next(get_db())):

    poste = pd.read_sql(db.query(modpo.Poste,modpo.EquipementPoste,modpo.Reception,modau.AudimetreInstalle).statement.join(modpo.EquipementPoste).join(modpo.Reception).join(modau.AudimetreInstalle).join(modfoy.Foyer) ,db.bind)

        # .order_by(desc(modfoy.Foyer.f_date_deb))
  
    modalite_dict = map_modalite()

    poste = poste.replace(modalite_dict).fillna('') 
    # db.close()
    db.rollback()     
    db.close()

    return poste.to_csv(index=False,encoding='utf-8-sig',quotechar='"')  



def get_mouvement(db: session=next(get_db())):
        mouvement = pd.read_sql(db.query(modau.Mouvements).statement,db.bind)
        modalite_dict = map_modalite()
        modalite_dict['old_emplacement'] = modalite_dict['emplacement_meter']
        modalite_dict['new_emplacement'] = modalite_dict['emplacement_meter']
        modalite_dict['old_statut'] = modalite_dict['statut_meter']
        modalite_dict['new_statut'] = modalite_dict['statut_meter']
        mouvement = mouvement.replace(modalite_dict).fillna('')
        #mouvement_list = []
        #mouvement_list = mouvement.values.tolist()
        #mouvement_list.insert(0,mouvement.columns.tolist())
        db.rollback()     
        db.close()

        return mouvement.to_dict("records") 