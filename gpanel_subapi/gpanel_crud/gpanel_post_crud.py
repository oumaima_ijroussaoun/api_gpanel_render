from typing import List
from ...models import ModelBaseAudimetre as modau
from ...schemas import SchemaBaseAudimetre as shemau
from sqlalchemy.orm import Session

from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelChefMenage as modcm
from ...models import ModelBasePoste as modpo

from ...schemas import SchemaBaseFoyer as schemfoy
from ...schemas import SchemaBaseIndividu as schemind
from ...schemas import SchemaBasePoste as schempo

from . import gpanel_put_crud as putcrud
from . import gpanel_get_crud as getcrud
from .. import GpanelExtraSchemas as extschem

from . import get_db




########################################create full foyer ###########################
def create_full_foyer(my_foyer:schemfoy.CreateFoyer,my_contact_foyer:extschem.ContactFoyer ,my_equipement:extschem.Equipement,my_sociodemo:extschem.Sociodemo, db: Session = next(get_db())) :
    try:
        ############## create foyer
        db_foyer = modfoy.Foyer(f_id_mediamat=my_foyer.f_id_mediamat,f_num_con=int('3'+str(my_foyer.f_id_mediamat)), f_nom=my_foyer.f_nom,
         f_etat=my_foyer.f_etat, f_csp=my_foyer.f_csp, num_recrut=my_foyer.num_recrut, date_recrut=my_foyer.date_recrut,
          vague_recrut=my_foyer.vague_recrut, f_date_deb=my_foyer.f_date_deb, mode_trait_f=my_foyer.mode_trait_f,tv_online=my_foyer.tv_online,
           nom_installateur=my_foyer.nom_installateur,telephones=my_foyer.telephones,motivation=my_foyer.motivation)

        db.add(db_foyer)
        db.flush()
        db.refresh(db_foyer)
        ################create db_effectif_foyer
        db_effectif_foyer = modfoy.EffectifFoyer(f_num_con=db_foyer.f_num_con)
        db.add(db_effectif_foyer)
        db.flush()
        db.refresh(db_effectif_foyer)
        ################create db_equipement
        db_equipement = modfoy.Equipement(f_num_con = db_foyer.f_num_con,
        nbr_clim =my_equipement.nbr_clim,
        nbr_app_chauf =my_equipement.nbr_app_chauf,
        nbr_refrig =my_equipement.nbr_refrig,
        nbr_congel =my_equipement.nbr_congel,
        nbr_four_micro =my_equipement.nbr_four_micro,
        nbr_cuisiniere =my_equipement.nbr_cuisiniere,
        nbr_lave_linge =my_equipement.nbr_lave_linge,
        nbr_lave_vaiss =my_equipement.nbr_lave_vaiss,
        nbr_aspi =my_equipement.nbr_aspi,
        poss_internet =bool(my_equipement.poss_internet),
        poss_tnt=bool(my_equipement.poss_tnt),
        source_electricite =my_equipement.source_electricite,
        nbr_tv =my_equipement.nbr_tv,
        nbr_vehicule =my_equipement.nbr_vehicule,
        nbr_tel_mob =my_equipement.nbr_tel_mob,
        nbr_tel_fix =my_equipement.nbr_tel_fix,
        nbr_ordi =my_equipement.nbr_ordi,
        nbr_radio =my_equipement.nbr_radio,
        nbr_ordi_emp =my_equipement.nbr_ordi_emp,
        nbr_home_cinema =my_equipement.nbr_home_cinema,
        nbr_deco_sat =my_equipement.nbr_deco_sat,
        nbr_console_jeux_se =my_equipement.nbr_console_jeux_se,
        nbr_console_jeux_ae =my_equipement.nbr_console_jeux_ae )
        db.add(db_equipement)
        db.flush()
        db.refresh(db_equipement)
    
        ################create db_contact_foyer
        db_contact_foyer = modfoy.ContactFoyer(f_num_con=db_foyer.f_num_con,
        date_install_telfix = my_contact_foyer.date_install_telfix,
        f_adr1 = my_contact_foyer.f_adr1,
        longitude = my_contact_foyer.longitude,
        latitude = my_contact_foyer.latitude,
        id_commune = my_contact_foyer.id_commune,
        f_adr2 = my_contact_foyer.f_adr2)
        db.add(db_contact_foyer)
        db.flush()
        db.refresh(db_contact_foyer)
        ################create db_sociodemo
        db_sociodemo = modfoy.Sociodemo(f_num_con=db_foyer.f_num_con,
        f_type_offre=my_sociodemo.f_type_offre,
        f_tranche_revenu=my_sociodemo.f_tranche_revenu,
        type_hab=my_sociodemo.type_hab,
        standing_hab=my_sociodemo.standing_hab,
        pos_res_sec=my_sociodemo.pos_res_sec,
        langue_prin=my_sociodemo.langue_prin,
        stand_vie=my_sociodemo.stand_vie)
        db.add(db_sociodemo)
        db.flush()
        db.refresh(db_sociodemo)
        ################commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  
    



#########################################################################create indiv and cm 
def create_indiv(my_ind: schemind.CreateIndividu, my_chef_menage: extschem.ChefMenage, db: Session = next(get_db())):
    try:    
        db_individu = modind.Individu(id_indiv = my_ind.id_indiv, nom = my_ind.nom , prenom = my_ind.prenom ,
        date_naiss = my_ind.date_naiss,telephones_indiv=my_ind.telephones_indiv, touche_indiv = my_ind.touche_indiv,
         niveau_instruc_glob = my_ind.niveau_instruc_glob,
        categorie_indiv =my_ind.categorie_indiv,
        sexe =my_ind.sexe,
        etat =my_ind.etat,
        categorie_inact =my_ind.categorie_inact,
        activite = my_ind.activite,
        statut_indiv=my_ind.statut_indiv,
        enfant_touche_mere =my_ind.enfant_touche_mere,
        is_cm = my_ind.is_cm,
        is_menagere = my_ind.is_menagere,
        is_maman = my_ind.is_maman,
        is_epouse = my_ind.is_epouse,
        is_aide_domestique = my_ind.is_aide_domestique,
        voyage_etrg=my_ind.voyage_etrg,
        but_voy =my_ind.but_voy,
        f_num_con = my_ind.f_num_con,
        mode_trait_indiv=my_ind.mode_trait_indiv)
        db.add(db_individu)
        db.flush()
        db.refresh(db_individu)
        db_chef_menage = modcm.ChefMenage(id_indiv=my_ind.id_indiv,
        profession_cm =my_chef_menage.profession_cm,
        csp_cm =my_chef_menage.csp_cm,niv_diplome_cm=my_chef_menage.niv_diplome_cm)
        db.add(db_chef_menage)
        db.flush()
        db.refresh(db_chef_menage)
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()    
    

#########################################################################create full poste 

def create_full_poste(my_poste: schempo.CreatePoste, my_equip: extschem.EquipementPoste,my_recep: extschem.Reception,my_audimetre:extschem.CreateAudimetreInstalle, db: Session = next(get_db())):
    try:
        db_poste = modpo.Poste(id_poste=my_poste.id_poste,
        num_poste=my_poste.num_poste,
        etat_actuel=my_poste.etat_actuel,
        marque_tv =my_poste.marque_tv,
        model_tv=my_poste.model_tv,
        tv_non_installe =my_poste.tv_non_installe,
        motif_tv_non_installe=my_poste.motif_tv_non_installe,
        dimension_tv=my_poste.dimension_tv,
        type_ecran=my_poste.type_ecran,
        taille_ecran=my_poste.taille_ecran,
        anciennete_tv=my_poste.anciennete_tv,
        statut_poste=my_poste.statut_poste,
        mode_trait_poste=my_poste.mode_trait_poste,

        f_num_con =my_poste.f_num_con,
        emplacement=my_poste.emplacement,
        date_installation_poste =my_poste.date_installation_poste)
        db.add(db_poste)
        db.flush() 
        db.refresh(db_poste)

        db_equipement_poste = modpo.EquipementPoste(id_poste=my_poste.id_poste,
        decodeur_sat_num=my_equip.decodeur_sat_num,
        decodeur_adsl=my_equip.decodeur_adsl,
        home_cinema=my_equip.home_cinema,
        antenne_parab=my_equip.antenne_parab,
        type_antenne_parab=my_equip.type_antenne_parab,
        reception_adsl=my_equip.reception_adsl,
        type_sonde_poste=my_equip.type_sonde_poste,
        equipe_tnt=my_equip.equipe_tnt)
        db.add(db_equipement_poste)
        db.flush()
        db.refresh(db_equipement_poste)

        db_reception = modpo.Reception(id_poste=my_poste.id_poste,type_reception=my_recep.type_reception,reception_chaine=my_recep.reception_chaine,reception_sat=my_recep.reception_sat)
        db.add(db_reception)
        db.flush()
        db.refresh(db_reception)

        db_audimetre = modau.AudimetreInstalle(id_poste=my_poste.id_poste,id_meter=my_audimetre.id_meter,sortie_audio=my_audimetre.sortie_audio,capture_audio=my_audimetre.capture_audio,nom_installateur=my_audimetre.nom_installateur)
        db.add(db_audimetre)
        db.flush()
        db.refresh(db_audimetre)
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  
        




def create_audimetre_inst(id_meters:List[str],nom_inst:str, db: Session = next(get_db())):
    try:
        for id_meter in id_meters:
            db_audimetre_inst =  modau.AudimetreInstallateur(id_meter=id_meter,nom_installateur=nom_inst)

            db.add(db_audimetre_inst)
            db.flush()
            db.refresh(db_audimetre_inst)
            stored_meter = getcrud.get_audimetre(id_meter,db=db)

               
            if (stored_meter.etat_meter==3):             
                for key, value in {"statut_meter": stored_meter.statut_meter,"etat_meter": 3,"emplacement_meter": 2}.items():
                    setattr(stored_meter, key, value)
                db.add(stored_meter)
                db.flush()
                db.refresh(stored_meter)
            else:
                    for key, value in {"statut_meter": 2,"etat_meter": 1,"emplacement_meter": 2}.items():
                         setattr(stored_meter, key, value)
                    db.flush()
                    db.refresh(stored_meter)    
        
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  
       

            
def panel_installateur(id_meters:List[str],nom_inst:str, db: Session = next(get_db())):
    try:
        
        for id_meter in id_meters:
            stored_meter = getcrud.get_audimetre(id_meter,db=db)
            if (stored_meter.etat_meter==3):
                    for key, value in {"statut_meter": stored_meter.statut_meter,"etat_meter": 3,"emplacement_meter": 2}.items():
                         setattr(stored_meter, key, value)
            else:               
                    for key, value in {"statut_meter": 2,"etat_meter": 1,"emplacement_meter": 2}.items():
                        setattr(stored_meter, key, value)
            
            del_meter_installateur=getcrud.get_audimetre_installateur(db=db ,id_meter=id_meter)
            db.delete(del_meter_installateur)
            db_audimetre_inst =  modau.AudimetreInstallateur(id_meter=id_meter,nom_installateur=nom_inst)

            db.add(db_audimetre_inst)
            db.flush()
            db.refresh(db_audimetre_inst)
            stored_meter = getcrud.get_audimetre(id_meter,db=db)

               
            if (stored_meter.etat_meter==3):             
                for key, value in {"statut_meter": stored_meter.statut_meter,"etat_meter": 3,"emplacement_meter": 2}.items():
                    setattr(stored_meter, key, value)
                db.add(stored_meter)
                db.flush()
                db.refresh(stored_meter)
            else:
                    for key, value in {"statut_meter": 2,"etat_meter": 1,"emplacement_meter": 2}.items():
                         setattr(stored_meter, key, value)
                    db.flush()
                    db.refresh(stored_meter)    
        
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  

    
        
           

