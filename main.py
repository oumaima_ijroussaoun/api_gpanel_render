
from fastapi import FastAPI
from sqlalchemy import orm

from .startup import startup


from .models import init_db_from_models
from .database import Base, engine

from . import cros_app, mount_app

init_db_from_models(Base,engine,orm)
app=FastAPI()
mount_app(app)
cros_app(app)
startup(app)

