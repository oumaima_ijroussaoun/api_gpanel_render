
from fastapi_utils.tasks import repeat_every



from .bussiness_subapi.bussiness_ops.structure_ops import calculus_save
from .gpanel_subapi.gpanel_crud.gpanel_put_crud import update_database
from .gpanel_subapi.gpanel_crud.gpanel_put_crud import terminate_idle_conn
from datetime import datetime
from threading import Timer



def startup(app):
    # def get_struct_time():
    #     x=datetime.today()
    #     y=x.replace(day=x.day+1, hour=22, minute=0, second=0, microsecond=0)
    #     delta_t=y-x
    #     return delta_t.seconds+1

    struct_time=60*60
    update_time=60*60
    idle_time=30

    #post structure calculs to db
    @app.on_event("startup")
    @repeat_every(seconds=struct_time,wait_first=True)
    def auto_save_calculs():
        x = datetime.today()
        if (x.hour == 22):    
            calculus_save()
            print(f'struct data saved successfuly ')

    @app.on_event("startup")        
    @repeat_every(seconds=update_time,wait_first=False)
    def update_tables():
        x = datetime.today()
        if (x.hour in [8,10,12,14,15,16,17]):    
            update_database()
            print(f'database updated')
 