from fastapi import APIRouter,HTTPException
from typing import List


from ..logging_ops import panda_logging_ops as plo


route=APIRouter(prefix="/logging")



##################### get var and modalite 

@route.post("/panda/log")
def panda_log(log,err:bool):
    try:
        return plo.logging_panda_info(log,err)
    except:
        raise HTTPException(status_code=400, detail="verifier les données saisies") 
