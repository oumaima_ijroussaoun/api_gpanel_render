import pandas as pd
from pyhocon import ConfigFactory
from loguru import logger
from datetime import date,datetime
import sys

import json

from xml.dom import minidom
from sqlalchemy.sql.expression import and_ 
from sqlalchemy.orm import Session 

from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseAudimetre as modau
from ...models import ModelChefMenage as modcm
from ...models import ModelBaseCommune as modco
from ...models import ModelModalite as mod

from ...models import ModelTripleS as modts
from ...models import ModelControles as modctr


from . import get_db




#########################rpa functions


def get_today_date(date_format):
    
    today = datetime.today()
    strDate = today.strftime(date_format)
    return strDate


def create_foyer_entry(row):
    
    if (row.mode_trait_f==1):
        return f'   <foyer foyer_id="{row.f_id_mediamat}" mode_traitement="WH_MCM_NORMAL">'
    elif(row.mode_trait_f==2):
        return f'   <foyer foyer_id="{row.f_id_mediamat}" mode_traitement="WH_MCM_LONG_ABSENCE">'



def create_indiv_entry(row):
    return f'         <membre individu_id="{row.id_indiv}" touche="{row.touche_indiv}" mode_traitement="WI_MCM_NORMAL"/>'        



def create_indiv_perm_entry(row):
    return f'         <invite_permanent individu_id="{row.id_indiv}" touche="{row.touche_indiv}" mode_traitement="WI_MCM_NORMAL"/>'



def create_poste_entry(row):
    
    if (row.mode_trait_poste==1):
        return f'         <meter id_technique="{row.id_meter}" meter_id="{row.num_poste}" mode_traitement="WE_MCM_NORMAL"/>'
    elif(row.mode_trait_poste==2):
        return f'         <meter id_technique="{row.id_meter}" meter_id="{row.num_poste}" mode_traitement="W_POSTE_NON_CONNECTE"/>'



def create_RPA(households_on_holiday:list,db: Session = next(get_db())):
#   db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
#   db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
#   db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
#   db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
#   db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
#   db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
#   db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
#   db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
#   db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
#   db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
#   db.commit()


   
   household_df = pd.read_sql(db.query(modfoy.Foyer.f_num_con,modfoy.Foyer.f_id_mediamat,modfoy.Foyer.mode_trait_f).statement.\
       filter(modfoy.Foyer.f_etat==1),db.bind)

   household_df.loc[household_df['f_id_mediamat'].isin(households_on_holiday),'mode_trait_f']=2
   household_df=household_df.sort_values(by=['f_id_mediamat'])


   today_date=get_today_date("%Y-%m-%d")
   today_date_time = get_today_date("%Y-%m-%dT%H:%M:%S")


   rpa=f'''<?xml version="1.0" encoding="UTF-8"?>
   <foyers xmlns="http://www.mediametrie.fr/nge/schemas" date="{today_date}" creation_date="{today_date_time}">'''+'\n'
   rpa=rpa+f'   <!--Nombre de foyers : {len(household_df.index)}-->'+'\n'

   for row in household_df.itertuples():
       rpa=rpa+str(create_foyer_entry(row))+'\n'
       id_foyer=row.f_num_con

       #-----------------------members---------------------------
       members_df = pd.read_sql(db.query(modind.Individu.id_indiv \
       ,modind.Individu.touche_indiv ,modind.Individu.mode_trait_indiv ,modfoy.Foyer.f_id_mediamat).statement\
           .join(modfoy.Foyer).filter(and_(modfoy.Foyer.f_num_con == id_foyer , modind.Individu.statut_indiv==1 ,\
                modind.Individu.etat!=5)),db.bind)
       members_df.sort_values(by='id_indiv',inplace=True)
      
       rpa=rpa+"      <membres>"+'\n'  

       for indiv_row in members_df.itertuples():
        
            rpa=rpa+create_indiv_entry(indiv_row)+'\n'

       rpa=rpa+"      </membres>"+'\n' 



       members_permanents_df= pd.read_sql(db.query(modind.Individu.id_indiv \
       ,modind.Individu.touche_indiv ,modind.Individu.mode_trait_indiv ,modfoy.Foyer.f_id_mediamat).statement\
           .join(modfoy.Foyer).filter(and_(modfoy.Foyer.f_num_con == id_foyer , modind.Individu.statut_indiv==2 ,\
                modind.Individu.etat!=5)),db.bind)    
       members_permanents_df.sort_values(by='touche_indiv',inplace=True)

       if(members_permanents_df.empty):
        rpa=rpa+"      <invites_permanents/>"+'\n'
       else: 
        rpa=rpa+"      <invites_permanents>"+'\n'
        for indiv_perm_row in members_permanents_df.itertuples():
            rpa=rpa+create_indiv_perm_entry(indiv_perm_row)+'\n'
        rpa=rpa+"      </invites_permanents>"+'\n'
    
       rpa=rpa+'      <invites_occasionnels mode_traitement="WG_MCM_NORMAL"/>'+'\n'
         
       postes_df = pd.read_sql(db.query(modpo.Poste.num_poste,modau.AudimetreInstalle.id_meter,modpo.Poste.mode_trait_poste,modfoy.Foyer.f_id_mediamat).statement\
           .join(modau.AudimetreInstalle).join(modfoy.Foyer).filter(modfoy.Foyer.f_num_con == id_foyer),db.bind)

       postes_df.loc[postes_df['f_id_mediamat'].isin(households_on_holiday),'mode_trait_poste']=2
       postes_df.sort_values(by=['f_id_mediamat','num_poste'],inplace=True)

       rpa=rpa+"      <meters>"+'\n'
       for poste_row in postes_df.itertuples():
           rpa=rpa+create_poste_entry(poste_row)+'\n'
       rpa=rpa+"      </meters>"+'\n'
    
       rpa=rpa+'   </foyer>'+'\n'

   rpa=rpa+'</foyers>'    
   print('rpa end')
   #Rpa_date = get_today_date("%Y%m%d")
   db.close()
   return rpa
    
   #Rpa_path=f'RPA_{Rpa_date}.xml'
  
   #open(Rpa_path, 'wb').write(rpa.encode('utf-8'))
          

def create_RPA_new(households_on_holiday:list,db: Session = next(get_db())):
    foy = pd.read_sql(db.query(modfoy.Foyer.f_num_con,modfoy.Foyer.f_id_mediamat,modfoy.Foyer.mode_trait_f).statement.\
                                  filter(modfoy.Foyer.f_etat==1),db.bind).replace({1:'WH_MCM_NORMAL',2:'WH_MCM_LONG_ABSENCE'}).sort_values(by='f_id_mediamat')   
                      
    indiv = pd.read_sql(db.query(modind.Individu.f_num_con,modind.Individu.id_indiv \
                             ,modind.Individu.touche_indiv ).statement\
                                .filter(and_(modind.Individu.statut_indiv==1 ,\
                                modind.Individu.etat!=5)),db.bind)
  
    invite= pd.read_sql(db.query(modind.Individu.f_num_con,modind.Individu.id_indiv \
                     ,modind.Individu.touche_indiv ).statement\
                         .filter(and_(modind.Individu.statut_indiv==2 ,\
                                            modind.Individu.etat!=5)),db.bind)    
    invite.sort_values(by='touche_indiv',inplace=True)
    poste = pd.read_sql(db.query(modpo.Poste.f_num_con,modfoy.Foyer.f_id_mediamat,modpo.Poste.num_poste,modau.AudimetreInstalle.id_meter,modpo.Poste.mode_trait_poste).statement\
                                         .join(modau.AudimetreInstalle).join(modfoy.Foyer),db.bind)
    poste['mode_trait_poste'].replace({1:'WE_MCM_NORMAL',2:'W_POSTE_NON_CONNECTE'},inplace=True)
    poste.loc[poste['f_id_mediamat'].isin(households_on_holiday),'mode_trait_poste']=2
    
  
    db.close()
    #initial data and comments
    today_date=get_today_date("%Y-%m-%d")
    today_date_time = get_today_date("%Y-%m-%dT%H:%M:%S")
    root = minidom.Document()
    foyers = root.createElement('foyers')
    foyers.setAttribute('xmlns', "http://www.mediametrie.fr/nge/schemas")
    foyers.setAttribute('date', f"{today_date}")
    foyers.setAttribute('creation_date', f"{today_date_time}")
    nbr_foy=root.createComment(f'Nombre des foyers : {len(foy.index)}')
    nbr_membre=root.createComment(f'Nombre des individus : {len(indiv.index)}')
    nbr_guest=root.createComment(f'Nombre des invités : {len(invite.index)}')
    nbr_postes=root.createComment(f'Nombre de postes : {len(poste.index)}')
    foyers.appendChild(nbr_membre)
    foyers.appendChild(nbr_guest)
    foyers.appendChild(nbr_postes)
    foyers.appendChild(nbr_foy)
    root.appendChild(foyers)
    
    #creating RPA entries

    for row in foy.itertuples():
        # indiv , poste , invité par foyer 
        membre_by_foy=indiv[indiv['f_num_con']==row.f_num_con].sort_values(by='touche_indiv')
        invite_by_foy=invite[invite['f_num_con']==row.f_num_con].sort_values(by='touche_indiv')
        meter_by_foy=poste[poste['f_num_con']==row.f_num_con].sort_values(by=['f_num_con','num_poste'])
        
        
        
        #---------------------------creating foyer-----------------------------------------------#
        foyer= root.createElement('foyer')
        foyer.setAttribute('foyer_id', f'{row.f_id_mediamat}')
        foyer.setAttribute('mode_traitement', f'{row.mode_trait_f}')


        #---------------------------creating individus-----------------------------------------------#
        individus =root.createElement('membres')

        for memb in membre_by_foy.itertuples():
            individu=root.createElement('membre')
            individu.setAttribute('individu_id', f'{memb.id_indiv}')
            individu.setAttribute('touche', f'{memb.touche_indiv}')
            individu.setAttribute('mode_traitement', 'WI_MCM_NORMAL')
            individus.appendChild(individu)

        foyer.appendChild(individus)
        #---------------------------individus/-----------------------------------------------#
        
        
        
        
        #---------------------------creating invites_permanents-----------------------------------------------#
        invites_permanents=root.createElement('invites_permanents')
        for inv in invite_by_foy.itertuples():

            invites_permanent=root.createElement('invite_permanent')
            invites_permanent.setAttribute('individu_id', f'{inv.id_indiv}')
            invites_permanent.setAttribute('touche', f'{inv.touche_indiv}')
            invites_permanent.setAttribute('mode_traitement', 'WI_MCM_NORMAL')
            invites_permanents.appendChild(invites_permanent)

        foyer.appendChild(invites_permanents)
        #---------------------------invites_permanents/-----------------------------------------------#
        
        
        #---------------------------creating invites_occasionnels-----------------------------------------------#
        invites_occasionnels=root.createElement('invites_occasionnels')
        invites_occasionnels.setAttribute('mode_traitement', 'WG_MCM_NORMAL')
        foyer.appendChild(invites_occasionnels)
        #---------------------------invites_occasionnels/-----------------------------------------------#
        
                
        #---------------------------creating meters-----------------------------------------------#
        meters =root.createElement('meters')
        for met in meter_by_foy.itertuples():
            meter=root.createElement('meter')
            meter.setAttribute('id_technique',f'{met.id_meter}')
            meter.setAttribute('meter_id', f'{met.num_poste}')
            meter.setAttribute('mode_traitement',f'{met.mode_trait_poste}')
            meters.appendChild(meter)

        foyer.appendChild(meters)
        #---------------------------meters/-----------------------------------------------#
        

        foyers.appendChild(foyer)
        #---------------------------foyer/-----------------------------------------------#
        
    #Creating RPA.xml text
    xml_str = root.toprettyxml(indent ="   ",encoding="UTF-8") 
    xml_str=xml_str.split(b'\n',1)[0]+b'\n   '+xml_str.split(b'\n',1)[1]
    return xml_str.decode("UTF-8")

 

	






   

