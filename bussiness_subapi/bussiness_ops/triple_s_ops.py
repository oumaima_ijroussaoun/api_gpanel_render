import difflib
import functools
import pandas as pd
from pyhocon import ConfigFactory
from loguru import logger
from datetime import date,datetime
import sys
import re
import numpy as np
from io import StringIO
from sqlalchemy import func
import datetime
from sqlalchemy import desc  # Import desc for descending order
import json
from datetime import datetime, timedelta
import datetime
from sqlalchemy.sql.expression import and_ 
from sqlalchemy.orm import Session, session 
from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseAudimetre as modau
from ...models import ModelChefMenage as modcm
from ...models import ModelBaseCommune as modco
from ...models import ModelTripleS as modts
from . import get_db
from fastapi.responses import JSONResponse
import json
from fastapi import Response



# config = ConfigFactory.parse_file('/home/mcm-hive/Desktop/hive/api_layer/general_api/test_db_mod/bussiness_subapi/bussiness_config/config_sss.conf')
config = ConfigFactory.parse_file('/home/issam/Desktop/mcm/projects/mcm-hive/api_layer/general_api/general/bussiness_subapi/bussiness_config/config_sss.conf')







############################################################ Oumaima ############################################################################################
######################################################## Household ###########################################################################
def get_monday_of_current_week():
    today= datetime.date.today()
    monday = today - timedelta(days=today.weekday())
    return monday


def check_presence_monday_current_week_household_hist(db: Session = next(get_db())):
           monday_of_current_week = get_monday_of_current_week()
           households =db.query(modts.House_hold_triple_s_hist).filter(modts.House_hold_triple_s_hist.date == monday_of_current_week).all()
           
           if households:
            print("Query result is not empty:")
            return("yes")
            
           else:
            print("Query result is empty.")
            return("no")

          

def save_household_triple_s_hist(db: Session = next(get_db())):
           #Recuperer les données de la vue : HouseholdTripleS
           households = db.query(modts.HouseholdTripleS).all()
           monday_of_current_week = get_monday_of_current_week()
           for household in households:
             data = household.__dict__
             data.pop('_sa_instance_state')
             data['date'] = monday_of_current_week
             data['statut'] = 0
             hist_entry = modts.House_hold_triple_s_hist(**data)
             db.add(hist_entry)

           db.commit()




def update_household_triple_s_hist(db: Session = next(get_db())):
    # Recuperer les données de la vue : HouseholdTripleS
    households = db.query(modts.HouseholdTripleS).all()
    
    # Get the Monday of the current week
    monday_of_current_week = get_monday_of_current_week()
    
    db.query(modts.House_hold_triple_s_hist).filter(
        modts.House_hold_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        )
    ).update({"statut": modts.House_hold_triple_s_hist.statut + 1}, synchronize_session='fetch')
    
    # Add new rows with statut set to 0
    for household in households:
        data = household.__dict__
        data.pop('_sa_instance_state')
        data['date'] =datetime.date.today()
        data['statut'] = 0
        hist_entry = modts.House_hold_triple_s_hist(**data)
        db.add(hist_entry)

    db.commit()


def compare_vue_table_hist_household(db: Session = next(get_db())):
    monday_of_current_week = get_monday_of_current_week()

    #today= datetime.date.today()
    #monday_of_current_week = today - timedelta(days=today.weekday()-7)
    vue = db.query(modts.HouseholdTripleS).all()
    table= db.query(modts.House_hold_triple_s_hist).filter(
        modts.House_hold_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        ),modts.House_hold_triple_s_hist.statut==0
    ).all()
    # Utilisez des ensembles pour simplifier la comparaison (excluez la date)
    vue_set = {(h.f_id_mediamat, h.tranche_age, h.f_taille, h.f_pres_enf, h.csp_cm, h.activite_menagere, h.langue_prin, h.region, h.milieu, h.region_milieu,h.taille_commune, h.nbr_tv,  h.f_type_offre, h.stand_vie, h.f_csp, h.offre_milieu, h.niv_diplome_cm,h.taille_milieu,h.num_region_administrative, h.tv_online  ) for h in vue}
    table_set = {(h.f_id_mediamat, h.tranche_age, h.f_taille, h.f_pres_enf, h.csp_cm, h.activite_menagere, h.langue_prin, h.region, h.milieu, h.region_milieu,h.taille_commune, h.nbr_tv,  h.f_type_offre, h.stand_vie, h.f_csp, h.offre_milieu, h.niv_diplome_cm,h.taille_milieu,h.num_region_administrative, h.tv_online  ) for h in table}


    # Identifiez les lignes présentes dans vue mais absentes dans table
    missing_lines_absentes_table = vue_set - table_set
    # Identifiez les lignes présentes dans table mais absentes dans vue
    missing_lines_absentes_vue = table_set - vue_set
    
    columns= ["f_id_mediamat","tranche_age","f_taille","f_pres_enf","csp_cm","activite_menagere","langue_prin","region", "milieu", "region_milieu","taille_commune", "nbr_tv", "f_type_offre", "stand_vie", "f_csp", "offre_milieu", "niv_diplome_cm","taille_milieu","num_region_administrative", "tv_online"  ]
    diff_absentes_table = []
    diff_absentes_vue = []



    if missing_lines_absentes_table:
        print("Lignes présentes dans la vue mais absentes dans la table:")
        
        for missing_line_table in missing_lines_absentes_table:
            infos_absentes_table = {}
            for col, val in zip(columns, missing_line_table):
                infos_absentes_table[col] = val
            diff_absentes_table.append(infos_absentes_table)
        print(diff_absentes_table)
                                                
    


    if missing_lines_absentes_vue:
        print("Lignes présentes dans la table mais absentes dans la vue:")
        
        for missing_line_vue in missing_lines_absentes_vue:
            infos_absentes_vue = {}
            for col, val in zip(columns, missing_line_vue):
                infos_absentes_vue[col] = val
            diff_absentes_vue.append(infos_absentes_vue)
    
    response_data = {"diff_absentes_table": diff_absentes_table, "diff_absentes_vue": diff_absentes_vue}
    return response_data





######################################################## Member ###########################################################################
def check_presence_monday_current_week_member_hist(db: Session = next(get_db())):
           monday_of_current_week = get_monday_of_current_week()
           households =db.query(modts.Member_triple_s_hist).filter(modts.Member_triple_s_hist.date == monday_of_current_week).all()
           
           if households:
            print("Query result is not empty:")
            return("yes")
            
           else:
            print("Query result is empty.")
            return("no")



def save_member_triple_s_hist(db: Session = next(get_db())):
           #Recuperer les données de la vue : HouseholdTripleS
           households = db.query(modts.MemberTripleS).all()
           monday_of_current_week = get_monday_of_current_week()
           for household in households:
             data = household.__dict__
             data.pop('_sa_instance_state')
             data['date'] = monday_of_current_week
             data['statut'] = 0
             hist_entry = modts.Member_triple_s_hist(**data)
             db.add(hist_entry)

           db.commit()



def update_member_triple_s_hist(db: Session = next(get_db())):
    # Recuperer les données de la vue : HouseholdTripleS
    households = db.query(modts.MemberTripleS).all()
    
    # Get the Monday of the current week
    monday_of_current_week = get_monday_of_current_week()
    
    db.query(modts.Member_triple_s_hist).filter(
        modts.Member_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        )
    ).update({"statut": modts.Member_triple_s_hist.statut + 1}, synchronize_session='fetch')
    
    # Add new rows with statut set to 0
    for household in households:
        data = household.__dict__
        data.pop('_sa_instance_state')
        data['date'] =datetime.date.today()
        data['statut'] = 0
        hist_entry = modts.Member_triple_s_hist(**data)
        db.add(hist_entry)

    db.commit()





def compare_vue_table_hist_member(db: Session = next(get_db())):
    monday_of_current_week = get_monday_of_current_week()

    #today= datetime.date.today()
    #monday_of_current_week = today - timedelta(days=today.weekday()-7)
    vue = db.query(modts.MemberTripleS).all()
    table= db.query(modts.Member_triple_s_hist).filter(
        modts.Member_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        ),modts.Member_triple_s_hist.statut==0
    ).all()
    # Utilisez des ensembles pour simplifier la comparaison (excluez la date)
    vue_set = {(h.id_indiv, h.touche_indiv, h.statut_indiv, h.sexe, h.tranche_2, h.tranche_3, h.tranche_5, h.tranche_1, h.age_sexe_1, h.age_sexe_3,h.activite, h.sexe_activite,  h.niveau_instruc, h.sexe_instruc, h.plus_cinq, h.age, h.is_menagere,h.is_maman,h.niv_diplome_indiv ) for h in vue}
    table_set = {(h.id_indiv, h.touche_indiv, h.statut_indiv, h.sexe, h.tranche_2, h.tranche_3, h.tranche_5, h.tranche_1, h.age_sexe_1, h.age_sexe_3,h.activite, h.sexe_activite,  h.niveau_instruc, h.sexe_instruc, h.plus_cinq, h.age, h.is_menagere,h.is_maman,h.niv_diplome_indiv  ) for h in table}


    # Identifiez les lignes présentes dans vue mais absentes dans table
    missing_lines_absentes_table = vue_set - table_set
    # Identifiez les lignes présentes dans table mais absentes dans vue
    missing_lines_absentes_vue = table_set - vue_set
    
    columns= ["id_indiv","touche_indiv","statut_indiv","sexe","tranche_2","tranche_3","tranche_5","tranche_1", "age_sexe_1", "age_sexe_3","activite", "sexe_activite", "niveau_instruc", "sexe_instruc", "plus_cinq", "age", "is_menagere","is_maman","niv_diplome_indiv" ]
    diff_absentes_table = []
    diff_absentes_vue = []



    if missing_lines_absentes_table:
        print("Lignes présentes dans la vue mais absentes dans la table:")
        
        for missing_line_table in missing_lines_absentes_table:
            infos_absentes_table = {}
            for col, val in zip(columns, missing_line_table):
                infos_absentes_table[col] = val
            diff_absentes_table.append(infos_absentes_table)
        print(diff_absentes_table)
                                                
    


    if missing_lines_absentes_vue:
        print("Lignes présentes dans la table mais absentes dans la vue:")
        
        for missing_line_vue in missing_lines_absentes_vue:
            infos_absentes_vue = {}
            for col, val in zip(columns, missing_line_vue):
                infos_absentes_vue[col] = val
            diff_absentes_vue.append(infos_absentes_vue)
    
    response_data = {"diff_absentes_table": diff_absentes_table, "diff_absentes_vue": diff_absentes_vue}
    return response_data




######################################################## Meter ###########################################################################
def check_presence_monday_current_week_meter_hist(db: Session = next(get_db())):
           monday_of_current_week = get_monday_of_current_week()
           households =db.query(modts.Meter_triple_s_hist).filter(modts.Meter_triple_s_hist.date == monday_of_current_week).all()
           
           if households:
            print("Query result is not empty:")
            return("yes")
            
           else:
            print("Query result is empty.")
            return("no")



def save_meter_triple_s_hist(db: Session = next(get_db())):
           #Recuperer les données de la vue : HouseholdTripleS
           households = db.query(modts.MeterTripleS).all()
           monday_of_current_week = get_monday_of_current_week()
           for household in households:
             data = household.__dict__
             data.pop('_sa_instance_state')
             data['date'] = monday_of_current_week
             data['statut'] = 0
             hist_entry = modts.Meter_triple_s_hist(**data)
             db.add(hist_entry)

           db.commit()



def update_meter_triple_s_hist(db: Session = next(get_db())):
    # Recuperer les données de la vue : HouseholdTripleS
    households = db.query(modts.MeterTripleS).all()
    
    # Get the Monday of the current week
    monday_of_current_week = get_monday_of_current_week()
    
    db.query(modts.Meter_triple_s_hist).filter(
        modts.Meter_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        )
    ).update({"statut": modts.Meter_triple_s_hist.statut + 1}, synchronize_session='fetch')
    
    # Add new rows with statut set to 0
    for household in households:
        data = household.__dict__
        data.pop('_sa_instance_state')
        data['date'] =datetime.date.today()
        data['statut'] = 0
        hist_entry = modts.Meter_triple_s_hist(**data)
        db.add(hist_entry)

    db.commit()





def compare_vue_table_hist_meter(db: Session = next(get_db())):
    monday_of_current_week = get_monday_of_current_week()

    #today= datetime.date.today()
    #monday_of_current_week = today - timedelta(days=today.weekday()-7)
    vue = db.query(modts.MeterTripleS).all()
    table= db.query(modts.Meter_triple_s_hist).filter(
        modts.Meter_triple_s_hist.date.between(
            monday_of_current_week, datetime.date.today()
        ),modts.Meter_triple_s_hist.statut==0
    ).all()
    # Utilisez des ensembles pour simplifier la comparaison (excluez la date)
    vue_set = {(h.id_view, h.num_poste, h.f_id_mediamat ) for h in vue}
    table_set = {(h.id_view, h.num_poste, h.f_id_mediamat  ) for h in table}


    # Identifiez les lignes présentes dans vue mais absentes dans table
    missing_lines_absentes_table = vue_set - table_set
    # Identifiez les lignes présentes dans table mais absentes dans vue
    missing_lines_absentes_vue = table_set - vue_set
    
    columns= ["id_view","num_poste","f_id_mediamat" ]
    diff_absentes_table = []
    diff_absentes_vue = []



    if missing_lines_absentes_table:
        print("Lignes présentes dans la vue mais absentes dans la table:")
        
        for missing_line_table in missing_lines_absentes_table:
            infos_absentes_table = {}
            for col, val in zip(columns, missing_line_table):
                infos_absentes_table[col] = val
            diff_absentes_table.append(infos_absentes_table)
        print(diff_absentes_table)
                                                
    


    if missing_lines_absentes_vue:
        print("Lignes présentes dans la table mais absentes dans la vue:")
        
        for missing_line_vue in missing_lines_absentes_vue:
            infos_absentes_vue = {}
            for col, val in zip(columns, missing_line_vue):
                infos_absentes_vue[col] = val
            diff_absentes_vue.append(infos_absentes_vue)
    
    response_data = {"diff_absentes_table": diff_absentes_table, "diff_absentes_vue": diff_absentes_vue}
    return response_data


############################################################### End Oumaima ##################################################################################################################################







######################################################################################################################################################################

def save(db: Session = next(get_db())):
    try:
        
        
        historical_records = pd.read_sql(db.query(modts.HouseholdTripleS).statement,db.bind)
        #print(historical_records)
        for record in historical_records:
            # Create an instance of House_hold_triple_s_hist
            print(record)
            historical_record = modts.House_hold_triple_s_hist()

            # Assign values based on the columns in HouseholdTripleS
            for column in modts.HouseholdTripleS.__view__.columns:
                setattr(historical_record, column.name, getattr(record, column.name))
                print(column)

            # Add the historical record to the session
            
            db.add(historical_record)

        # Commit the changes to the database
        db.commit()

       
        return historical_records

    except Exception as e:
        # Handle exceptions
        print(f"An error occurred: {e}")
        db.rollback()

    finally:
        db.close()






def save_triple_s_foyer(db: Session = next(get_db())):

        households_hist = db.query(modts.House_hold_triple_s_hist).all()
        data_hist = [entry for entry in households_hist]
        date_values = [entry.date.strftime('%Y%m%d') for entry in households_hist]
        date_values=set(date_values)
        today_date = datetime.date.today()
        is_today_included = today_date.strftime('%Y%m%d') in date_values

        if is_today_included:
            print("Today's date is included in the list.")
            households_filtered = db.query(modts.House_hold_triple_s_hist).filter(modts.House_hold_triple_s_hist.date==today_date).all()
           


           
            households = db.query(modts.HouseholdTripleS).all()
            for household in households:
             data = household.__dict__
             data.pop('_sa_instance_state')
             data['date'] = today_date
             data['statut'] = 0
             hist_entry = modts.House_hold_triple_s_hist(**data)
             db.add(hist_entry)

        else:
           households = db.query(modts.HouseholdTripleS).all()
           for household in households:
             data = household.__dict__
             data.pop('_sa_instance_state')
             data['date'] = today_date
             data['statut'] = 0
             hist_entry = modts.House_hold_triple_s_hist(**data)
             db.add(hist_entry)

           db.commit()





def get_triple_s_foyer_file(db: Session = next(get_db())):
          db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
          db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
          db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
          db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
          db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
          db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
          db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
          db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
          db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
          db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
          db.commit()
    
    
          household_df = pd.read_sql(db.query(modts.HouseholdTripleS).order_by(modts.HouseholdTripleS.f_id_mediamat).statement,db.bind)



          triple_s_household_file = household_df.to_csv(sep=" ",header=False,index=False)
          triple_s_household_file = triple_s_household_file.replace(" ","")
          triple_s_household_file = triple_s_household_file.replace(".0","")

          db.close()
 
          return triple_s_household_file
 



def get_triple_s_meter_file(db: Session = next(get_db())):
    db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
    db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
    db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
    db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
    db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
    db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
    db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
    db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
    db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
    db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
    db.commit()

    meter_df = pd.read_sql(db.query(modts.MeterTripleS.f_id_mediamat,modts.MeterTripleS.num_poste).statement,db.bind)
    meter_df['num_poste'] = meter_df['num_poste'].astype(int)-1
    triple_s_meter_file = meter_df.to_string(header=False,index=False)
    triple_s_meter_file = triple_s_meter_file.replace(" ","")
    triple_s_meter_file = triple_s_meter_file.replace(".0","")

    return triple_s_meter_file




def get_triple_s_indiv_file(db: Session = next(get_db())):

    db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
    db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
    db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
    db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
    db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
    db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
    db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
    db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
    db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
    db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
    db.commit()    
 
    indiv_df = pd.read_sql(db.query(modts.MemberTripleS).order_by(modts.MemberTripleS.f_id_mediamat,modts.MemberTripleS.id_indiv).statement,db.bind)


    triple_s_indiv_file = indiv_df.to_string(header=False,index=False)
    triple_s_indiv_file = triple_s_indiv_file.replace(" ","")
    triple_s_indiv_file = triple_s_indiv_file.replace(".0","")

    return triple_s_indiv_file

def getVar(Vars):
    variable=[]
    for var in Vars:
        variable.append(var)
    return variable

def getValues(var,num):
    vals = config.get(f'correspondance{num}.vars.{var}.vals')
    for k,v in vals.items():
        
        return v
def getValuesExp(var,num):
    vals = config.get(f'correspondance{num}.vars.{var}.vals')
    for k,v in vals.items():
        code=compile(v[0], "<string>", "eval")
        
        return eval(code)        
def correspondance(table,var1,value_var1,var2,value_var2,error_list, db: Session = next(get_db())):
    
    baseFoyer = pd.read_sql(db.query(modts.HouseholdTripleS).statement, db.bind)
    baseMember =pd.read_sql(db.query(modts.MemberTripleS).statement, db.bind)
    #la liste columns pour le but de stocker le nom de colonne qui contient la valeur 
    columns=[]
    #la liste corr pour le but de stocker la ligne qui contient la valeur 
    corr=[]
    if (table == 'house_hold_triple_s'):    
    
        #baseFoyer['categorie_inact'][baseFoyer['activite']==1]=69    
        for colonne in baseFoyer:
            if colonne==var1 :
                for key,value in baseFoyer[colonne].items():
                    #checker si la valeur matche la valeur dans le fichier de config 
                    if value in value_var1: 
                        corr.append(key)
                        columns.append(colonne)
            
            
            #cr������e un dictionnaire qui a comme key le nom de la colonne est comme valeur la liste des lignes
            corres={i:{j for j in corr} for i in columns}
            
            #si le dictionnaire est vide, pas besoin de faire le contr������le
            if len(corres)!=0: 
                
                #boucler sur notre dictionnaire 
                #col_name: les nom de colonnes
                #lignes: la liste des lignes
                for col_name,lignes in corres.items():
                    #boucler sur la liste des lignes
                    
                    for ligne in lignes:
                        
                        valeur=baseFoyer[f'{var2}'][ligne]
                        
                        if valeur in value_var2:
                            id_foyer= baseFoyer['f_id_mediamat'][ligne]
                            #display(id_foyer)
                            error_list.append(",".join([f"foyer",f'{id_foyer}',col_name,f'{baseFoyer[col_name][ligne]}',f'{var2}',f"{valeur}\n",]))
                        
            corr.clear()
            columns.clear()
    if (table == 'member_triple_s'):    
    
        #baseFoyer['categorie_inact'][baseFoyer['activite']==1]=69    
        for colonne in baseMember:
            if colonne==var1 :
                for key,value in baseMember[colonne].items():
                    #checker si la valeur matche la valeur dans le fichier de config 
                    if value in value_var1: 
                        corr.append(key)
                        columns.append(colonne)
            
            
            #cr������e un dictionnaire qui a comme key le nom de la colonne est comme valeur la liste des lignes
            corres={i:{j for j in corr} for i in columns}
            
            #si le dictionnaire est vide, pas besoin de faire le contr������le
            if len(corres)!=0: 
                
                #boucler sur notre dictionnaire 
                #col_name: les nom de colonnes
                #lignes: la liste des lignes
                for col_name,lignes in corres.items():
                    #boucler sur la liste des lignes
                    
                    for ligne in lignes:
                        
                        valeur=baseMember[f'{var2}'][ligne]
                        
                        if valeur in value_var2:

                            #Nq=cards_dic['card1']['Nq'][ligne]
                            id_indiv= baseMember['id_indiv'][ligne]
                           #display(id_indiv)
                            error_list.append(",".join([f"individu",f'{id_indiv}',col_name,f'{baseMember[col_name][ligne]}',f'{var2}',f"{valeur}\n",]))
                        
            corr.clear()
            columns.clear()


def execution_check(testdebut,testfin,error_list):
    #num est le nombre de correspondance dans le fichier config actuellement on a 161 
    for num in range(testdebut,testfin+1):
        
        Vars=config.get(f'correspondance{num}.vars')
        table=config.get(f'correspondance{num}.table.name')

        var1 = getVar(Vars)[0]
        
        #display(var1)
        
        var2 = getVar(Vars)[-1]
        
        #display(var2)
        
        value_var1 = getValues(var1,num)
        #display(value_var1)
        value_var2 = getValues(var2,num)
        #display(value_var2)
        correspondance(table,var1,value_var1,var2,value_var2,error_list)

def execution_check_exp(testdebut,testfin,error_list):
    #num est le nombre de correspondance dans le fichier config actuellement on a 161 
    for num in range(testdebut,testfin+1):
        
        Vars=config.get(f'correspondance{num}.vars')
        table=config.get(f'correspondance{num}.table.name')
        

        var1 = getVar(Vars)[0]
        
        #display(var1)
        
        var2 = getVar(Vars)[-1]
        
        #display(var2)
        
        value_var1 = getValuesExp(var1,num)
        #display(value_var1)
        value_var2 = getValues(var2,num)
        #display(value_var2)
        correspondance(table,var1,value_var1,var2,value_var2,error_list)

def anomalie(db: Session = next(get_db())):
    db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
    db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
    db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
    db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
    db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
    db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
    db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
    db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
    db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
    db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
    db.commit()
    
    error_list=[]
    #l'ajout des nom de colonne qu'on utilisera dans notre dataframe
    error_list.append("Table,id,Variable_1,valeur_variable_1,Variable_2,valeur_variable_2\n")
    #display(error_list)
    execution_check(1,9,error_list)
    execution_check_exp(60,75,error_list)

    #la convertion de la liste "error_list" en dataframe
    csv = ''.join(error_list)
    #display(csv)
    anomalie_df= pd.read_csv(StringIO(csv))
    
    anomalie_dict=anomalie_df.to_dict('records')
    #anomalie_df['valeur_variable_2']=anomalie_df['valeur_variable_2'].astype(int)

    #anomalie_df[f'{anomalie_df.columns[3]}']=anomalie_df[f'{anomalie_df.columns[3]}'].astype(int)
    #comme pr������vue ������craser la valeur 600 par une valeur significatif
    #anomalie_df.loc[anomalie_df.valeur_variable_2==600, 'valeur_variable_2'] = "non renseign������e"

    #anomalie_df.sort_values(by=['ligne_var1'],ascending=False)

    return anomalie_dict       
