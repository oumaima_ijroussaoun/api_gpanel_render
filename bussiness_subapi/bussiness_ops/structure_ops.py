import pandas as pd
import numpy as np
import datetime
import itertools
import threading

from sqlalchemy.sql.expression import and_ 
from sqlalchemy.orm import Session 

from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseAudimetre as modau
from ...models import ModelChefMenage as modcm
from ...models import ModelBaseCommune as modco
from ...models import ModelModalite as mod


from ...models import ModelStructure as modst


from . import get_db



def Theoriques(dataframe_th:pd.DataFrame,dataframe_BF:pd.DataFrame):
    
    dataframe_th['Théoriques #']=round(dataframe_th['Théoriques %']*len(dataframe_BF)/100)
    
    return dataframe_th

def countIF(dataframe_BF:pd.DataFrame):
    countif=[]
    for col_name in dataframe_BF.columns[1:]:
        
        list_values=dataframe_BF[col_name].value_counts(sort=False).sort_index().to_list()
        
        print(list_values)
        #if col_name==dataframe_BF.columns[1]:
            #list_values.append(sum(dataframe_BF[col_name].value_counts(sort=False).sort_index().to_list()[-2:]))
            #countif.append(list_values)
            
        #else:
        
        countif.append(list_values)
            
    merged = list(itertools.chain(*countif))
    return merged


def installe(dataframe_th:pd.DataFrame,dataframe_BF:pd.DataFrame):
    
    dataframe_th['Installé %']=round(dataframe_th['Insatllé #']/len(dataframe_BF)*100,2)
    
    return dataframe_th

def Ecart_relatif(dataframe_th):
    
    dataframe=pd.DataFrame()
    
    dataframe['Ecart Relatif'] = round(( (dataframe_th['Installé %']-dataframe_th['Théoriques %'])/dataframe_th['Théoriques %'] )*100,2)
    
    
    return dataframe

def Ecart_relatif2(dataframe_solver:pd.DataFrame,Ecart_Relatif_max:int):
    
    dataframe_solver['0(F)/1(V)']=dataframe_solver['Ecart Relatif'].apply(lambda x: 0 if abs(x) <= Ecart_Relatif_max else 1)
    
    return dataframe_solver

def Ecart_cible(dataframe_solver:pd.DataFrame,dataframe_th:pd.DataFrame):
    
    dataframe_solver['Ecart cible%']= round(abs((dataframe_solver['installé cible']-dataframe_th['Théoriques #'])/dataframe_th['Théoriques #'])*100,2)
    
    return dataframe_solver

def Th(df_th:pd.DataFrame,BT:int,BI:int):
    t_df=pd.DataFrame()
    
    BtxT= df_th['Théoriques %'].multiply(BT)
    BixI= df_th['Installé %'].multiply(BI)
    
    
    blo= df_th['Théoriques %']-df_th['Installé %']
    
    
    
    t_df['T']=round(1/np.sqrt( ((BtxT+BixI) * 1/(BT+BI) )* (100-(BtxT+BixI) * 1/(BT+BI) )  * (1/BT + 1/BI) )*abs(blo),2)
    
    
    return t_df

def Significativite(df_T:pd.DataFrame):
    
    df_T['Significativité']=df_T['T'].apply(lambda x: 'S.01' if x>=2.58 else ('S.05' if x>=1.96 else ('S.10' if x>=1.64 else('S.20' if x>=1.28 else 'N.S.' ) ) )   )
    
    
    return df_T



# Base_Theorique=7758
# Ecart_Relatif_max=15


def calcul_structure(Base_Theorique:int,date_theo:str,Ecart_Relatif_max:int,descri:str,db: Session = next(get_db())):
    # db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
    # db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
    # db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
    # db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
    # db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
    # db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
    # db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
    # db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
    # db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
    # db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
    # db.commit()
    

    #struct_dict={}

    baseFoyer=pd.read_sql(db.query(modst.StructureView).statement,db.bind)
    print(baseFoyer)
    structure_theorique=pd.read_sql(db.query(modst.StructureTheorique.struct_theo).order_by(modst.StructureTheorique.struct_date.desc()).statement,db.bind)
    
    theorique = pd.DataFrame(eval(structure_theorique['struct_theo'][0]))
    print(theorique)
    theorique = Theoriques(theorique,baseFoyer)

    


    theorique['Insatllé #']=countIF(baseFoyer)

    theorique = installe(theorique,baseFoyer)
    print(theorique)
    
    #struct_dict['theorique']=theorique.to_dict("records")


    Solver = Ecart_relatif(theorique)

    Solver = Ecart_relatif2(Solver,Ecart_Relatif_max)

    Solver['installé cible']=theorique['Insatllé #']

    Solver=Ecart_cible(Solver,theorique)

    #struct_dict["Solver"]=Solver.to_dict("records")


    significative = Th(theorique,Base_Theorique,len(baseFoyer))

    significative = Significativite(significative)

    #struct_dict["significative"]= significative.to_dict("records")
    struct=pd.DataFrame()
    struct=pd.concat([theorique,Solver,significative] ,axis=1, join='inner')
    infos=pd.DataFrame([f'Nombre des foyers : {len(baseFoyer)}',f'Ecart reltif max : {Ecart_Relatif_max}',f'Base theorique : {Base_Theorique}',f'Date theorique : {date_theo}',f'Description : {descri}'])
    infos.columns=['strat']
    struct=pd.concat([theorique,Solver,significative] ,axis=1, join='inner')
    struct=pd.concat([struct,infos] ,axis=0)
    struct=struct.fillna('')
    struct_dict= struct.to_dict("records")

    return struct_dict

def calculus_save(db: Session = next(get_db())):
    try:
        db_calc = modst.StructureCalculs(struct_calc_date=datetime.datetime.today(),struct_calc=str(calcul_structure(7758,'2021-08-02',15,'Fusion 2019 HCP ENE 2018 actualisé 2019',db=db)))
  
        db.add(db_calc)
        
        
    except:
        db.rollback()
        db.close()
        raise    
    else:
        db.commit()

# def auto_save_calculs(db: Session = next(get_db())):

#     # x=datetime.today()
#     # y=x.replace(day=x.day+1, hour=18, minute=0, second=0, microsecond=0)
#     # delta_t=y-x

#     # secs=delta_t.seconds+1

#     t = threading.Timer(5, lambda: print('oooooooooo'))
#     await asyncio.sleep(1)
#     t.start()




