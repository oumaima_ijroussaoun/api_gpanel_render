import pandas as pd
from pyhocon import ConfigFactory
from loguru import logger
from datetime import date,datetime
import sys
import re
import numpy as np
from io import StringIO
import time
import json
#import xmlschema

from sqlalchemy.sql.expression import and_ 
from sqlalchemy.orm import Session, session




from ...models import ModelBaseFoyer as modfoy
from ...models import ModelBaseIndividu as modind
from ...models import ModelBasePoste as modpo
from ...models import ModelBaseAudimetre as modau
from ...models import ModelChefMenage as modcm
from ...models import ModelBaseCommune as modco
from ...models import ModelModalite as mod

from ...models import ModelTripleS as modts
from ...models import ModelControles as modctr
from ...database import Base



from . import get_db



# config = ConfigFactory.parse_file('/home/mcm-hive/Desktop/hive/api_layer/general_api/test_db_mod/bussiness_subapi/bussiness_config/config_controles.conf')
config = ConfigFactory.parse_file('/home/issam/Desktop/mcm/projects/mcm-hive/api_layer/general_api/general/bussiness_subapi/bussiness_config/config_controles.conf')

tables_dict={'house_hold_triple_s':modts.HouseholdTripleS,'controles_view':modctr.ControlesView,
'individu':modind.Individu,'member_triple_s':modts.MemberTripleS,'foyer':modfoy.Foyer,'sociodemo':modfoy.Sociodemo,
'poste':modpo.Poste,'equipement_poste':modpo.EquipementPoste,
'effectif_foyer':modfoy.EffectifFoyer,'equipement':modfoy.Equipement,'contact_foyer':modfoy.ContactFoyer,'commune':modco.Commune
,'chef_menage':modcm.ChefMenage}



def map_modalite(db: Session = next(get_db())):
    modalite_df =  pd.read_sql(db.query(mod.Modalite.nom_var,mod.Modalite.num_modalite,mod.Modalite.label_modalite).statement,db.bind)
    modalite_dict = {k: val.groupby('num_modalite')['label_modalite'].apply(lambda x: x.to_string(index = False)).to_dict()
     for k, val in modalite_df.groupby('nom_var')}
    db.close()
    return modalite_dict


def getVarMod(numvar):
   return config.get(f'controles_modalite.{numvar}.num_modalite')

def getNomVar(numvar):
    return config.get(f'controles_modalite.{numvar}.var')
def getTable(numvar):
    return config.get(f'controles_modalite.{numvar}.table')
def getId(numvar):
    return config.get(f'controles_modalite.{numvar}.id')
def check_modalite(error_list ,db: Session = next(get_db())):

    controles_modalite=config.get(f'controles_modalite')
    for ctr in controles_modalite :
        nom_var=getNomVar(ctr)
        modalite=getVarMod(ctr) 
        table=getTable(ctr)
        my_id=getId(ctr)
        if table in ['sociodemo','effectif_foyer','equipement','contact_foyer','poste','individu']:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement.join(modfoy.Foyer).filter(modfoy.Foyer.f_etat!=5), db.bind)
        elif  table in ['chef_menage']:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement.join(modind.Individu,modcm.ChefMenage.id_indiv==modind.Individu.id_indiv).join(modfoy.Foyer,modind.Individu.f_num_con==modfoy.Foyer.f_num_con).filter(modfoy.Foyer.f_etat!=5), db.bind)
        elif  table in ['foyer']:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement.filter(modfoy.Foyer.f_etat!=5), db.bind)
        elif  table in ['equipement_poste']:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement.join(modpo.Poste,modpo.Poste.id_poste==modpo.EquipementPoste.id_poste).join(modfoy.Foyer,modpo.Poste.f_num_con==modfoy.Foyer.f_num_con).filter(modfoy.Foyer.f_etat!=5), db.bind) 
        else:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement, db.bind) 


        base=base.fillna('9977')
        for column in base:
            if column == nom_var:
                for key,value in base[column].items():
                    if not(value in modalite) :
                        id= base[my_id][key]
                            #display(id_indiv)
                        error_list.append(",".join([f'{table}',f'{my_id} ', f'{id}',column,f'{base[column][key]}\n',]))
                            


    


def getVar(Vars):
    variable=[]
    for var in Vars:
        variable.append(var)
    return variable

def getValues(var,num):
    vals = config.get(f'correspondance{num}.vars.{var}.vals')
    for k,v in vals.items():
        
        return v
def getValuesExp(var,num):
    vals = config.get(f'correspondance{num}.vars.{var}.vals')
    for k,v in vals.items():
        code=compile(v[0], "<string>", "eval")
        
        return eval(code)        
def correspondance(table,var1,value_var1,var2,value_var2,error_list,my_id,niveau_corr, db: Session = next(get_db())):

    if table in ['sociodemo','effectif_foyer','equipement','contact_foyer','poste','individu']:
            base = pd.read_sql(db.query(tables_dict.get(table)).statement.join(modfoy.Foyer).filter(modfoy.Foyer.f_etat!=5), db.bind)
    else:        
            base = pd.read_sql(db.query(tables_dict.get(table)).statement, db.bind)
    
    #la liste columns pour le but de stocker le nom de colonne qui contient la valeur 
    columns=[]
    #la liste corr pour le but de stocker la ligne qui contient la valeur 
    corr=[]
   
        
    for colonne in base:
            if colonne==var1 :
                for key,value in base[colonne].items():
                    #checker si la valeur matche la valeur dans le fichier de config 
                    if value in value_var1: 
                        corr.append(key)
                        columns.append(colonne)
            
            
            #cr������������������������������������������������������e un dictionnaire qui a comme key le nom de la colonne est comme valeur la liste des lignes
            corres={i:{j for j in corr} for i in columns}
            
            #si le dictionnaire est vide, pas besoin de faire le contr������������������������������������������������������le
            if len(corres)!=0: 
                
                #boucler sur notre dictionnaire 
                #col_name: les nom de colonnes
                #lignes: la liste des lignes
                for col_name,lignes in corres.items():
                    #boucler sur la liste des lignes
                    
                    for ligne in lignes:
                        
                        valeur=base[f'{var2}'][ligne]
                        
                        if valeur in value_var2:

                            #Nq=cards_dic['card1']['Nq'][ligne]
                            id= base[my_id][ligne]
                           #display(id_indiv)
                            error_list.append(",".join([f'{niveau_corr}',f'{table}',f'{my_id} ', f'{id}',col_name,f'{base[col_name][ligne]}',f'{var2}',f"{valeur}\n",]))
                        
            corr.clear()
            columns.clear()


def execution_check(testdebut,testfin,error_list):
    #num est le nombre de correspondance dans le fichier config actuellement on a 161 
    for num in range(testdebut,testfin+1):
        try:
            if num in range(1,100):
                niv_corr='������lev������' #corr critiques
            elif num in range(100,200):
                niv_corr='moyen' #corr moy
            else:
                niv_corr='bas' #corr min
            
            Vars=config.get(f'correspondance{num}.vars')
            table=config.get(f'correspondance{num}.table.name')
            id=config.get(f'correspondance{num}.table.id')

            var1 = getVar(Vars)[0]
            
            #display(var1)
            
            var2 = getVar(Vars)[-1]
            
            #display(var2)
            
            value_var1 = getValues(var1,num)
            #display(value_var1)
            value_var2 = getValues(var2,num)
            #display(value_var2)
            correspondance(table,var1,value_var1,var2,value_var2,error_list,id,niv_corr)
        except: continue    



def execution_check_exp(testdebut,testfin,error_list):
    #num est le nombre de correspondance dans le fichier config actuellement on a 161 
    for num in range(testdebut,testfin+1):
        try:
            if num in range(1,100):
                niv_corr='������lev������' #corr critiques
            elif num in range(100,200):
                niv_corr='moyen' #corr moy
            else:
                niv_corr='bas' #corr min

            Vars=config.get(f'correspondance_exp{num}.vars')
            table=config.get(f'correspondance_exp{num}.table.name')
            id=config.get(f'correspondance_exp{num}.table.id')


            var1 = getVar(Vars)[0]

            #display(var1)

            var2 = getVar(Vars)[-1]

            #display(var2)

            value_var1 = getValuesExp(var1,num)
            #display(value_var1)
            value_var2 = getValues(var2,num)
            #display(value_var2)
            correspondance(table,var1,value_var1,var2,value_var2,error_list,id,niv_corr)
        except: continue    
# def check_modalite():
#    var_modalite= {x.var:getMod(x.var) for x in config.get(f'controles_modalite')} 




def anomalie(db: Session = next(get_db())):
    db.query(modfoy.ContactFoyer).update({modfoy.ContactFoyer.f_num_con:modfoy.ContactFoyer.f_num_con})
    db.query(modfoy.EffectifFoyer).update({modfoy.EffectifFoyer.f_num_con:modfoy.EffectifFoyer.f_num_con})
    db.query(modfoy.Sociodemo).update({modfoy.Sociodemo.f_num_con:modfoy.Sociodemo.f_num_con})
    db.query(modfoy.Equipement).update({modfoy.Equipement.f_num_con:modfoy.Equipement.f_num_con})
    db.query(modind.Individu).update({modind.Individu.f_num_con:modind.Individu.f_num_con})
    db.query(modcm.ChefMenage).update({modcm.ChefMenage.id_indiv:modcm.ChefMenage.id_indiv})
    db.query(modpo.Poste).update({modpo.Poste.f_num_con:modpo.Poste.f_num_con})
    db.query(modco.Commune).update({modco.Commune.nom_commune:modco.Commune.nom_commune})
    db.query(modco.Region).update({modco.Region.num_region_administrative:modco.Region.num_region_administrative})
    db.query(modco.Ville).update({modco.Ville.ville:modco.Ville.ville})   
    db.commit()
    
    recrut=pd.read_sql(db.query(modfoy.Foyer.f_num_con).statement.filter(modfoy.Foyer.num_recrut=='7676767'), db.bind)
    rec_err=pd.DataFrame()
    rec_err['id']=recrut["f_num_con"]
    rec_err['Variable']="num_recrut"
    rec_err['valeur_variable']="7676767"
    #rec_err['Table']="foyer"
    #rec_err['nom_id']= "f_num_con"
    rec_err.insert(0, "Table", "foyer")
    rec_err.insert(1, "nom_id", "f_num_con")
    # start = time.time()
    error_list=[]
    error_modalite=[]
    anomalie_dict={}
    #l'ajout des nom de colonne qu'on utilisera dans notre dataframe
    error_list.append("Niveau_controles,Table,nom_id,id,Variable_1,valeur_variable_1,Variable_2,valeur_variable_2\n")
    error_modalite.append("Table,nom_id,id,Variable,valeur_variable\n")
    check_modalite(error_modalite)
    for elt in rec_err.values:
    	  #print(elt)
       error_modalite.append(",".join([f'{elt[0]}',f'{elt[1]}',f'{elt[2]}',f'{elt[3]}',f'{elt[4]}\n',]))
         

    execution_check(1,300,error_list)
    execution_check_exp(1,300,error_list)

    #la convertion de la liste "error_list" en dataframe
    csv = ''.join(error_list)
    modalite_csv=''.join(error_modalite)
   
    anomalie_df= pd.read_csv(StringIO(csv))
    mod_df= pd.read_csv(StringIO(modalite_csv))

    modalite_dict = map_modalite()

    anomalie_df = anomalie_df.replace(modalite_dict).fillna('') 
    
    anomalie_dict['corresp']=anomalie_df.to_dict('records')
    anomalie_dict['modalite']=mod_df.to_dict('records')
    # end = time.time()
    # anomalie_dict['execution_time']=start-end
   

    return anomalie_dict       