from pydantic import BaseModel
from typing import Optional
import datetime as d



################################################################## rpa foyer########################################################

class HouseholdRPA(BaseModel):
    f_num_con :int
    f_id_mediamat :int
    mode_trait_f :int
    class Config:
        orm_mode = True 





################################################################## rpa individu########################################################

class MemberRPA(BaseModel):
    id_indiv :int
    touche_indiv :int
    mode_trait_indiv :int
    f_id_mediamat :int

    class Config:
        orm_mode = True

################################################################## rpa individu########################################################

class PermanentMemberRPA(BaseModel):
    id_indiv :int
    touche_indiv :int
    mode_trait_indiv :int
    class Config:
        orm_mode = True
    
################################################################## rpa poste########################################################
class MeterRPA(BaseModel):


    id_meter : str
    num_poste : int
    mode_trait_poste : int
    f_id_mediamat : int
    
    class Config:
        orm_mode = True 


