from fastapi import APIRouter,Response
from typing import List


from .. import bussiness_extra_schemas as extshem
from ..bussiness_ops import controles_ops as ops
from ...gpanel_subapi.gpanel_crud import gpanel_get_crud as gpcrud


controles_route=APIRouter(prefix="/controles")

@controles_route.get("/controle")
def controle():
    dict={}
    # dict['count']=gpcrud.get_count()
    # dict['count_foy']=gpcrud.get_count_foy()
    # dict['count_ind']=gpcrud.get_count_ind()
    dict['anomalies']=ops.anomalie()

    return dict

@controles_route.get("/controle/r")  
def test():
   return ops.table_object()