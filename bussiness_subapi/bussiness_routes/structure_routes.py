from fastapi import APIRouter,Response,BackgroundTasks
from fastapi_utils.tasks import repeat_every

from typing import List
import threading


from .. import bussiness_extra_schemas as extshem
from ..bussiness_ops import structure_ops as struc

structure_route= APIRouter(prefix="/structure")



@structure_route.get("/get/structData",description="calcule de la structure")
def get_struct_data(Base_Theorique:int,Ecart_Relatif_max:int):
    return struc.calcul_structure(Base_Theorique,'2021-08-02',Ecart_Relatif_max,'Fusion 2019 HCP ENE 2018 actualisé 2019')

