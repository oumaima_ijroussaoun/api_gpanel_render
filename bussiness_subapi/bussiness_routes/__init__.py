from ...bussiness_subapi.bussiness_ops import structure_ops


def include_route(app):
    
    app.include_router(RPA_routes.RPA_route)
    app.include_router(triple_s_routes.triple_s_route)
    app.include_router(structure_routes.structure_route)
    app.include_router(controles_routes.controles_route)


    