from fastapi import APIRouter,Response
from typing import List


from .. import bussiness_extra_schemas as extshem
from ..bussiness_ops import triple_s_ops as trps
from ...gpanel_subapi.gpanel_crud import gpanel_get_crud as gpcrud


triple_s_route=APIRouter(prefix="/triple_s")



##################### get var and modalite 

@triple_s_route.get("/get/household")
def create_house_hold():
    return Response(content=trps.get_triple_s_foyer_file(), media_type="application/atom+xml")

@triple_s_route.get("/get/member")
def create_individual():
    return Response(content=trps.get_triple_s_indiv_file(), media_type="application/atom+xml")


@triple_s_route.get("/get/meter")
def create_meter():
    return Response(content=trps.get_triple_s_meter_file(), media_type="application/atom+xml")

@triple_s_route.get("/controle")
def controle():
    dict={}
    dict['count']=gpcrud.get_count()
    dict['count_foy']=gpcrud.get_count_foy()
    dict['count_ind']=gpcrud.get_count_ind()
    dict['anomalies']=trps.anomalie()

    return dict
# @triple_s_route.get("/get/triple_s")
# def create_ts():
#     return Response(content=trps.get_triple_s_file(), media_type="application/atom+xml")



#Oumaima
###################################### HouseHold #################################################
@triple_s_route.get("/get/save_household_triple_s_hist")
def save_household_triple_s_hist():
    return Response(content=trps.save_household_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/update_household_triple_s_hist")
def update_household_triple_s_hist():
    return Response(content=trps.update_household_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/household_hist_ckeck_date")
def check_presence_monday_current_week_household_hist():
    return Response(content=trps.check_presence_monday_current_week_household_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/compare_vue_table_hist_household")
def compare_vue_table_hist_household():
    return trps.compare_vue_table_hist_household()



###################################### Member #################################################
@triple_s_route.get("/get/save_member_triple_s_hist")
def save_member_triple_s_hist():
    return Response(content=trps.save_member_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/update_member_triple_s_hist")
def update_member_triple_s_hist():
    return Response(content=trps.update_member_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/member_hist_ckeck_date")
def check_presence_monday_current_week_member_hist():
    return Response(content=trps.check_presence_monday_current_week_member_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/compare_vue_table_hist_member")
def compare_vue_table_hist_member():
    return trps.compare_vue_table_hist_member()



###################################### Meter #################################################

@triple_s_route.get("/get/save_meter_triple_s_hist")
def save_meter_triple_s_hist():
    return Response(content=trps.save_meter_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/update_meter_triple_s_hist")
def update_meter_triple_s_hist():
    return Response(content=trps.update_meter_triple_s_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/meter_hist_ckeck_date")
def check_presence_monday_current_week_meter_hist():
    return Response(content=trps.check_presence_monday_current_week_meter_hist(), media_type="application/atom+xml")


@triple_s_route.get("/get/compare_vue_table_hist_meter")
def compare_vue_table_hist_meter():
    return trps.compare_vue_table_hist_meter()
