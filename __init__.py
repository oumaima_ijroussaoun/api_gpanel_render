from . import logging_subapi
from fastapi.middleware.cors import CORSMiddleware


from .refrence_subapi.main import refrenceapp
from .gpanel_subapi.main import gpanelsubapi
from .bussiness_subapi.main import bussiness_subapi
from .logging_subapi.main import loggingsubapi


def cros_app(app):
    origins = [
    
    "*"
    ]

    app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    )



def mount_app(app):
    app.mount("/refrencesubapi",refrenceapp)
    app.mount("/gpanelsubapi",gpanelsubapi)
    app.mount("/bussinesssubapi",bussiness_subapi)
    app.mount("/loggingsubapi",loggingsubapi)
