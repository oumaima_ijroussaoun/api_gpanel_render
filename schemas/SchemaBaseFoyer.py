from pydantic import BaseModel
from typing import Optional,List
import datetime as d



##################################################################foyer########################################################
class CreateFoyer(BaseModel):
    
    f_id_mediamat: int
    f_nom: str
    f_etat: int
    f_csp: int
    num_recrut: str
    date_recrut: d.datetime
    vague_recrut: int
    f_date_deb: d.datetime
    mode_trait_f: int
    nom_installateur: str
    telephones:List[str]
    tv_online: int
    motivation:Optional[int] 
    
    class Config:
	    orm_mode=True
class GetFoyer(CreateFoyer):
    f_num_con: int
    f_date_fin: Optional[d.datetime]
    motif_sorti: Optional[int]
    detail_motif_sorti: Optional[str]
    num_recrut_uni: Optional[str]
    

    class Config:
        orm_mode = True
class DeleteFoyer(GetFoyer):
    
    class Config:
	    orm_mode=True
class UpdateFoyer(GetFoyer):
    
    class Config:
	    orm_mode=True



class CreateContactFoyer(BaseModel):
    f_num_con : int 
    date_install_telfix :Optional[d.datetime]
    f_adr1 :str
    longitude :Optional[float]
    latitude :Optional[float]
    id_commune :int
    f_adr2 :str
    

    class Config:
        orm_mode = True
class GetContactFoyer(CreateContactFoyer):
    region_milieu : int
    class Config:
	    orm_mode=True
class DeleteContactFoyer(GetContactFoyer):
    
    class Config:
	    orm_mode=True
class UpdateContactFoyer(GetContactFoyer):
    
    class Config:
	    orm_mode=True


class CreateEffectifFoyer(BaseModel):
    f_num_con :int
    class Config:
	    orm_mode=True
class GetEffectifFoyer(CreateEffectifFoyer):
  
    f_nbr_emp_res :int
    nbr_inv_regulier :int
    nbr_employ_non_res :int
    nbr_fam_ami_res :int
    total_touche_prog :int
    total_membre :int
    f_taille:int
    f_nbr_enf :int
    f_pres_enf :bool

    class Config:
        orm_mode = True
class DeleteEffectifFoyer(GetEffectifFoyer):
    
    class Config:
	    orm_mode=True
class UpdateEffectifFoyer(GetEffectifFoyer):
    
    class Config:
	    orm_mode=True


class CreateEquipement(BaseModel):
    

    f_num_con :int
    nbr_clim :int
    nbr_app_chauf :int
    nbr_refrig :int
    nbr_congel :int
    nbr_four_micro :int
    nbr_cuisiniere :int
    nbr_lave_linge :int
    nbr_lave_vaiss :int
    nbr_aspi :int
    poss_internet :bool
    poss_tnt:bool
    source_electricite :int
    nbr_tv :int
    nbr_vehicule :int
    nbr_tel_mob :int
    nbr_tel_fix :int
    nbr_ordi :int
    nbr_radio :int
    nbr_ordi_emp :int
    nbr_home_cinema :int
    nbr_deco_sat :int
    nbr_console_jeux_se :int
    nbr_console_jeux_ae :int 
    nbr_tablette :int
   
    class Config:
        orm_mode = True   
class GetEquipement(CreateEquipement):
    
    class Config:
	    orm_mode=True
class DeleteEquipement(CreateEquipement):
    
    class Config:
	    orm_mode=True
class UpdateEquipement(CreateEquipement):
    
    class Config:
	    orm_mode=True

class CreateSociodemo(BaseModel):
 

    f_num_con : int
    f_type_offre : int
    f_tranche_revenu : int
    type_hab : int
    standing_hab : int
    pos_res_sec : int 
    langue_prin : int
    stand_vie : int
    class Config:
        orm_mode = True
class GetSociodemo(CreateSociodemo):
    
    class Config:
	    orm_mode=True
class DeleteSociodemo(CreateSociodemo):
    
    class Config:
	    orm_mode=True
class UpdateSociodemo(CreateSociodemo):
    
    class Config:
	    orm_mode=True


# class CreateTel(BaseModel):
    
#     f_num_con :int
#     num_tel :str
#     class Config:
#         orm_mode = True
# class GetTel(CreateTel):
#     id_tel:int
#     class Config:
# 	    orm_mode=True
# class DeleteTel(GetTel):
    
#     class Config:
# 	    orm_mode=True
# class UpdateTel(GetTel):
    
#     class Config:
# 	    orm_mode=True

##################################################################foyer########################################################
