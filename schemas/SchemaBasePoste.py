from pydantic import BaseModel
from typing import List, Optional
import datetime as d






###############################################poste#######################################################################
class CreatePoste(BaseModel):
    id_poste:int
    num_poste: int
    etat_actuel: int
    marque_tv : str
    model_tv : str
    tv_non_installe :Optional[bool]
    motif_tv_non_installe:Optional[int]
    dimension_tv:int
    type_ecran:int
    taille_ecran:int
    anciennete_tv:int
    statut_poste: int
    mode_trait_poste: int
   
    f_num_con :int
    emplacement: int
    date_installation_poste :Optional[d.datetime]

    class Config:
        orm_mode = True
class GetPoste(CreatePoste):
    
    class Config:
	    orm_mode=True
class DeletePoste(GetPoste):
    
    class Config:
	    orm_mode=True
class UpdatePoste(GetPoste):
   
    class Config:
	    orm_mode=True



class CreateEquipementPoste(BaseModel):
    
    id_poste:int
    decodeur_sat_num:int
    decodeur_adsl:int
    home_cinema:int
    antenne_parab:int
    type_antenne_parab:int
    reception_adsl:bool
    type_sonde_poste:int
    equipe_tnt:int
    class Config:
        orm_mode = True
class GetEquipementPoste(CreateEquipementPoste):
    
    class Config:
	    orm_mode=True
class DeleteEquipementPoste(CreateEquipementPoste):
    
    class Config:
	    orm_mode=True
class UpdateEquipementPoste(CreateEquipementPoste):
   
    class Config:
	    orm_mode=True





    ###############################################recetion#######################################################################
###############################################reception#######################################################################
class CreateReception(BaseModel):
    
    
    id_poste:int
    type_reception:List[int]
    reception_chaine:List[int]
    reception_sat:List[int]
    class Config:
        orm_mode = True
class GetReception(CreateReception):

   
    class Config:
	    orm_mode=True
class DeleteReception(GetReception):
    
    class Config:
	    orm_mode=True
class UpdateReception(GetReception):
   
    class Config:
	    orm_mode=True