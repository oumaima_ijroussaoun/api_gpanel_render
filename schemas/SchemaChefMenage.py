
from pydantic import BaseModel
from typing import Optional
import datetime as d



class CreateChefMenage(BaseModel):
    
    id_indiv :int
    profession_cm :int
    csp_cm :int
    activite_menagere:int
    class Config:
        orm_mode = True
class GetChefMenage(CreateChefMenage):
    tranche_age:int
    
    class Config:
	    orm_mode=True
class DeleteChefMenage(GetChefMenage):
    
    class Config:
	    orm_mode=True
class UpdateChefMenage(GetChefMenage):
    
    class Config:
	    orm_mode=True
