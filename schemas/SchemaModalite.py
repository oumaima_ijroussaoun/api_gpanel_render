from pydantic import BaseModel




class CreateModalite(BaseModel):

    id_modalite :int

    id_var:int
    id_table:int
    id_table_var:int
    table_var:str
    nom_var:str
    num_modalite:int
    type_var:str
    label_modalite:str

    class Config:
	    orm_mode=True
class GetModalite(CreateModalite):
    pass
    class Config:
	    orm_mode=True
class UpdateModalite(CreateModalite):
    pass
    class Config:
	    orm_mode=True
class DeleteModalite(CreateModalite):
    pass   
    class Config:
	    orm_mode=True     
