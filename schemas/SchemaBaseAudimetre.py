
from typing import Optional
from pydantic import BaseModel


class CreateAudimetre(BaseModel):
    
    id_meter:str
    statut_meter: int
    type_meter: int
    class Config:
        orm_mode = True
class GetAudimetre(CreateAudimetre):
    
    class Config:
	    orm_mode=True
class DeleteAudimetre(CreateAudimetre):
    
    class Config:
	    orm_mode=True
class UpdateAudimetre(CreateAudimetre):
   
    class Config:
	    orm_mode=True

class CreateAudimetreInstalle(BaseModel):
    id_meter :str
    id_poste :int
    sortie_audio :Optional[int] 
    capture_audio :Optional[int]
    nom_installateur :Optional[str]
    
    class Config:
        orm_mode = True
class GetAudimetreInstalle(CreateAudimetreInstalle):
    
    class Config:
	    orm_mode=True
class DeleteAudimetreInstalle(CreateAudimetreInstalle):
    
    class Config:
	    orm_mode=True
class UpdateAudimetreInstalle(CreateAudimetreInstalle):
   
    class Config:
	    orm_mode=True