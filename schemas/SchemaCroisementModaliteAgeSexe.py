
from pydantic import BaseModel
from typing import Optional
import datetime as d



###########################################################CreateCroisementAgeSexe###############################################################

class CreateCroisementAgeSexe(BaseModel):
    
    label_crois :str
    class Config:
        orm_mode = True
class GetCroisementAgeSexe(CreateCroisementAgeSexe):
    id_crois_seq :int
    class Config:
	    orm_mode=True
class DeleteCroisementAgeSexe(GetCroisementAgeSexe):
    
    class Config:
	    orm_mode=True
class UpdateCroisementAgeSexe(GetCroisementAgeSexe):
    
    class Config:
	    orm_mode=True

###########################################################ModaliteAgeSexe###############################################################

class CreateModaliteAgeSexe(BaseModel):
    age_max :int
    age_min :int
    sexe :int
    num_mod_crois :int
    id_crois_seq :int
    class Config:
        orm_mode = True   
class GetModaliteAgeSexe(CreateModaliteAgeSexe):
    id_mod_crois_seq:int
    class Config:
	    orm_mode=True
class DeleteModaliteAgeSexe(GetModaliteAgeSexe):
    
    class Config:
	    orm_mode=True
class UpdateModaliteAgeSexe(GetModaliteAgeSexe):
    
    class Config:
	    orm_mode=True