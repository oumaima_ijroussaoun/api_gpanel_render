
from pydantic import BaseModel
from typing import Optional
import datetime as d



class CreateCommune(BaseModel):
    id_commune : int
    nom_commune :str
    nom_zone : str
    num_zone : int
    taille_commune : int
    milieu : int
    class Config:
        orm_mode = True  
class GetCommune(CreateCommune):
    
    class Config:
	    orm_mode=True
class DeleteCommune(CreateCommune):
    
    class Config:
	    orm_mode=True
class UpdateCommune(CreateCommune):
    
    class Config:
	    orm_mode=True

class CreateRegion(BaseModel):
    id_region : int
    region : str
    region_administrative : int
    id_commune :int
    class Config:
        orm_mode = True   
class GetRegion(CreateRegion):
    
    class Config:
	    orm_mode=True
class DeleteRegion(CreateRegion):
    
    class Config:
	    orm_mode=True
class UpdateRegion(CreateRegion):
    
    class Config:
	    orm_mode=True

class CreateVille(BaseModel):
  

    id_ville :int
    ville : str
    id_commune : int
    class Config:
        orm_mode = True   
class GetVille(CreateVille):
    
    class Config:
	    orm_mode=True
class DeleteVille(CreateVille):
    
    class Config:
	    orm_mode=True
class UpdateVille(CreateVille):
    
    class Config:
	    orm_mode=True


class InstallateursCommune(BaseModel):


    
    installateur: str
    nom_commune : list
    class Config:
	    orm_mode=True