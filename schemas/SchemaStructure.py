from pydantic import BaseModel
import datetime as D

from pydantic.types import Json



class CreateStructureTheorique(BaseModel):

   
    id_structure : int
    struct_date : D.datetime
    struct_theo : Json

    class Config:
	    orm_mode=True
class GetStructureTheorique(CreateStructureTheorique):
    pass
    class Config:
	    orm_mode=True
class UpdateStructureTheorique(CreateStructureTheorique):
    pass
    class Config:
	    orm_mode=True
class DeleteStructureTheorique(CreateStructureTheorique):
    pass   
    class Config:
	    orm_mode=True     



class CreateStructureCalculs(BaseModel):

   
    struct_calc_date : D.datetime
    struct_calc : Json

    class Config:
	    orm_mode=True
class GetStructureCalculs(CreateStructureCalculs):
    pass
    class Config:
	    orm_mode=True
class UpdateStructureCalculs(CreateStructureCalculs):
    pass
    class Config:
	    orm_mode=True
class DeleteSStructureCalculs(CreateStructureCalculs):
    pass   
    class Config:
	    orm_mode=True 
