
from pydantic import BaseModel
from typing import List, Optional
import datetime as d



class CreateIndividu(BaseModel):
    id_indiv: int
    nom: str
    prenom: str
    date_naiss: d.datetime
    telephones_indiv:Optional[List[str]]
    touche_indiv: int
    niveau_instruc_glob: int
    categorie_indiv: int
    
    sexe: int
    etat: int
    statut_indiv:int
    
    categorie_inact: Optional[int]
    activite: bool
    enfant_touche_mere: Optional[int]
    is_cm: bool
    is_menagere: bool
    is_maman: bool
    is_epouse: Optional[bool]
    is_aide_domestique: bool
    voyage_etrg : int
    but_voy :Optional[str] 
    f_num_con: int
  
    
    
    mode_trait_indiv:int

    class Config:
        orm_mode = True
class GetIndividu(CreateIndividu):
    age_calcl :int
    sexe_instruc :int
    date_sortie: Optional[d.datetime]
    niv_diplome_indiv:int
    niveau_instruc: int

    class Config:
	    orm_mode=True
class DeleteIndividu(GetIndividu):
    
    class Config:
	    orm_mode=True
class UpdateIndividu(CreateIndividu):
    
    class Config:
	    orm_mode=True


class CreatAbsIndividu(BaseModel):
    
    id_indiv :int
    date_debut_abs :d.datetime
    date_fin_abs :Optional[d.datetime]
    motif_abs :Optional[int]
    class Config:
	    orm_mode=True
class GetAbsIndiv(CreatAbsIndividu):
    id_abs_seq:int
    
    class Config:
        orm_mode = True
class DeleteAbsIndividu(GetAbsIndiv):
    
    class Config:
	    orm_mode=True
class UpdateAbsIndividu(GetAbsIndiv):
    
    class Config:
	    orm_mode=True
