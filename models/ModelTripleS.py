from sqlalchemy import Column, ForeignKey, Integer, String, Date,CheckConstraint,text,Identity,MetaData
from sqlalchemy.orm import relationship

from ..view_factory import View 
from ..database import Base



class HouseholdTripleS:
    __view__ = View(
        'house_hold_triple_s', MetaData(),
        
        Column('f_id_mediamat', Integer,primary_key=True),

            Column('tranche_age', Integer),
            Column('f_taille', Integer),
            Column('f_pres_enf', Integer),
            Column('csp_cm', Integer),
            Column('activite_menagere', Integer),
            Column('langue_prin', Integer),
            Column('region', Integer),
            Column('milieu', Integer),
            Column('region_milieu', Integer),
            Column('taille_commune', Integer),
           
            
            Column('nbr_tv', Integer),
            Column('f_type_offre', Integer),
            Column('stand_vie', Integer),
            Column('f_csp', Integer),
            Column('offre_milieu', Integer),
            Column('niv_diplome_cm', Integer),
            Column('taille_milieu', Integer),
            Column('num_region_administrative', Integer),
            Column('tv_online', Integer),
    )
     

    __definition__ = text(
        '''  SELECT f.f_id_mediamat,
    cm.tranche_age,
    ef.f_taille,
        CASE
            WHEN ef.f_pres_enf = true THEN 1
            WHEN ef.f_pres_enf = false THEN 2
            ELSE NULL::integer
        END AS f_pres_enf,
    cm.csp_cm,
    cm.activite_menagere,
    so.langue_prin,
    re.region,
    co.milieu,
    cf.region_milieu,
    co.taille_commune,
    CASE
            WHEN e.nbr_tv = 1 THEN 1
            ELSE 2
        END AS nbr_tv,
    so.f_type_offre,
    so.stand_vie,
    f.f_csp,
    so.offre_milieu,
    cm.niv_diplome_cm,
    co.taille_milieu,
    "right"(concat('0', re.num_region_administrative), 2)::character varying AS num_region_administrative,
    f.tv_online
   FROM foyer f
     LEFT JOIN effectif_foyer ef USING (f_num_con)
     LEFT JOIN contact_foyer cf USING (f_num_con)
     LEFT JOIN sociodemo so USING (f_num_con)
     LEFT JOIN equipement eq USING (f_num_con)
     LEFT JOIN individu i USING (f_num_con)
     LEFT JOIN commune co ON co.id_commune = cf.id_commune
     LEFT JOIN region re ON co.id_commune = re.id_commune
     LEFT JOIN chef_menage cm ON i.id_indiv = cm.id_indiv
  WHERE i.age_calcl > 4 AND (i.etat < 5 OR i.etat = 6) AND (f.f_etat < 5 OR f.f_etat = 6) AND i.is_cm = true;'''
        
        )


class MeterTripleS:
    __view__ = View(
        'meter_triple_s', MetaData(),
        
        Column('id_view', Integer,primary_key=True),
        Column('num_poste', Integer),

        Column('f_id_mediamat', Integer),
            
    )
     

    __definition__ = text(
        '''SELECT concat(f.f_id_mediamat, s.num_poste) AS id_view, f.f_id_mediamat,s.num_poste 
    FROM poste s, foyer f 
    WHERE (f.f_etat <5 or f.f_etat=6) 
    AND s.f_num_con=f.f_num_con 
    ORDER BY f.f_id_mediamat, s.id_poste;'''
        
        )


class MemberTripleS:
    __view__ = View(
        'member_triple_s', MetaData(),
        
        Column('id_indiv', Integer,primary_key=True),

        Column('f_id_mediamat', Integer),
        Column('touche_indiv', Integer),
        Column('statut_indiv', Integer),
        Column('sexe', Integer),
        Column('tranche_2', Integer),
        Column('tranche_3', Integer),
        Column('tranche_5', Integer),
        Column('tranche_1', Integer),
        Column('age_sexe_1', Integer),
        Column('age_sexe_3', Integer),
        Column('activite', Integer),
        Column('sexe_activite', Integer),
        Column('niveau_instruc', Integer),
        Column('sexe_instruc', Integer),
        Column('plus_cinq', Integer),
        Column('age', Integer),
        Column('is_menagere', Integer),
        Column('is_maman', Integer),
        Column('niv_diplome_indiv', Integer),

            
    )
     

    __definition__ = text(
        '''SELECT RIGHT(CONCAT('0000',s.id_indiv),5)::character varying  as id_indiv,f.f_id_mediamat,
    RIGHT(CONCAT('0',s.touche_indiv),2)::character varying as touche_indiv,
    s.statut_indiv,
	s.sexe,
	fun_get_modalite_tranche(s.id_indiv, 2) as tranche_2,
	fun_get_modalite_tranche(s.id_indiv, 3) as tranche_3,
	fun_get_modalite_tranche(s.id_indiv, 5) as tranche_5,
	fun_get_modalite_tranche(s.id_indiv, 1) as tranche_1,
	
	fun_get_modalite(s.id_indiv, 1) as age_sexe_1,
	fun_get_modalite(s.id_indiv, 3) as age_sexe_3,
	s.activite,
	s.sexe_activite,
	s.niveau_instruc,
	s.sexe_instruc,
	(case when fun_to_age(s.date_naiss) > 5 then 1 end ) as plus_cinq,
	RIGHT(CONCAT('00',fun_to_age(s.date_naiss)::character varying),3)::character varying as age,
	s.is_menagere,
	s.is_maman,
	s.niv_diplome_indiv
	
	
	
	from individu s 
	join foyer f using(f_num_con)
    where s.age_calcl>4 and (s.etat<5  or  s.etat=6) and (f.f_etat<5  or f.f_etat=6)
    ORDER BY s.id_indiv;'''
        
        )



#################### Oumaima #########################################
class House_hold_triple_s_hist(Base):

       __tablename__ = 'house_hold_triple_s_hist'
        
       f_id_mediamat = Column( Integer,primary_key=True)
       tranche_age = Column( Integer)
       f_taille = Column( Integer)
       f_pres_enf = Column( Integer)
       csp_cm = Column( Integer)
       activite_menagere = Column( Integer)
       langue_prin = Column( Integer)
       region = Column( Integer)
       milieu = Column( Integer)
       region_milieu = Column( Integer)
       taille_commune = Column( Integer)
       nbr_tv = Column(Integer)
       f_type_offre = Column( Integer)
       stand_vie = Column( Integer)
       f_csp = Column( Integer)
       offre_milieu = Column( Integer)
       niv_diplome_cm = Column( Integer)
       taille_milieu = Column( Integer)
       num_region_administrative = Column( Integer)
       tv_online = Column( Integer)
       date = Column( Date,primary_key=True)
       statut = Column(Integer,primary_key=True)
       
       def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)



      # def __init__(self, f_id_mediamat, tranche_age, f_taille, f_pres_enf,csp_cm,activite_menagere,langue_prin,region,milieu,region_milieu,taille_commune,nbr_tv,f_type_offre,stand_vie,f_csp,offre_milieu,niv_diplome_cm,taille_milieu,num_region_administrative,tv_online):
      #  self.f_id_mediamat = f_id_mediamat
      #  self.tranche_age=tranche_age
      #  self.f_taille=f_taille
      #  self.f_pres_enf=f_pres_enf
      #  self.csp_cm=csp_cm
      #  self.activite_menagere=activite_menagere
      #  self.langue_prin=langue_prin
      #  self.region=region
      #  self.milieu=milieu
      #  self.region_milieu=region_milieu
      #  self.taille_commune=taille_commune
      #  self.nbr_tv=nbr_tv
      #  self.f_type_offre=f_type_offre
      #  stand_vie,
      #  self.f_csp=f_csp
      #  self.offre_milieu=offre_milieu
      #  self.niv_diplome_cm=niv_diplome_cm
      #  self.taille_milieu=taille_milieu
      #  self.num_region_administrative=num_region_administrative
      #  self.tv_online=tv_online





class Member_triple_s_hist(Base):

       __tablename__ = 'member_triple_s_hist'
        
       id_indiv = Column( Integer,primary_key=True)
       f_id_mediamat =  Column( Integer,primary_key=True)
       touche_indiv = Column( Integer)
       statut_indiv = Column( Integer)
       sexe = Column( Integer)
       tranche_2 = Column( Integer)
       tranche_3 = Column( Integer)
       tranche_5 = Column( Integer)
       tranche_1 = Column( Integer)
       age_sexe_1 = Column( Integer)
       age_sexe_3 = Column( Integer)
       activite = Column( Integer)
       sexe_activite = Column(Integer)
       niveau_instruc = Column( Integer)
       sexe_instruc = Column( Integer)
       plus_cinq = Column( Integer)
       age = Column( Integer)
       is_menagere = Column( Integer)
       is_maman = Column( Integer)
       niv_diplome_indiv = Column( Integer) 
       tv_online_indiv = Column( Integer)
       date = Column( Date,primary_key=True)
       statut = Column(Integer,primary_key=True)
       
       def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)




class Meter_triple_s_hist(Base):

       __tablename__ = 'meter_triple_s_hist'
        
       id_view = Column( Integer,primary_key=True)
       num_poste = Column( Integer)
       f_id_mediamat = Column( Integer)
       date = Column( Date,primary_key=True)
       statut = Column(Integer,primary_key=True)
       
       
       def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
