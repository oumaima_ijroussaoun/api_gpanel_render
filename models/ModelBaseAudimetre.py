from sqlalchemy import Column, ForeignKey, Integer, String,DateTime,Identity
from sqlalchemy.orm import relationship

from ..database import Base

#----------------------------------------------------------audimetre-------------------------------------------------------------------

class Audimetre(Base):

    __tablename__ = 'audimetre'

    __table_args__ = {
        'comment': 'la table audimetre qui contient les donn��es relatives aux audimetres'
    }

    id_meter = Column(String(8), primary_key=True,comment="l'identifiant de la table audimetre")
    statut_meter = Column(Integer, nullable=False,comment="les diff��rents ��tats des audimetres")
    type_meter = Column(Integer ,nullable=False,comment="les diff��rents types des audimetres")
    emplacement_meter = Column(Integer ,nullable=False,comment="les diff��rents types des audimetres")
    imei = Column(String,unique=True, primary_key=True,comment="l'identifiant IMEI audimetre")
    etat_meter = Column(Integer, nullable=False,comment="les diff��rents ��tats des audimetres")
    commentaire_meter = Column(Integer,comment="les diff��rents ��tats des audimetres")
    

   
    


#----------------------------------------------------------audimetre installe----------------------------------------------------------

class AudimetreInstalle(Base):

    __tablename__ = 'audimetre_installe'

    __table_args__ = {
        'comment': 'la table audimetre_installe qui contient les donn��es relatives aux audimetres_installes'
    }

    id_meter = Column(ForeignKey('audimetre.id_meter', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cl�� primaire provenant de la table audimetre")
    id_poste = Column(ForeignKey('poste.id_poste', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, unique=True,comment="cl�� ��trang��re provenant de la table poste")
    sortie_audio = Column(Integer ,comment="la source de sortie du son")#audio et micro
    capture_audio = Column(Integer,comment="la m��thode de capture audio")#sat et tv
    nom_installateur = Column(ForeignKey('installateurs.nom_installateur', ondelete='CASCADE', onupdate='CASCADE'),nullable=True ,comment="cl�� ��trang��re provenant de la table installateurs")
    

    
    aud = relationship('Audimetre',backref='audimetre_installe')
    poste = relationship('Poste',backref='audimetre_installe')


class AudimetreInstallateur(Base):

    __tablename__ = 'audimetre_installateur'

    __table_args__ = {
        'comment': 'la table audimetre_installateur qui contient les donn��es des audimetres chez les installateurs '
    }

    id_meter = Column(ForeignKey('audimetre.id_meter', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cl�� primaire provenant de la table audimetre")
    nom_installateur =Column(ForeignKey('installateurs.nom_installateur', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, comment="cl�� ��trang��re provenant de la table installateurs")
    
    
    aud = relationship('Audimetre',backref='audimetre_installateur')
    isntallateur = relationship('Installateurs',backref='audimetre_installateur')


class Installateurs(Base) :
    __tablename__ = 'installateurs'

    __table_args__ = {
        'comment': 'la table audimetre_installe qui contient les donn��es relatives aux audimetres_installes'
    }

    id_installateur = Column(Integer, primary_key=True,comment="cl�� primaire de la table installateurs")
    nom_installateur = Column(String(50),unique=True,nullable=False,comment="le nom de l'installateur")
    etat_installateur = Column(Integer,nullable=False,comment="l'etat de l'installateur")
    



class Mouvements(Base):
       __tablename__ = 'mouvements'

       __table_args__ = {
        'comment': 'la table mouvements qui contient les données relatives aux mouvements des audimetres'
    }
       date_mouvement = Column(DateTime, primary_key=True,comment="la date du mouvement")
       id_mouvement = Column(Integer, Identity(start=1) ,comment="identifiant du mouvement")
       old_emplacement = Column(Integer ,comment="l'ancient emplacement de l'audimetre ")
       old_statut =Column(Integer ,comment="l'ancient statut de l'audimetre ")
       new_emplacement = Column(Integer ,comment="le nouveau emplacement de l'audimetre ")
       new_statut =Column(Integer ,comment="le nouveau  statut de l'audimetre ")
       id_old_emplacement = Column(String(50),comment="identifiant de l'ancient emplacement")
       id_new_emplacement = Column(String(50),comment="identifiant du nouveau emplacement")
       id_meter=Column(String(8), primary_key=True,comment="l'identifiant de la table audimetre")
    