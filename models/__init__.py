from ..view_factory import CreateView

from .ModelCroisementModaliteAgeSexe import CroisementAgeSexeView
from .ModelCroisementModaliteAgeSexe import ExportPanelIndiv
from .ModelTrancheAge import TrancheAgeView
from .ModelModalite import Variables
from .ModelStructure import StructureView
from .ModelControles import ControlesView

from .ModelTripleS import HouseholdTripleS,MeterTripleS, MemberTripleS

def init_db_from_models(Base,engine,orm):
    # keeping track of your defined views 
    views_list = [TrancheAgeView,Variables,CroisementAgeSexeView,HouseholdTripleS,MeterTripleS,MemberTripleS,StructureView,ControlesView,ExportPanelIndiv]

    Base.metadata.create_all(bind=engine)

    for view in views_list:
        if not hasattr(view, '_sa_class_manager'):
            orm.mapper(view, view.__view__)  
        try:    
            engine.execute(CreateView(view))
        except(Exception) as error :
            print('vue deja existante')
            
