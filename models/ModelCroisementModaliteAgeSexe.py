from sqlalchemy import Column, ForeignKey, Integer,ARRAY,Boolean, String, Date,CheckConstraint,text,Identity,MetaData

from sqlalchemy.orm import relationship

from ..view_factory import View 
from ..database import Base

#----------------------------------------------------------croisement_age_sexe----------------------------------------------------------

class CroisementAgeSexe(Base):

    __tablename__ = 'croisement_age_sexe'

    __table_args__ = {

        'comment': "la table croisement_age_sexe qui contient les données relatives aux croisements de l'age et le sexe"
    }
    
    id_crois_seq = Column(Integer, Identity(start=1),primary_key=True, comment="l'identifiant de la table croisement_age_sexe")
    label_crois = Column(String(20), nullable=False, unique=True, comment="le label du croisement age x sexe")

#----------------------------------------------------------modalite_age_sexe----------------------------------------------------------

class ModaliteAgeSexe(Base):
    __tablename__ = 'modalite_age_sexe'

    __table_args__ = (

        CheckConstraint('age_max >= age_min'),
        {'comment': "la table modalite_age_sexe qui contient les données relatives aux modalites des croisements de l'age et le sexe"}
     
    )

    id_mod_crois_seq = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table croisement_age_sexe")
    age_max = Column(Integer, nullable=False,comment="l'age maximal pour un croisement age x sexe")
    age_min = Column(Integer, nullable=False,comment="l'age minimal pour un croisement age x sexe")
    sexe_crois = Column(Integer, nullable=False,comment="le sexe pour un croisement age x sexe")
    num_mod_crois = Column(Integer, nullable=False,comment="le numéro pour un croisement age x sexe")
    id_crois_seq = Column(ForeignKey('croisement_age_sexe.id_crois_seq', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére provenant de la table croisement_age_sexe")

    croisement_age_sexe = relationship('CroisementAgeSexe')



class CroisementAgeSexeView:
    __view__ = View(
        'croisement_age_sexe_view', MetaData(),
        Column('id_view', Integer , primary_key=True),
            Column('id_indiv', Integer),
            Column('f_num_con', Integer),
            Column('label_crois', Integer),
            Column('num_mod_crois', Integer)
    )
     

    __definition__ = text(''' SELECT concat(i.id_indiv, ci.id_crois_seq) AS id_view,
    i.id_indiv,
    i.f_num_con,
    ci.label_crois,
    mo.num_mod_crois
   FROM individu i
     CROSS JOIN croisement_age_sexe ci
     RIGHT JOIN modalite_age_sexe mo USING (id_crois_seq)
  WHERE mo.num_mod_crois = fun_get_modalite(i.id_indiv, ci.id_crois_seq);''')

class ExportPanelIndiv:
    __view__ = View(
        'export_panel_individu', MetaData(),
        Column('f_num_con', Integer),
        
        Column('id_indiv', Integer,primary_key=True),
	   Column('nom', String),
        Column('prenom', String),
         Column('date_naiss', Date),
        Column('touche_indiv', Integer),
        Column('niveau_instruc_glob', Integer),
        Column('categorie_indiv', Integer),
        Column('sexe', Integer),
        Column('etat', Integer),
        Column('activite', Integer),
        Column('categorie_inact', Integer),
        Column('enfant_touche_mere', Integer),
         Column('is_cm', Boolean),
        Column('is_menagere', Boolean),
        Column('is_maman', Boolean),
        Column('is_epouse', Boolean),
        
        Column('is_aide_domestique', Boolean),
        Column('mode_trait_indiv', Integer),
        Column('voyage_etrg', Integer),
         Column('date_sortie', Date),
        
        Column('telephones_indiv', ARRAY(String)),
        Column('age', Integer),
         Column('but_voy', Integer),
        Column('statut_indiv', Integer),
        
     
        Column('sexe_activite', Integer),
        
        Column('niveau_instruc', Integer),
        Column('sexe_instruc', Integer),
        Column('niv_diplome_indiv', Integer),
        Column('tranche_1', Integer),
        Column('tranche_2', Integer),
        Column('tranche_3', Integer),
        Column('tranche_4', Integer),
        Column('tranche_5', Integer),
        Column('tranche_6', Integer),
        Column('tranche_7', Integer),
        Column('tranche_8', Integer),
        Column('tranche_9', Integer),
        Column('tranche_10', Integer),
        Column('age_sexe_1', Integer),
        Column('age_sexe_2', Integer),
	   Column('age_sexe_3', Integer),
        Column('age_sexe_4', Integer), 
	   Column('age_sexe_5', Integer)
	   )
	   
    __definition__ = text(''' SELECT s.f_num_con,
    s.id_indiv,
    s.nom,
    s.prenom,
    s.date_naiss,
    s.touche_indiv,
    s.niveau_instruc_glob,
    s.categorie_indiv,
    s.sexe,
    s.etat,
    s.activite,
    s.categorie_inact,
    s.enfant_touche_mere,
    s.is_cm,
    s.is_menagere,
    s.is_maman,
    s.is_epouse,
    s.is_aide_domestique,
    s.mode_trait_indiv,
    s.voyage_etrg,
    s.date_sortie,
    s.telephones_indiv,
    s.age_calcl AS age,
    s.but_voy,
    s.statut_indiv,
    s.sexe_activite,
    s.niveau_instruc,
    s.sexe_instruc,
    s.niv_diplome_indiv,
    COALESCE(fun_get_age_tranche(s.id_indiv, 1),'hors tranche') AS tranche_1,
    COALESCE(fun_get_age_tranche(s.id_indiv, 2), 'hors tranche') AS tranche_2,
    COALESCE(fun_get_age_tranche(s.id_indiv, 3), 'hors tranche') AS tranche_3,
    COALESCE(fun_get_age_tranche(s.id_indiv, 4), 'hors tranche') AS tranche_4,
    COALESCE(fun_get_age_tranche(s.id_indiv, 5), 'hors tranche') AS tranche_5,
    COALESCE(fun_get_age_tranche(s.id_indiv, 6), 'hors tranche') AS tranche_6,
    COALESCE(fun_get_age_tranche(s.id_indiv, 7), 'hors tranche') AS tranche_7,
    COALESCE(fun_get_age_tranche(s.id_indiv, 8), 'hors tranche') AS tranche_8,
    COALESCE(fun_get_age_tranche(s.id_indiv, 9), 'hors tranche') AS tranche_9,
    COALESCE(fun_get_age_tranche(s.id_indiv, 10), 'hors tranche') AS tranche_10,
    COALESCE(fun_get_age_sexe_tranche(s.id_indiv, 1),  'hors tranche') AS age_sexe_1,
    COALESCE(fun_get_age_sexe_tranche(s.id_indiv, 2),  'hors tranche') AS age_sexe_2,
    COALESCE(fun_get_age_sexe_tranche(s.id_indiv, 3),  'hors tranche') AS age_sexe_3,
    COALESCE(fun_get_age_sexe_tranche(s.id_indiv, 4),  'hors tranche') AS age_sexe_4,
    COALESCE(fun_get_age_sexe_tranche(s.id_indiv, 5),  'hors tranche') AS age_sexe_5
   FROM individu s;
   ''')
