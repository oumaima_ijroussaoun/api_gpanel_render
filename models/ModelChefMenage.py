from sqlalchemy import  Column, ForeignKey, Integer,Computed
from sqlalchemy.orm import relationship

from ..database import Base

#----------------------------------------------------------chef de manege----------------------------------------------------------

class ChefMenage(Base):

    __tablename__ = 'chef_menage'

    __table_args__ = {
        'comment': 'la table chef_menage qui contient les données relatives des chefs de menage'
    }

    id_indiv = Column(ForeignKey('individu.id_indiv', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, unique=True, autoincrement=False,comment="clé primaire provenant de la table individu")
    profession_cm = Column(Integer, nullable=False,comment="la profession du chef de menage")
    niv_diplome_cm = Column(Integer, nullable=False,comment="le niveau de diplome du chef de menage")
    csp_cm = Column(Integer, nullable=False,comment="le CSP (Catégorie SocioProfessionnelle) du chef de ménage")
    tranche_age = Column(Integer, Computed('fun_cm_tranche_age(id_indiv)',persisted=True), nullable=True,comment="la tranche d'age à laquelle appartient le chef de menage (calculée)")
    activite_menagere = Column(Integer,Computed('fun_get_activite_menagere(id_indiv)',persisted=True), nullable=False ,comment="l'activité de la menegere")

    ind = relationship('Individu',backref='chef_menage')

