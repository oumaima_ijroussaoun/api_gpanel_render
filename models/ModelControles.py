from typing import List
from sqlalchemy import Column, ForeignKey, Integer, String, Date,CheckConstraint,text,Identity,MetaData,DateTime,ARRAY,JSON
from sqlalchemy.orm import relationship

from ..view_factory import View 
from ..database import Base


class ControlesView:
    __view__ = View(
        'controles_view', MetaData(),
        
        Column('f_num_con', Integer,primary_key=True),

            Column('csp_cm', Integer),
            Column('f_csp', Integer),
            Column('f_type_offre', Integer),
            Column('offre_milieu', Integer),
            Column('milieu', Integer),
            Column('f_nom', Integer),
            Column('nom_cm', Integer),
            Column('niv_diplome_cm', Integer),
            Column('niv_diplome_indiv', Integer),
            Column('activite_cm', Integer),
            Column('activite_menagere', Integer),
            Column('presence_femme_foyer', Integer),
            Column('check_presence_maman', Integer),
            Column('plusieur_menageres', Integer),
            Column('plusieur_cm', Integer),
        
            
    )
     

    __definition__ = text(
        '''  SELECT individu.f_num_con,
    chef_menage.csp_cm,
    foyer.f_csp,
    sociodemo.f_type_offre,
    sociodemo.offre_milieu,
    commune.milieu,
    foyer.f_nom,
    individu.nom AS nom_cm,
    chef_menage.niv_diplome_cm,
    individu.niv_diplome_indiv,
    chef_menage.profession_cm,
    individu.activite::integer AS activite_cm,
    chef_menage.activite_menagere,
    fun_presence_femme(chef_menage.id_indiv) AS presence_femme_foyer,
    fun_check_is_maman(chef_menage.id_indiv) AS check_presence_maman,
    fun_multiple_menagere(individu.f_num_con) AS plusieur_menageres,
    fun_multiple_cm(individu.f_num_con) AS plusieur_cm
   FROM chef_menage
     JOIN individu USING (id_indiv)
     JOIN foyer USING (f_num_con)
     JOIN sociodemo USING (f_num_con)
     JOIN contact_foyer USING (f_num_con)
     JOIN commune USING (id_commune)
  WHERE foyer.f_etat <> 5;'''
        
        )
