from typing import List
from sqlalchemy import Column, ForeignKey, Integer, String, Date,CheckConstraint,text,Identity,MetaData,DateTime,ARRAY,JSON
from sqlalchemy.orm import relationship

from ..view_factory import View 
from ..database import Base


class StructureView:
    __view__ = View(
        'structure_view', MetaData(),
        
        Column('f_num_con', Integer,primary_key=True),

            Column('tranche_age_cm', Integer),
            Column('f_taille', Integer),
            Column('f_pres_enf', Integer),
            Column('csp_cm', Integer),
            Column('langue_prin', Integer),
            Column('activite_menagere', Integer),
            Column('nbr_tv', Integer),
            Column('taille_milieu', Integer),
            Column('niv_diplome_cm', Integer),
            Column('region_administrative', Integer),
        
            
    )
     

    __definition__ = text(
        ''' SELECT f.f_num_con,
        CASE
            WHEN i.age_calcl >= 15 AND i.age_calcl <= 34 THEN 1
            WHEN i.age_calcl >= 35 AND i.age_calcl <= 44 THEN 2
            WHEN i.age_calcl >= 45 AND i.age_calcl <= 54 THEN 4
            WHEN i.age_calcl >= 55 AND i.age_calcl <= 64 THEN 5
            WHEN i.age_calcl >= 65 THEN 6
            ELSE NULL::integer
        END AS tranche_age_cm,
    ef.f_taille,
        CASE
            WHEN ef.f_pres_enf = true THEN 1
            WHEN ef.f_pres_enf = false THEN 2
            ELSE NULL::integer
        END AS f_pres_enf,
    cm.csp_cm,
    sd.langue_prin,
    cm.activite_menagere,
	case when e.nbr_tv=1 then 1
	else 2 end as nbr_tv ,
    co.taille_milieu,
    cm.niv_diplome_cm,
    r.region_administrative
   FROM foyer f
     JOIN individu i USING (f_num_con)
     JOIN effectif_foyer ef USING (f_num_con)
     JOIN chef_menage cm USING (id_indiv)
     JOIN sociodemo sd USING (f_num_con)
     JOIN equipement e USING (f_num_con)
     JOIN contact_foyer cf USING (f_num_con)
     JOIN region r USING (id_commune)
     JOIN commune co USING (id_commune)
  WHERE i.is_cm = true AND f.f_etat = 1;
'''
        
        ) 

class StructureTheorique(Base):

    __tablename__ = "structure_theorique"


    id_structure = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table structure_theorique" )
    struct_date = Column(DateTime, nullable=False,comment="la date des valeurs théorique")
    struct_theo = Column(String, nullable=False,comment="Le Json contenant les modalitées et les varibales théoriques")





class StructureCalculs(Base):

    __tablename__ = "structure_calculs"


    
    struct_calc_date = Column(DateTime, nullable=False,primary_key=True,comment="la date du calcul")
    struct_calc = Column(String, nullable=False,comment="Le Json contenant les calculs")





#   SELECT f.f_num_con,
#         CASE
#             WHEN i.age_calcl >= 15 AND i.age_calcl <= 34 THEN 1
#             WHEN i.age_calcl >= 35 AND i.age_calcl <= 44 THEN 2
#             WHEN i.age_calcl >= 45 AND i.age_calcl <= 54 THEN 4
#             WHEN i.age_calcl >= 55 AND i.age_calcl <= 64 THEN 5
#             WHEN i.age_calcl >= 65 THEN 6
#             ELSE NULL::integer
#         END AS tranche_age_cm,
#     ef.f_taille,
#         CASE
#             WHEN ef.f_pres_enf = true THEN 1
#             WHEN ef.f_pres_enf = false THEN 2
#             ELSE NULL::integer
#         END AS f_pres_enf,
#     cm.csp_cm,
#     sd.langue_prin,
#     cm.activite_menagere,
#     e.nbr_tv,
#     co.taille_milieu,
#     cm.niv_diplome_cm,
#     r.region_administrative
#    FROM foyer f
#      JOIN individu i USING (f_num_con)
#      JOIN effectif_foyer ef USING (f_num_con)
#      JOIN chef_menage cm USING (id_indiv)
#      JOIN sociodemo sd USING (f_num_con)
#      JOIN equipement e USING (f_num_con)
#      JOIN contact_foyer cf USING (f_num_con)
#      JOIN region r USING (id_commune)
#      JOIN commune co USING (id_commune)
#   WHERE i.is_cm = true AND f.f_etat = 1;