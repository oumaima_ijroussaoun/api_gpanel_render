
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime,CheckConstraint, Identity ,ARRAY
from sqlalchemy.orm import relationship

from ..database import Base

#----------------------------------------------------------poste----------------------------------------------------------

class Poste(Base):
    __tablename__ = 'poste'

    __table_args__ = (

        CheckConstraint('((tv_non_installe IS TRUE) AND (motif_tv_non_installe IS NOT NULL)) OR ((tv_non_installe IS FALSE) AND (motif_tv_non_installe IS NULL))'),
        {'comment' :'la table poste qui contient les données relatives aux postes'}
    )

    id_poste = Column(Integer, primary_key=True, autoincrement=False, comment="l'identifiant de la table poste")

    num_poste = Column(Integer, nullable=False,comment="le numéro du poste dans le foyer")
    etat_actuel = Column(Integer, nullable=False,comment="l'état actuel du poste")
    marque_tv = Column(String(20),comment="la marque TV du poste")
    model_tv = Column(String(20),comment="le modèle TV du poste")
    tv_non_installe = Column(Boolean, nullable=False,comment="l'installation ou non du poste dans le foyer")
    motif_tv_non_installe = Column(Integer,comment="le motif de la non installation du poste dans le foyer")
    dimension_tv = Column(String,comment="la dimension du poste")
    type_ecran = Column(Integer,comment="le type d'écran du poste")
    taille_ecran = Column(Integer,comment="la taille d'écran du poste")
    anciennete_tv = Column(Integer,comment="l'ancienneté du poste")
    statut_poste = Column(Integer, nullable=False,comment="le statut du poste dans le foyer")
    mode_trait_poste = Column(Integer, nullable=False,comment="le mode de traitement lors du processing des audiences du poste dans le foyer")
   
    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére provenant de la table foyer")
    emplacement = Column(Integer, nullable=False,comment="l'emplacement du poste dans le foyer")
    date_installation_poste = Column(DateTime,comment="la date d'installation du poste dans le foyer")
    photo_installation = Column(String(50),comment="la photo d'installation du poste dans le foyer")
     
   
    foyer = relationship('Foyer',backref='poste')

#----------------------------------------------------------equipement poste----------------------------------------------------------

class EquipementPoste(Base):

    __tablename__ = 'equipement_poste'

    __table_args__ = {

        'comment': 'la table equipement_poste qui contient données relatives aux équipements liés au poste '
    }
    id_poste = Column(ForeignKey('poste.id_poste', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False,comment="clé primaire provenant de la table poste")
    decodeur_sat_num = Column(Integer, nullable=False,comment="la possession ou non d'un décodeur satellite numérique lié au poste")
    decodeur_adsl = Column(Integer, nullable=False,comment="la possession ou non d'un décodeur ADSL lié au poste")
    home_cinema = Column(Integer, nullable=False,comment="la possession ou non d'un home cinema lié au poste")
    antenne_parab = Column(Integer, nullable=False,comment="la possession ou non d'une antenne parabolique liée au poste")
    type_antenne_parab = Column(Integer, nullable=False,comment="le type d'antenne parabolique liée au poste")
    reception_adsl = Column(Boolean, nullable=False,comment="la possession ou non d'une reception ADSL lié au poste")
    type_sonde_poste = Column(Integer, nullable=False,comment="le type de la sonde du poste")#tuner, watermark
    equipe_tnt = Column(Integer, nullable=False,comment="la possession ou non du TNT lié au poste")


    poste = relationship('Poste',backref='equipement_poste')



class Reception(Base):
    __tablename__ ='reception'
    __table_args__ = {

        'comment': 'la table mode_reception'
    }
    
    id_poste = Column(ForeignKey('poste.id_poste', ondelete='CASCADE', onupdate='CASCADE'),primary_key=True, nullable=False,comment="clé étrangére provenant de la table poste")
    type_reception = Column(ARRAY(Integer), nullable=True,comment="liste des types de réception pour le poste")
    reception_chaine = Column(ARRAY(Integer), nullable=False,comment="liste des chaines recues ")
    reception_sat = Column(ARRAY(Integer), nullable=False,comment="iste des satelites recus ")


    poste = relationship('Poste',backref='reception')