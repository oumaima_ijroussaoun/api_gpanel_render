from sqlalchemy import Column, ForeignKey, Integer, String, Date,CheckConstraint,text,Identity,MetaData
from sqlalchemy.orm import relationship

from ..view_factory import View 
from ..database import Base

#----------------------------------------------------------tranche_age----------------------------------------------------------

class TrancheAge(Base):

    __tablename__ = 'tranche_age'

    __table_args__ = {

        'comment': "la table tranche_age qui contient les données relatives aux croisements de l'age et le sexe"
    }
    
    id_tranche_seq = Column(Integer, Identity(start=1),primary_key=True, comment="l'identifiant de la table tranche_age")
    libelle_tranche = Column(String(20), nullable=False, unique=True, comment="le label du croisement age x sexe")

#----------------------------------------------------------modalite_age_sexe----------------------------------------------------------

class ModaliteTrancheAge(Base):
    __tablename__ = 'modalite_tranche_age'

    __table_args__ = (

        CheckConstraint('age_max >= age_min'),
        {'comment': "la table modalite_age_sexe qui contient les données relatives aux modalites des tranche d'age²"}
     
    )

    id_mod_tranche_seq = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table tranche_age")
    age_max = Column(Integer, nullable=False,comment="l'age maximal pour une tranche d'age")
    age_min = Column(Integer, nullable=False,comment="l'age minimal  pour une tranche d'age")
    num_mod_tranche = Column(Integer, nullable=False,comment="le numéro pour une tranche d'age")
    id_tranche_seq = Column(ForeignKey('tranche_age.id_tranche_seq', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére provenant de la table tranche_age")

    tranche_age = relationship('TrancheAge')



class TrancheAgeView:
    __view__ = View(
        'tranche_age_view', MetaData(),
        Column('id_view', Integer , primary_key=True),
            Column('id_indiv', Integer),
            Column('f_num_con', Integer),
            Column('libelle_tranche', Integer),
            Column('num_mod_tranche', Integer)
    )
     

    __definition__ = text('''SELECT concat(i.id_indiv, ci.id_tranche_seq) AS id_view,
    i.id_indiv,
    i.f_num_con,
    ci.libelle_tranche,
    mo.num_mod_tranche
   FROM individu i
     CROSS JOIN tranche_age ci
     RIGHT JOIN modalite_tranche_age mo USING (id_tranche_seq)
  WHERE mo.num_mod_tranche = fun_get_modalite_tranche(i.id_indiv, ci.id_tranche_seq);''')



