
# from test_db_mod.refrence_subapi.routers.RouterBaseFoyer import tel

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime,CheckConstraint,Computed,Identity,ARRAY
from sqlalchemy.orm import relation, relationship
from sqlalchemy.sql.sqltypes import Float

from ..database import Base

#----------------------------------------------------------foyer----------------------------------------------------------

class Foyer(Base):

    __tablename__ = "foyer"

    __table_args__ = (

        CheckConstraint('((f_date_fin IS NULL) AND (motif_sorti IS NULL) AND (detail_motif_sorti IS NULL)) OR ((f_date_fin IS NOT NULL) AND (motif_sorti IS NOT NULL))'),
        CheckConstraint('date_recrut <= f_date_deb'),
        {'comment': 'la table foyer qui contient les donnees relatives aux foyers'}
    )

    f_id_mediamat= Column(Integer, primary_key=True,unique=True, autoincrement=False, comment="l'identifiant de la table foyer (mediamat)")
    
    f_num_con= Column(Integer,unique=True, nullable=False,comment="le numero du contrat du foyer (utilisee techniquement)")

    f_nom = Column(String, nullable=False,comment="le nom du foyer")
    f_etat = Column(Integer, nullable=False,comment="l'etat du foyer")
    f_csp = Column(Integer, nullable=False,comment="le CSP (Categorie SocioProfessionnelle) du foyer")
    num_recrut = Column(String, nullable=False,comment="le numero de recrutement du foyer")
    date_recrut = Column(DateTime, nullable=False,comment="la date de recrutement du foyer")
    vague_recrut = Column(Integer, nullable=False,comment="la vague de recrutement du foyer")
    f_date_deb = Column(DateTime, nullable=False,comment="la date de debut d'un foyer dans le panel")
    f_date_fin = Column(DateTime, nullable=True,comment="la date de sortie d'un foyer du panel")
    motif_sorti = Column(Integer, nullable=True,comment="le motif de sortie d'un foyer du panel")
    tv_online = Column(Integer, nullable=False,comment="connection a l'internet ou non du poste")
    detail_motif_sorti = Column(String, nullable=True,comment="le detail sur le motif de sortie d'un foyer du panel")
    mode_trait_f = Column(Integer, nullable=False,comment="le mode de traitement lors du processing des audiences du foyer")
    nom_installateur = Column(ForeignKey('installateurs.nom_installateur', ondelete='CASCADE', onupdate='CASCADE'),comment="cle etrangere provenant de la table installateurs")
    telephones = Column(ARRAY(String), nullable=False,comment="liste des telephones du foyer ")
    motivation = Column(Integer, nullable=True,comment="la motivation du foyer")
    installateur= relationship('Installateurs',backref='foyer')
    num_recrut_uni=Column(String, nullable=False,comment="le numero de recrutement unifier du foyer")


#----------------------------------------------------------effectif foyer----------------------------------------------------------

class EffectifFoyer(Base):
    __tablename__ = 'effectif_foyer'

    __table_args__ = {
    
        'comment': "la table effectif foyer qui contient les donneees relatives au effectif des individus du foyer"
    }

    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cle etrangere provenant de la table foyer")

    f_nbr_emp_res = Column(Integer, Computed('fun_nb_cat_indiv(f_num_con, 2)', persisted=True),comment="le nombre des employes residents dans le foyer (calculee)")
    nbr_inv_regulier = Column(Integer, Computed('fun_nb_cat_indiv(f_num_con, 4)', persisted=True),comment="le nombre des invites reguliers dans le foyer (calculee)")
    nbr_employ_non_res = Column(Integer, Computed('fun_nb_cat_indiv(f_num_con, 3)', persisted=True),comment="le nombre des employes non residents dans le foyer (calculee)")
    nbr_fam_ami_res = Column(Integer, Computed('fun_nb_cat_indiv(f_num_con, 1)', persisted=True),comment="le nombre des membres de la famille et des amis residents dans le foyer (calculee)")
    total_touche_prog = Column(Integer, Computed('fun_nb_indiv_foy(f_num_con)',  persisted=True),comment="les touches programmees dans le foyer (calculee)")
    total_membre = Column(Integer, Computed('fun_total_membre(f_num_con)', persisted=True),comment="le nombre des membres du foyer (calculee)")
    f_taille = Column(Integer, Computed('fun_taille_foyer(f_num_con)', persisted=True),comment="la taille (petite, moyenne, grande) du foyer (calculee)")
    f_nbr_enf = Column(Integer, Computed('fun_nb_enf(f_num_con)', persisted=True),comment="le nombre des enfants dans le foyer (calculee)")
    f_pres_enf = Column(Boolean, Computed('fun_enf_pres(f_num_con)', persisted=True), comment="la presence des enfants dans le foyer (calculee)")

    foyer=relationship("Foyer",backref='effectif_foyer')
    
#----------------------------------------------------------equipement----------------------------------------------------------

class Equipement(Base):

    __tablename__ = 'equipement'

    __table_args__ = {

       'comment': 'la table equipement qui contient les donn������������������������������������������������������es relatives aux equipements du foyer'
    }

    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cl������������������������������������������������������ primaire provenant de la table foyer")
       
    nbr_clim = Column(Integer, nullable=False,comment="le nombre des climatiseurs dans le foyer")
    nbr_app_chauf = Column(Integer, nullable=False,comment="le nombre des appareils de chauffage dans le foyer")
    nbr_refrig = Column(Integer, nullable=False,comment="le nombre des r������������������������������������������������������frig������������������������������������������������������rateurs dans le foyer")
    nbr_congel = Column(Integer, nullable=False,comment="le nombre des cong������������������������������������������������������lateurs dans le foyer")
    nbr_four_micro = Column(Integer, nullable=False,comment="le nombre des fours micro ondes dans le foyer")
    nbr_cuisiniere = Column(Integer, nullable=False,comment="le nombre des cuisini������������������������������������������������������res dans le foyer")
    nbr_lave_linge = Column(Integer, nullable=False,comment="le nombre des lave linges dans le foyer")
    nbr_lave_vaiss = Column(Integer, nullable=False,comment="le nombre des lave vaisselles dans le foyer")
    nbr_aspi = Column(Integer, nullable=False,comment="le nombre des aspirateurs dans le foyer")
    poss_internet = Column(Boolean, nullable=False,comment="la possession ou non de l'internet dans le foyer")
    poss_tnt = Column(Boolean, nullable=False,comment="la possession ou non de la TNT dans le foyer")
    source_electricite = Column(Integer, nullable=False,comment="la source de l'������������������������������������������������������lectricit������������������������������������������������������ dans le foyer")
    nbr_tv = Column(Integer, nullable=False,comment="le nombre des TV dans le foyer")
    nbr_vehicule = Column(Integer, nullable=False,comment="le nombre des v������������������������������������������������������hicules dans le foyer")
    nbr_tel_mob = Column(Integer, nullable=False,comment="le nombre des t������������������������������������������������������l������������������������������������������������������phones mobiles dans le foyer")
    nbr_tel_fix = Column(Integer, nullable=False,comment="le nombre des t������������������������������������������������������l������������������������������������������������������phones fixes dans le foyer")
    nbr_ordi = Column(Integer, nullable=False,comment="le nombre des ordinateurs dans le foyer")
    nbr_radio = Column(Integer, nullable=False,comment="le nombre des radios dans le foyer")
    nbr_ordi_emp = Column(Integer, nullable=False,comment="le nombre des ordinateurs des employ������������������������������������������������������s dans le foyer")
    nbr_home_cinema = Column(Integer, nullable=False,comment="le nombre des home cin������������������������������������������������������mas dans le foyer")
    nbr_deco_sat = Column(Integer, nullable=False,comment="le nombre des d������������������������������������������������������codeurs satellite dans le foyer")
    nbr_console_jeux_se = Column(Integer, nullable=False,comment="le nombre des consoles jeux sans ������������������������������������������������������cran dans le foyer")
    nbr_console_jeux_ae = Column(Integer, nullable=False,comment="le nombre des consoles jeux avec ������������������������������������������������������cran dans le foyer")
    nbr_tablette = Column(Integer, nullable=True,comment="le nombre des tablettes dans le foyer")
    foyer=relationship("Foyer",backref='equipement')

#----------------------------------------------------------sociodemo----------------------------------------------------------

class Sociodemo(Base):

    __tablename__ = 'sociodemo'

    __table_args__ = {
        'comment': 'la table sociodemo qui contient les donn������������������������������������������������������es sociod������������������������������������������������������mographiques du foyer'
    }
        

    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, unique=True,comment="cl������������������������������������������������������ primaire provenant de la table foyer")

    f_type_offre = Column(Integer, nullable=False,comment="le type d'offre du foyer")
    offre_milieu = Column(Integer,Computed('fun_offre_milieu(f_num_con)', persisted=True),comment="croisement type offre et milieu d'habitat")
    f_tranche_revenu = Column(Integer, nullable=False,comment="la tranche de revenu du foyer")
    type_hab = Column(Integer, nullable=False,comment="le type d'habitat du foyer")
    standing_hab = Column(Integer, nullable=False,comment="le standing habitat du foyer")
    pos_res_sec = Column(Integer, nullable=False,comment="la possession ou non d'une r������������������������������������������������������sidence secondaire")
    langue_prin = Column(Integer, nullable=False,comment="la langue principale parl������������������������������������������������������e dans le foyer")
    stand_vie = Column(Integer, nullable=False,comment="le standard de vie dans le foyer")
    foyer=relationship("Foyer",backref='sociodemo')


#----------------------------------------------------------Tel----------------------------------------------------------

# class Tel(Base):

#     __tablename__ = 'tel'

#     __table_args__ = {

#         'comment': 'la table tel qui contient les num������������������������������������������������������ros des t������������������������������������������������������l������������������������������������������������������phones du foyer'
#     }
        

#     id_tel = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table tel")
#     f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="cl������������������������������������������������������ ������������������������������������������������������trang������������������������������������������������������re provenant de la table foyer")
#     num_tel = Column(String(50), nullable=False,comment="num������������������������������������������������������ro de t������������������������������������������������������l������������������������������������������������������phone du foyer")

#----------------------------------------------------------contact foyer----------------------------------------------------------

class ContactFoyer(Base):

    __tablename__ = 'contact_foyer'

    __table_args__ = {
        
        'comment': 'la table contact_foyer qui contient les donn������������������������������������������������������es g������������������������������������������������������ographiques du foyer'
    }
        
    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cl������������������������������������������������������ primaire provenant de la table foyer")
    
    date_install_telfix = Column(DateTime,comment="la date d'installation du t������������������������������������������������������l������������������������������������������������������phone fixe du foyer")
    f_adr1 = Column(String(250), nullable=False,comment="l'adresse (la rue) du foyer")
    longitude = Column(Float,comment="la longitude des coordonnees GPS du foyer")
    latitude = Column(Float,comment="la latitude des coordonnees GPS du foyer")
    id_commune = Column(ForeignKey('commune.id_commune', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="cl������������������������������������������������������ ������������������������������������������������������trang������������������������������������������������������re provenant de la table commune")
    f_adr2 = Column(String(250),comment="le compl������������������������������������������������������ment d'adresse du foyer")
    region_milieu = Column(Integer, Computed('fun_region_milieu(id_commune)', persisted=True),comment="croisement entre la region et le milieu du foyer (calcul������������������������������������������������������e)")

    commune = relationship('Commune',backref='contact_foyer')
    foyer=relationship("Foyer",backref='contact_foyer')

