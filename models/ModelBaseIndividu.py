from sqlalchemy import Boolean, Column, ForeignKey, Integer,ARRAY, String, Date,CheckConstraint,Computed,UniqueConstraint,Identity,Index,DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql.elements import True_
from ..view_factory import View 
from ..database import Base

#----------------------------------------------------------individu----------------------------------------------------------
class Individu(Base):
    __tablename__ = "individu"

    __table_args__ = (

    
        CheckConstraint('nom_foy_cm_check',  '\nCASE\n  WHEN (is_cm = true) THEN ((nom)::text = (fun_get_nom(f_num_con))::text)\n    ELSE NULL::boolean\nEND'),
        CheckConstraint('sexe_check',' \nCASE\n  WHEN (is_maman = true) THEN (sexe = 2)\n    WHEN (is_menagere = true) THEN (sexe = 2)\n    WHEN (is_epouse = true) THEN (sexe = 2)\n    ELSE NULL::boolean\nEND'),
        CheckConstraint('touche_check','(touche_indiv <= 12) AND (touche_indiv <> 0)'),
        UniqueConstraint('f_num_con', 'touche_indiv'),
        Index('cm_check', 'f_num_con', 'is_cm', unique=True, postgresql_where=("is_cm=True")),
        {'comment': "la table individu qui contient les données relatives aux individus d'un foyer"}
    )


    id_indiv = Column(Integer, primary_key=True, autoincrement=False,comment="l'identifiant de la table individu")

    nom = Column(String(30), nullable=False,comment="le nom de l'individu")
    prenom = Column(String(50), nullable=False,comment="le prénom de l'individu")
    date_naiss = Column(Date, nullable=False,comment="la date de naissance de l'individu")
    touche_indiv = Column(Integer, nullable=False,comment="la touche de l'individu")
    telephones_indiv = Column(ARRAY(String),comment="liste des telephones de l'individu ")
    niveau_instruc_glob = Column(Integer, nullable=False,comment="le niveau d'instruction de l'individu")
    niveau_instruc = Column(Integer, Computed('fun_instruc(niveau_instruc_glob)', persisted=True),comment="l'individu est alphabéte ou non")
    categorie_indiv = Column(Integer, nullable=False,comment="la catégorie de l'individu")
    statut_indiv = Column(Integer, nullable=False,comment="le statut de l'individu")
    sexe = Column(Integer, nullable=False,comment="le sexe de l'individu")
    etat = Column(Integer, nullable=False, comment="l'état de l'individu")
    activite = Column(Boolean, nullable=False, comment="l'activit�� ou non de l'individu")
    categorie_inact = Column(Integer, comment="le type d'inactivit�� de l'individu")
    enfant_touche_mere = Column(Integer, nullable=True, comment="la touche de la m��re pour un individu enfant moins de 5ans")
    voyage_etrg = Column(Integer, nullable=False,comment="voyage ou non �� l'��tranger de l'individu")
    but_voy = Column(Integer,comment="le but du voyage de l'individu")
    is_cm = Column(Boolean, nullable=False, comment="l'individu est chef de menage ou non")
    is_menagere = Column(Boolean, nullable=False, comment="l'individu est menagere ou non'")
    is_maman = Column(Boolean, nullable=False,comment="l'individu est m��re ou non")
    is_epouse = Column(Boolean,comment="l'individu est ��pouse ou non")
    is_aide_domestique = Column(Boolean, nullable=False,comment="l'individu est aide domestique ou non")
    date_sortie = Column(Date, nullable=True,comment="la date de sortie de l'individu du panel")
    mode_trait_indiv = Column(Integer, nullable=False,comment="le mode de traitement lors du processing des audiences de l'individu")
    f_num_con = Column(ForeignKey('foyer.f_num_con', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="cl�� ��trang��re provenant de la table foyer")
    age_calcl = Column(Integer, Computed('fun_to_age(date_naiss)', persisted=True), nullable=False,comment="l'age de l'individu (calcul��e)")
    sexe_instruc = Column(Integer, Computed('fun_sexe_instruc(niveau_instruc, sexe)', persisted=True),comment="croisement entre niveau_instruc et le sexe de l'individu (calcul��e)")
    sexe_activite = Column(Integer, Computed('fun_sexe_activite(sexe, activite)', persisted=True),comment="croisement entre activite et le sexe de l'individu (calcul��e)")
    niv_diplome_indiv = Column(Integer,  Computed('fun_niv_diplome(niveau_instruc_glob)', persisted=True),comment="le niveau de diplome de l'individu")

    foyer = relationship("Foyer",backref="individu")
   

#----------------------------------------------------------abs individu----------------------------------------------------------

class AbsIndiv(Base):

    __tablename__ = 'abs_indiv'

    __table_args__ = (

        CheckConstraint('date_debut_abs < date_fin_abs'),
        {'comment': "la table abs_individu qui contient l'historique des absences des individus"}
    )
    
    id_abs_seq = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table abs_indiv")

    id_indiv = Column(ForeignKey('individu.id_indiv', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére de la table absence individu provenant de la table individu")
    date_debut_abs = Column(DateTime,comment="la date de début de l'absence de l'individu")
    date_fin_abs = Column(DateTime, nullable=True,comment="la date de fin de l'absence de l'individu")
    motif_abs = Column(Integer, nullable=False,comment="le motif de l'absence de l'individu")
    
    individu = relationship("Individu",backref='abs_indiv')





    