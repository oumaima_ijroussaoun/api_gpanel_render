from sqlalchemy import  Column, ForeignKey, Integer, String,Computed,Identity,ARRAY

from sqlalchemy.orm import relationship
from ..database import Base

#---------------------------------------------------------commune----------------------------------------------------------

class Commune(Base):

    __tablename__ = 'commune'

    __table_args__ = {
        'comment': 'la table commune qui contient les données relatives aux communes'
    }

    id_commune = Column(Integer, primary_key=True, autoincrement=False,comment="l'identifiant de la table commune")
    nom_commune = Column(String(50), nullable=False,comment="le nom de la commune")
    nom_zone = Column(String(50), nullable=False,comment="le nom de la zone de la commune")
    num_zone = Column(Integer, nullable=False,comment="le numéro de la zone de la commune")
    taille_commune = Column(Integer, nullable=False,comment="la taille de la commune")
    milieu = Column(Integer, nullable=False,comment="le milieu de la commune (urbain, rural)")
    taille_milieu = Column(Integer,Computed('fun_taille_milieu(id_commune)', persisted=True),comment="croisement taille de la commune avec millieu")



#----------------------------------------------------------region----------------------------------------------------------

class Region(Base):

    __tablename__ = 'region'

    __table_args__ = {
        'comment': 'la table region qui contient les données relatives aux regions'
    }

    id_region = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table region")
    num_region_administrative  = Column(Integer, nullable=False,comment="le numero de la region administrative ")
    region = Column(Integer, nullable=False,comment="le nom de la region")
    region_administrative = Column(String(50), nullable=False,comment="le nom de la region administrative")
    id_commune = Column(ForeignKey('commune.id_commune', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére provenant de la table commune")

    commune = relationship('Commune',backref='region')

#----------------------------------------------------------ville----------------------------------------------------------

class Ville(Base):

    __tablename__ = 'ville'

    __table_args__ = {
        'comment': 'la table ville qui contient les données relatives aux villes'
    }

    id_ville_seq = Column(Integer, Identity(start=1), primary_key=True,comment="l'identifiant de la table ville")
    ville = Column(String(50), nullable=False,comment="le nom de la ville")
    id_commune = Column(ForeignKey('commune.id_commune', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,comment="clé étrangére provenant de la table commune")

    commune = relationship('Commune',backref='ville')




class InstallateursCommune(Base):

    __tablename__ = 'installateurs_commune'

    __table_args__ = {
        'comment': 'la table commune qui contient les données relatives aux communes'
    }
    
    installateur= Column(String,primary_key=True, nullable=False,comment="le nom de l'installateur")
    commune = Column(ARRAY(String), nullable=False,comment="le nom de la commune")
    