from sqlalchemy import Column,Integer,String,MetaData,text

from ..view_factory import View 
from ..database import Base

#----------------------------------------------------------modalite----------------------------------------------------------

class Modalite(Base):

    __tablename__ = 'modalite'

    id_modalite = Column(Integer, primary_key=True,autoincrement = False , comment="l'identifiant de la table modalite")

    id_var = Column(Integer,comment="l'identifiant de la variable")
    nom_table = Column(String(50), nullable=False,comment="le nom de la table")
    nom_var = Column(String(50), nullable=False,comment="le nom de la variable")
    num_modalite = Column(Integer, nullable=False,comment="le numero de la modalite")
    label_modalite = Column(String(50), nullable=False,comment="le label de la variable")




class Variables:
    __view__ = View(
        'variables', MetaData(),
            Column('id_var', Integer , primary_key=True),
            Column('nom_table', String(250) ),
            Column('nom_var', String(250)),
            Column('type_var', String(250)),
            Column('description', String(250))
    )
     

    __definition__ = text('''SELECT concat(pgd.objoid,pgd.objsubid)::integer as id_var ,col.table_name as nom_table,col.column_name as nom_var, col.data_type as type_var, pgd.description as description

from pg_catalog.pg_statio_all_tables as st

inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)

inner join information_schema.columns col

on (pgd.objsubid=col.ordinal_position and  col.table_schema=st.schemaname

and col.table_name=st.relname) 

where table_schema = 'public';''')