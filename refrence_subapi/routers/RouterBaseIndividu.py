from fastapi import APIRouter, FastAPI, HTTPException
from sqlalchemy.orm import Session
from typing import List


from ...models import ModelBaseIndividu as bimodels
from ...schemas import SchemaBaseIndividu  as bischemas

from ..crud import CrudBaseIndividu as bicrud
from ..crud import CrudBaseFoyer as bfcrud




route = APIRouter(prefix="/BaseIndividu",tags=["BaseIndividu"])








####################################################individu########################################################################
@route.get("/Individu/get/{id_indiv}", response_model=bischemas.GetIndividu, description= "lecture des données d'un individu")
def read_individu(id_indiv: int):
    db_individu = bicrud.get_indiv(id_indiv)
    if db_individu is None:
        raise HTTPException(status_code=404, detail="individu not found")
    return db_individu

@route.post("/Individu/random/create", response_model=bischemas.CreateIndividu, description= "generation et insertion des données aleatoires d'un individu")
def create_random_individu(id_foyer: int, nb:int):
    db_foyer = bfcrud.get_foy(id_foyer)
    if not db_foyer:
        raise HTTPException(status_code=400, detail="foyer not found")
    db_individu = bicrud.get_indiv(nb+id_foyer+1000)
    if db_individu:
        raise HTTPException(status_code=400, detail="cet individu est  deja existant")     
    return bicrud.random_indiv(id_foy=id_foyer, nb=nb)
      
@route.post("/Individu/create", response_model=bischemas.CreateIndividu, description= "insertion des données d'un individu")
def create_individu(my_indiv: bischemas.CreateIndividu):
    # db_foyer = bfcrud.get_foy(my_indiv.f_num_con)
    # if not db_foyer :
    #     raise HTTPException(status_code=400, detail="ce foyer n existe pas ") 
    # db_individu = bicrud.get_indiv(my_indiv.id_indiv)
    # if db_individu:
    #     raise HTTPException(status_code=400, detail="cet individu est  deja existant")      
    return bicrud.create_indiv(my_indiv)
    
@route.delete("/Individu/delete/{id_indiv}", description= "suppression des données d'un individu")
def delete_individu(id_indiv: int):
    db_individu = bicrud.get_indiv(id_indiv)
    if not db_individu:
        raise HTTPException(status_code=400, detail="cet individu n'existe pas") 

    return bicrud.delete_indiv(id_indiv)

@route.get("/Individu/get/all/{id_foyer}", response_model=List[bischemas.GetIndividu], description= "lecture des données de tous les individus d'un foyer")
def read_individus_by_foyer(id_foyer: int):
    db_individus = bicrud.get_all_indiv_foy(id_foyer)
    db_foyer = bfcrud.get_foy(id_foyer)
    if db_individus is None:
        raise HTTPException(status_code=404, detail="aucun indiv")
    elif not db_foyer :
        raise HTTPException(status_code=400, detail="ce foyer n existe pas ")  

    return db_individus

@route.put("/Individu/update/{id_ind}",response_model=bischemas.GetIndividu, description= "modification des données d'un individu")
def update_individu(id_ind:int, updates:dict):
    db_individu = bicrud.get_indiv(id_ind)
    if not db_individu:
        raise HTTPException(status_code=400, detail="individu not found")
    return bicrud.update_indiv(id_ind=id_ind, my_updates=updates)
    

@route.put("/Individus/update/{id_foy}",response_model=List[bischemas.GetIndividu], description= "modification des données des individus d'un foyer")
def update_individu_foy(id_foy:int, updates:dict):
    db_individu = bicrud.get_all_indiv_foy(id_foy)
    if not db_individu:
        raise HTTPException(status_code=400, detail="individus not found")
    return bicrud.update_indiv_id_foy(id_foy, my_updates=updates)
    

#####################################################################abs_indiv#######################################################################

@route.get("/AbsIndividu/get/all/{id_indiv}", response_model=List[bischemas.GetAbsIndiv], description= "lecture de toutes les absences d'un individu")
def read_abs_individu(id_indiv: int):
    db_individu = bicrud.get_indiv(id_indiv)
    if db_individu is None:
        raise HTTPException(status_code=404, detail="individu not found")
    return bicrud.get_abs_indiv(id_indiv)

@route.get("/AbsIndividu/get{id_abs}", response_model=bischemas.GetAbsIndiv, description= "lecture de toutes les absences d'un individu")
def read_abs(id_abs: int):
    db_abs = bicrud.get_abs(id_abs)
    if db_abs is None:
        raise HTTPException(status_code=404, detail="absence not found")
    return bicrud.get_abs(id_abs)

@route.post("/AbsIndividu/create", response_model=bischemas.CreatAbsIndividu, description= "insertion d'une absence")
def create_abs_individu(my_abs_indiv: bischemas.CreatAbsIndividu):
    db_indiv = bicrud.get_indiv(my_abs_indiv.id_indiv)
    if not db_indiv :
        raise HTTPException(status_code=400, detail="individu not found ")      
    return bicrud.create_abs_indiv(my_abs_indiv)

@route.put("/AbsIndividu/update/{id_abs}",response_model=bischemas.UpdateAbsIndividu, description= "modification d'une absence")
def update_abs_individu(id_abs:int, updates:dict):
    
    return bicrud.update_abs_indiv(id_abs=id_abs, my_updates=updates)
