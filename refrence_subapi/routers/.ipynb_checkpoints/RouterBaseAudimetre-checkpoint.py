from fastapi import Depends,APIRouter, FastAPI, HTTPException
from sqlalchemy.orm import Session
from typing import List

from ..crud import CrudBaseAudimetre as bacrud
from ..models import ModelBaseAudimetre as bamodels
from ..schemas import SchemaBaseAudimetre as baschemas
from ..database import SessionLocal, engine

from . import get_db




route = APIRouter(prefix="/BaseAudimetre",tags=["BaseAudimetre"])

# description= "lecture des données d'un audimetre"
@route.get("/Audimetre/get/{id_meter}", response_model=baschemas.GetAudimetre)
def read_audimetre(id_meter: str, db: Session = Depends(get_db)):
    db_audimetre = bacrud.get_audimetre(db=db, id_meter=id_meter)
    if db_audimetre is None:
        raise HTTPException(status_code=404, detail="Audimetre not found")
    return db_audimetre

@route.post("/Audimetre/create/", response_model=baschemas.GetAudimetre)
def create_audimetre(audimetre: baschemas.CreateAudimetre, db: Session = Depends(get_db)):
    db_audimetre = bacrud.get_audimetre(db, id_meter=audimetre.id_meter)
    if db_audimetre:
        raise HTTPException(status_code=400, detail="meter already registered")
    return bacrud.create_audimetre(db, my_meter=audimetre)


@route.put("/Audimetre/update/{id_meter}", response_model=baschemas.GetAudimetre)
def update_audimetre(id_meter: str, updates: dict, db: Session = Depends(get_db)):
    db_meter =  bacrud.get_audimetre(db, id_meter=id_meter)
    if not db_meter:
        raise HTTPException(status_code=400, detail="meter not found")
    return bacrud.update_audimetre(db= db, id_meter=id_meter,my_updates=updates)

@route.delete("/Audimetre/delete/{id_meter}")
def delete_audimetre(id_meter: str, db: Session = Depends(get_db)):
    db_audimetre = bacrud.get_audimetre(db, id_meter=id_meter)
    if db_audimetre is None :
        raise HTTPException(status_code=400, detail="meter already deleted")
    return bacrud.delete_audimetre(db=db, id_meter=id_meter)

####################################audimetre_installe

@route.get("/AudimetreInstalle/get/{id_meter}", response_model=baschemas.GetAudimetreInstalle, description= "lecture des données d'un audimetre installé")
def read_audimetre_installe(id_meter: str, db: Session = Depends(get_db)):
    db_audimetre_installe = bacrud.get_audimetre_installe(db=db, id_meter=id_meter)
    if db_audimetre_installe is None:
        raise HTTPException(status_code=404, detail="AudimetreInstalle not found")
    return db_audimetre_installe

@route.post("/AudimetreInstalle/create/", response_model=baschemas.GetAudimetreInstalle, description= "lecture des données d'un audimetre installé")
def create_audimetre_installe(audimetreinstalle: baschemas.CreateAudimetreInstalle, db: Session = Depends(get_db)):
    db_audimetre_installe = bacrud.get_audimetre_installe(db=db, id_meter=audimetreinstalle.id_meter)
    if db_audimetre_installe :
        raise HTTPException(status_code=400, detail="already registered")
    return bacrud.create_audimetre_installe(db, my_meter=audimetreinstalle)   


@route.put("/AudimetreInstalle/update/{id_meter}", response_model=baschemas.GetAudimetreInstalle, description= "lecture des données d'un audimetre installé")
def update_audimetre_installe(id_meter: str, updates: dict, db: Session = Depends(get_db)):
    db_audimetre_installe = bacrud.get_audimetre_installe(db=db, id_meter=id_meter)
    if db_audimetre_installe is None:
        raise HTTPException(status_code=404, detail="AudimetreInstalle not found")
    return bacrud.update_audimetre_installe(db=db, id_meter=id_meter,my_updates= updates)

@route.delete("/AudimetreInstalle/delete/{id_meter}", description= "lecture des données d'un audimetre installé")
def delete_audimetre_installe(id_meter: str, db: Session = Depends(get_db)):
    db_audimetre_installe = bacrud.get_audimetre_installe(db=db, id_meter=id_meter)
    if db_audimetre_installe is None:
        raise HTTPException(status_code=404, detail="AudimetreInstalle not found")
    return bacrud.delete_audimetre_installe(db, id_meter)