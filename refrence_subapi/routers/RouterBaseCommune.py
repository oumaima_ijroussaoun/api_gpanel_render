from fastapi import APIRouter, FastAPI, HTTPException



from ..crud import CrudBaseCommune as cocrud
from ...models import ModelBaseCommune as comodels
from ...schemas import SchemaBaseCommune as coschemas




route = APIRouter(prefix="/BaseCommune",tags=["BaseCommune"])


#########################################################commune###############################################################################

@route.get("/Commune/get/{id_commune}", response_model=coschemas.GetCommune, description= "lecture des données d'une commune")
def read_commune(id_commune: int):
    db_commune = cocrud.get_commune(id_commune)
    if db_commune is None:
        raise HTTPException(status_code=404, detail="commune not found")
    return db_commune



@route.post("/Commune/create/", response_model=coschemas.GetCommune, description= "ajout d'une commune")
def create_commune(commune: coschemas.CreateCommune):
    db_commune = cocrud.get_commune(commune.id_commune)
    if db_commune :
        raise HTTPException(status_code=400, detail="already registered")
    return cocrud.create_commune(my_commune=commune)   


@route.put("/Commune/update/{id_commune}", response_model=coschemas.GetCommune, description= "modification d'une commune ")
def update_commune(id_commune: int, updates: dict):
    db_commune = cocrud.get_commune(id_commune)
    if db_commune is None:
        raise HTTPException(status_code=404, detail="commune not found")
    return cocrud.update_commune(id_commune=id_commune,my_updates= updates)

@route.delete("/Commune/delete/{id_commune}", description= "supprimer une commune")
def delete_commune(id_commune: int):
    db_commune = cocrud.get_commune(id_commune)
    if db_commune is None:
        raise HTTPException(status_code=404, detail="commune not found")
    return cocrud.delete_commune(id_commune)
#########################################################ville###############################################################################
@route.get("/Ville/get/{id_ville}", response_model=coschemas.GetVille, description= "lecture des données d'une ville")
def read_ville(id_ville: int):
    db_ville = cocrud.get_ville(id_ville)
    if db_ville is None:
        raise HTTPException(status_code=404, detail="ville not found")
    return db_ville

@route.post("/Ville/create/", response_model=coschemas.GetVille, description= "ajout d'une ville")
def create_ville(ville: coschemas.CreateVille):
    db_ville = cocrud.get_ville(ville.id_ville)
    if db_ville :
        raise HTTPException(status_code=400, detail="already registered")
    return cocrud.create_ville(my_ville=ville)   


@route.put("/Ville/update/{id_ville}", response_model=coschemas.GetVille, description= "modification d'une villee ")
def update_ville(id_ville: int, updates: dict):
    db_ville = cocrud.get_ville(id_ville)
    if db_ville is None:
        raise HTTPException(status_code=404, detail="commune not found")
    return cocrud.update_ville(id_ville=id_ville,my_updates= updates)

@route.delete("/Ville/delete/{id_ville}", description= "supprimer une ville")
def delete_ville(id_ville: int):
    db_ville = cocrud.get_ville(id_ville)
    if db_ville is None:
        raise HTTPException(status_code=404, detail="ville not found")
    return cocrud.delete_ville(id_ville)
#########################################################region###############################################################################
@route.get("/Region/get/{id_region}", response_model=coschemas.GetRegion, description= "lecture des données d'une ville")
def read_region(id_region: int):
    db_region = cocrud.get_region(id_region)
    if db_region is None:
        raise HTTPException(status_code=404, detail="ville not found")
    return db_region

@route.post("/Region/create/", response_model=coschemas.GetRegion, description= "ajout d'une ville")
def create_region(region: coschemas.CreateRegion):
    db_region = cocrud.get_ville(region.id_region)
    if db_region :
        raise HTTPException(status_code=400, detail="already registered")
    return cocrud.create_ville(region)   


@route.put("/Region/update/{id_region}", response_model=coschemas.GetRegion, description= "modification d'une villee ")
def update_ville(id_region: int, updates: dict):
    db_region = cocrud.get_region(id_region)
    if db_region is None:
        raise HTTPException(status_code=404, detail="commune not found")
    return cocrud.update_region(id_region,my_updates= updates)

@route.delete("/Region/delete/{id_region}", description= "supprimer une region")
def delete_region(id_region: int):
    db_region = cocrud.get_region(id_region)
    if db_region is None:
        raise HTTPException(status_code=404, detail="region not found")
    return cocrud.delete_region(id_region)