from fastapi import APIRouter, FastAPI, HTTPException

from typing import List

from ...models import ModelChefMenage as cmmodels
from ...schemas import SchemaChefMenage  as cmschemas

from ..crud import CrudChefMenage as cmcrud
from ..crud import CrudBaseIndividu as bicrud






route = APIRouter(prefix="/ChefMenage",tags=["ChefMenage"])

@route.get("/get/{id_indiv}", response_model=cmschemas.GetChefMenage)
def read_chef_menage(id_indiv: int):
    db_chef_menage = cmcrud.get_chef_menage(id_indiv)
    if db_chef_menage is None:
        raise HTTPException(status_code=404, detail="chef de menage not found")
    return db_chef_menage

@route.post("/create/", response_model=cmschemas.GetChefMenage)
def create_chef_menage(my_chef_menage: cmschemas.CreateChefMenage):
    db_individu = bicrud.get_indiv(my_chef_menage.id_indiv)
    if not db_individu:
        raise HTTPException(status_code=400, detail="cet individu n'existe pas")      
    return cmcrud.create_chef_menage(my_chef_menage)

@route.put("/update/{id_ind}",response_model=cmschemas.GetChefMenage)
def update_chef_menage(id_ind:int, updates:dict):
    db_chef_menage = cmcrud.get_chef_menage(id_ind)
    if not db_chef_menage:
        raise HTTPException(status_code=400, detail="chef menage not found")
    return cmcrud.update_chef_menage(id_ind=id_ind, my_updates=updates)