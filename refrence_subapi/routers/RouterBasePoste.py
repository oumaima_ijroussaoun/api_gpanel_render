from fastapi import APIRouter, FastAPI, HTTPException

from typing import List

from ...models import ModelBasePoste as bpmodels
from ...schemas import SchemaBasePoste as bpschemas

from ..crud import CrudBasePoste as bpcrud







route = APIRouter(prefix="/BasePoste",tags=["BasePoste"])



################################################################poste#########################################################################


@route.get("/Poste/get/{id_poste}", response_model=bpschemas.GetPoste, description= "affichage poste")
def read_poste(id_poste: int):
    db_poste = bpcrud.get_poste(id_poste)
    if db_poste is None:
        raise HTTPException(status_code=404, detail="poste not found")
    return db_poste

@route.post("/Poste/create", response_model=bpschemas.CreatePoste, description= "creation poste")
def create_poste(my_poste:bpschemas.CreatePoste):    
    return bpcrud.create_poste(my_poste)

@route.put("/Poste/update/{id_poste}",response_model=bpschemas.UpdatePoste, description= "modification poste")
def update_poste(id_poste:int, updates:dict):
    db_poste = read_poste(id_poste)
    if not db_poste:
        raise HTTPException(status_code=400, detail="poste not found")
    return bpcrud.update_poste(id_poste=id_poste,my_updates=updates)

@route.delete("/Poste/delete/{id_poste}", description= "modification d'une absence")
def delete_poste(id_poste: int):
    db_poste = bpcrud.get_poste(id_poste)
    if not db_poste:
        raise HTTPException(status_code=400, detail="ce poste n'existe pas") 

    return bpcrud.delete_poste(id_poste)


####################################equipement_poste

@route.get("/EquipementPoste/get/{id_poste}", response_model=bpschemas.GetEquipementPoste, description= "affichage equipement poste")
def read_equipement_poste(id_poste: int):
    db_equipement_poste = bpcrud.get_equipement_poste(id_poste)
    if db_equipement_poste is None:
        raise HTTPException(status_code=404, detail="Equipement Poste not found")
    return db_equipement_poste

@route.post("/EquipementPoste/create/", response_model=bpschemas.GetEquipementPoste, description= "creation equipement poste")
def create_equipement_poste(equipement_poste: bpschemas.CreateEquipementPoste):
    db_equipement_poste = bpcrud.create_equipement_poste(equipement_poste)
    if db_equipement_poste is None:
        raise HTTPException(status_code=400, detail="already registered")
    return db_equipement_poste 

@route.put("/EquipementPoste/update/{id_poste}", response_model=bpschemas.GetEquipementPoste, description= "modification equipement poste")
def update_equipement_poste(id_poste: int, updates: dict):
    db_equipement_poste = bpcrud.get_equipement_poste(id_poste)
    if db_equipement_poste is None:
        raise HTTPException(status_code=404, detail="AudimetreInstalle not found")
    return bpcrud.update_equipement_poste(id_poste=id_poste,my_updates= updates)

# @route.delete("/EquipementPoste/delete/{id_poste}", response_model=bpschemas.DeleteEquipementPoste)
# def del_equipement_poste(id_poste: int):
#     db_equipement_poste = bpcrud.get_equipement_poste(db, id_poste=id_poste)
#     if not db_equipement_poste:
#         raise HTTPException(status_code=400, detail="ce poste n'existe pas") 
#     return bpcrud.delete_equipement_poste(db, id_poste)


####################################reception

@route.get("/Reception/get/{id_recep}", response_model=bpschemas.GetReception, description= "affichage d'une reception")
def read_reception(id_recep: int):
    db_reception = bpcrud.get_reception(id_recep)
    if db_reception is None:
        raise HTTPException(status_code=404, detail="reception not found")
    return db_reception

@route.post("/Reception/create/", response_model=bpschemas.GetReception, description= "modification d'une reception")
def create_reception(reception: bpschemas.CreateReception):
    db_reception = bpcrud.create_reception(reception)
    if db_reception is None:
        raise HTTPException(status_code=400, detail="already registered")
    return db_reception 

@route.put("/Reception/update/{id_recep}", response_model=bpschemas.GetReception, description= "modification d'une reception")
def update_reception(id_recep: int, updates: dict):
    db_reception = bpcrud.get_reception(id_recep)
    if db_reception is None:
        raise HTTPException(status_code=404, detail="AudimetreInstalle not found")
    return bpcrud.update_reception(id_recep=id_recep,my_updates= updates)