from fastapi import APIRouter, FastAPI, HTTPException

from typing import List

from ...models import ModelCroisementModaliteAgeSexe as casmodels
from ...schemas import SchemaCroisementModaliteAgeSexe  as casschemas

from ..crud import CrudCroisementModaliteAgeSexe as cascrud





from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date,CheckConstraint,Computed, engine,text,UniqueConstraint,Index,Identity,Table,MetaData,select
from sqlalchemy_utils import create_view

route = APIRouter(prefix="/CroisementModaliteAgeSexe",tags=["CroisementModaliteAgeSexe"])




@route.get("/Croisement/get/{label}", response_model=casschemas.GetCroisementAgeSexe)
def read_croisement(label: str):
    db_croisement = cascrud.get_croisement(label)
    if db_croisement is None:
        raise HTTPException(status_code=404, detail="croisement not found")
    return db_croisement

@route.post("/Croisement/create", response_model=casschemas.GetCroisementAgeSexe)
def create_croisement(label: str):
    db_croisement = cascrud.get_croisement(label)
    if db_croisement:
        raise HTTPException(status_code=400, detail="ce croisementdeja existe ")      
    return cascrud.create_croisement(label)

@route.put("/Croisement/update/{id_croisement}",response_model=casschemas.GetCroisementAgeSexe)
def update_croisement(id_croisement:int, updates:str):
    # db_croisement = db.query(casmodels.CroisementAgeSexe).filter(casmodels.CroisementAgeSexe.id_croisement == id_croisement).first()  ### add a crud by id
    # if not db_croisement:
    #     raise HTTPException(status_code=400, detail="croisement not found")
    return cascrud.update_croisement(croisement=id_croisement,my_updates=updates)

@route.delete("/Croisement/delete/{id_croisement}")
def delete_croisement(id_croisement: int):
    # db_croisement =  db.query(casmodels.CroisementAgeSexe).filter(casmodels.CroisementAgeSexe.id_croisement == id_croisement).first() ### add a crud by id
    # if not db_croisement:
    #     raise HTTPException(status_code=400, detail="cette croisement  n'existe pas") 
    return cascrud.delete_croisement(id_croisement)

####################################modalite

@route.get("/Modalite/get/{croisement}", response_model=List[casschemas.GetModaliteAgeSexe])
def read_modalite(croisement: int):
    db_modalite = cascrud.get_modalite(croisement)
    if db_modalite is None:
        raise HTTPException(status_code=404, detail="modalite not found")
    return db_modalite

@route.post("/Modalite/create", response_model=casschemas.GetModaliteAgeSexe)
def create_modalite(my_modalite:casschemas.CreateModaliteAgeSexe,):    
    return cascrud.create_modalite(my_modalite)


@route.put("/Modalite/update/{id_mod}",response_model=casschemas.GetModaliteAgeSexe)
def update_modalite(id_mod:int, updates:dict):
    # db_modalite = db.query(casmodels.ModaliteAgeSexe).filter(casmodels.ModaliteAgeSexe.id_modalite == id_mod).first() ### add a crud by id
    # if not db_modalite:
    #     raise HTTPException(status_code=400, detail="cible not found")
    return cascrud.update_modalite(id_mod=id_mod,my_updates=updates)

@route.delete("/Modalite/delete/{id_mod}")
def delete_modalite(id_mod: int):
    # modalite = db.query(casmodels.ModaliteAgeSexe).filter(casmodels.ModaliteAgeSexe.id_modalite == id_mod).first() ### add a crud by id
    # if not modalite :
    #     raise HTTPException(status_code=400, detail="cette modalite  n'existe pas") 

    return cascrud.delete_modalite(id_mod)


###################################################mtranche_view###############################################
@route.get("/View/get/{id_indiv}")
def read_modalite_by_indiv(id_indiv: int):
    return cascrud.get_modalite_by_indiv(id_indiv)



# @app.post("View/test_create") 
# def new_view():
#     tranche_test = select(casmodels.CroisementAgeSexe.id_croisement_seq)
#     create_view('tranche_view_test', selectable=tranche_test, metadata=meta, cascade_on_drop=True)