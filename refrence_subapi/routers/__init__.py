


def include_route(app):
    app.include_router(RouterBaseFoyer.route)
    app.include_router(RouterBaseIndividu.route)
    app.include_router(RouterBasePoste.route)
    app.include_router(RouterBaseAudimetre.route)
    app.include_router(RouterCroisementModaliteAgeSexe.route)
    app.include_router(RouterChefMenage.route)
    app.include_router(RouterBaseCommune.route)
    
