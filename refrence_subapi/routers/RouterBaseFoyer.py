from fastapi import APIRouter, FastAPI, HTTPException

from typing import List

from ...models import ModelBaseFoyer as bfmodels
from ...schemas import SchemaBaseFoyer  as bfschemas

from ..crud import CrudBaseFoyer as bfcrud
from ..crud import CrudBaseIndividu as bicrud






route= APIRouter(prefix="/BaseFoyer",tags=["BaseFoyer"])




######################################################################foyer###############################################################

@route.get("/Foyer/get/", response_model=bfschemas.GetFoyer)
def read_foyer(id_foyer: int):
    db_foyer = bfcrud.get_foy(id_foyer)
    if db_foyer is None:
        raise HTTPException(status_code=404, detail="foyer not routeund")
    return db_foyer


@route.post("/Foyer/create", response_model=bfschemas.CreateFoyer)
def create_foyer(my_foyer: bfschemas.CreateFoyer):
    db_foyer = bfcrud.get_foy_medoiamat(id_mediamat=my_foyer.f_id_mediamat)
    if db_foyer:
        raise HTTPException(status_code=400, detail="foyer deja existant")
    return bfcrud.create_foy(my_foyer)
    


@route.post("/Foyer/create/random", response_model=bfschemas.CreateFoyer,description= "creation d'un foyer aleatoirement ")
def create_random_foyer(id_mediamat: int):  
    db_foyer = bfcrud.get_foy_medoiamat(id_mediamat=id_mediamat)
    if db_foyer:
        raise HTTPException(status_code=400, detail="foyer deja existant") 
    return bfcrud.random_foy(id_mediamat)
      
@route.post("/Foyer/Random/create/with_indiv",description= "creation d'un foyer et ces indivudus aleatoirement ")
def random_foyer_with_individus(id_mediamat: int,nbr: int ):
    db_foyer = bfcrud.get_foy_medoiamat(id_mediamat=id_mediamat)
    if db_foyer:
        raise HTTPException(status_code=400, detail="foyer deja existant")
    return bicrud.random_foy_indiv(id_mediamat=id_mediamat, nbr=nbr)



@route.delete("/Foyer/delete/{id_foyer}",description= "suppression des données d'un foyer apartir de l'idetifiant")
def delete_foyer(id_foyer: int):
    db_foyer = bfcrud.get_foy(id_foyer)
    if not db_foyer:
        raise HTTPException(status_code=400, detail="ce foyer n'existe pas") 

    return bfcrud.delete_foy(id_foyer)

@route.put("/Foyer/update/{id_foyer}",response_model=bfschemas.UpdateFoyer,description= "modification  des données d'un foyer ")
def update_foyer(id_foyer:int, updates:dict):

    db_foyer = bfcrud.get_foy(id_foyer)
    if not db_foyer:
        raise HTTPException(status_code=400, detail="foyer not found")
    return bfcrud.update_foy(id_foy=id_foyer,my_updates=updates)   
    

#############contact_foyer

@route.get("/Contact/get/{id_foyer}", response_model=bfschemas.GetContactFoyer,description= "lecture des données de la table contact_foyer ")
def read_contact_foyer(id_foyer: int):
    db_contact_foyer = bfcrud.get_contact_foy(id_foyer)
    if db_contact_foyer is None:
        raise HTTPException(status_code=404, detail="foyer not routeund")
    return db_contact_foyer

@route.post("/Contact/create", response_model=bfschemas.CreateContactFoyer,description= "insertion des données de la table contact_foyer ")
def create_contact_foyer(my_contact_foyer: bfschemas.CreateContactFoyer):
    db_contact_foyer = bfcrud.get_contact_foy(my_contact_foyer.f_num_con)
    if db_contact_foyer:
        raise HTTPException(status_code=400, detail="foyer deja existant")  
    db_foyer = bfcrud.get_foy(my_contact_foyer.f_num_con)
    if not db_foyer:
        raise HTTPException(status_code=400, detail="ce foyer n'existe pas")     
    return bfcrud.create_contact_foy(my_contact_foyer)
      


@route.put("/Contact/update/{id_foyer}",response_model=bfschemas.UpdateContactFoyer,description= "modification des données de la table contact_foyer ")
def update_contact_foyer(id_foyer:int, updates:dict):
    db_contact_foyer = bfcrud.get_contact_foy(id_foy=id_foyer)
    if not db_contact_foyer:
        raise HTTPException(status_code=400, detail="foyer not routeund")
    return bfcrud.update_contact_foy(id_foy=id_foyer, my_updates=updates) 
      


@route.delete("/Contact/delete/{id_foyer}",description= "suppression des données de la table contact_foyer ")
def delete_contact_foyer(id_foyer: int):
    db_contact_foyer = bfcrud.get_contact_foy(id_foyer)
    if not db_contact_foyer:
        raise HTTPException(status_code=400, detail="ce foyer n'existe pas") 
    return bfcrud.delete_contact_foy(id_foyer)

#############effectif_foyer  
  
@route.get("/Effectif/get/{id_foyer}", response_model=bfschemas.GetEffectifFoyer,description= "lecture des données de la table effectif_foyer ")
def read_effectif_foyer(id_foyer: int):
    db_effectif_foyer = bfcrud.get_effectif_foy(id_foyer)
    if db_effectif_foyer is None:
        raise HTTPException(status_code=404, detail="foyer not routeund")
    return db_effectif_foyer

@route.post("/Effectif/create/{id_foyer}", response_model=bfschemas.GetEffectifFoyer,description= "insertion des données de la table effectif_foyer (toute les variables de cette tables sont calculées il suffit d'inserer l'identifiant du foyer) ")
def create_effectif_foyer(id_foyer:int):
    db_effectif_foyer = bfcrud.get_effectif_foy(id_foyer)
    if db_effectif_foyer:
        raise HTTPException(status_code=400, detail="foyer deja existant")  
    return bfcrud.create_effectif_foy(id_foyer)

#############_equipement

@route.get("/Equipement/get/{id_foyer}", response_model=bfschemas.GetEquipement,description= "lecture des données de la table equipement_foyer ")
def read_equipement(id_foyer: int):
    db_equipement= bfcrud.get_equipement(id_foyer)
    if db_equipement is None:
        raise HTTPException(status_code=404, detail="foyer not found")
    return db_equipement   

@route.post("/Equipement/create", response_model=bfschemas.CreateEquipement,description= "insertion des données de la table equipement_foyer ")
def create_equipement(my_equipement: bfschemas.CreateEquipement):
    db_equipement = bfcrud.get_equipement(my_equipement.f_num_con)
    if db_equipement:
        raise HTTPException(status_code=400, detail="foyer deja existant")  
    return bfcrud.create_equipement(my_equipement)
      
@route.put("/Equipement/update/{id_foyer}",response_model=bfschemas.UpdateEquipement,description= "modification  des données de la table equipement_foyer")
def update_equipement(id_foyer:int, updates:dict):
    db_equipement = bfcrud.get_equipement(id_foyer)
    if not db_equipement:
        raise HTTPException(status_code=400, detail="foyer not found")
    return bfcrud.update_equipement(id_foy=id_foyer, my_updates=updates)       

##############sociodemo


@route.get("/Sociodemo/get/{id_foyer}", response_model=bfschemas.GetSociodemo,description= "lecture des données de la table sociodemo")
def read_sociodemo(id_foyer: int):
    db_sociodemo= bfcrud.get_sociodemo(id_foyer)
    if db_sociodemo is None:
        raise HTTPException(status_code=404, detail="foyer not found")
    return db_sociodemo   

@route.post("/Sociodemo/create", response_model=bfschemas.GetSociodemo,description= "insertion  des données de la table sociodemo")
def create_sociodemo(my_sociodemo: bfschemas.CreateSociodemo):
    db_sociodemo = bfcrud.get_sociodemo(my_sociodemo.f_num_con)
    if db_sociodemo:
        raise HTTPException(status_code=400, detail="foyer deja existant")  
    db_foyer = bfcrud.get_foy(my_sociodemo.f_num_con)
    if not db_foyer:
        raise HTTPException(status_code=400, detail="ce foyer n'existe pas") 
    return bfcrud.create_sociodemo(my_sociodemo)

@route.put("/Sociodemo/update/{id_foyer}",response_model=bfschemas.UpdateSociodemo,description= "modification  des données de la table sociodemo ")
def update_sociodemo(id_foyer:int, updates:dict):
    db_sociodemo= bfcrud.get_sociodemo(id_foyer)
    if not db_sociodemo:
        raise HTTPException(status_code=400, detail="foyer not found")
    return bfcrud.update_sociodemo(id_foy=id_foyer, my_updates=updates)  
    
##############tel 
# @route.get("/Tel/get_all/{id_foyer}", response_model=List[bfschemas.GetTel],description= "lecture de tout le telephones d'un foyer")
# def read_all_tel(id_foyer: int):
#     db_foyer= bfcrud.get_foy(id_foyer)
#     if db_foyer is None:
#         raise HTTPException(status_code=404, detail="foyer not found")
#     return bfcrud.get_tel_foy(id_foyer)

# @route.get("/Tel/get/{id_tel}", response_model=bfschemas.GetTel,description= "lecture d'un telephone ")
# def read_tel(id_tel: int):
#     db_tel= bfcrud.get_tel(id_tel)
#     if db_tel is None:
#         raise HTTPException(status_code=404, detail="tel not found")
#     return bfcrud.get_tel(id_tel)

# @route.post("/Tel/create", response_model=bfschemas.GetTel,description= "insertion d'un numero de telephone")
# def tel(my_tel: bfschemas.CreateTel):
#     return bfcrud.create_tel(my_tel)

# @route.put("/Tel/update/{id_tel}",response_model=bfschemas.UpdateTel,description= "modification  d'un numero de telephone ")
# def update_tel(id_tel:int, updates:str):
    
#     return bfcrud.update_tel(tel=id_tel, my_updates=updates)

# @route.delete("/Tel/delete/{id_tel}",description= "suppression d'un numero de telephone ")
# def delete_tel(id_tel: int):
#     db_tel = bfcrud.get_tel(id_tel)
#     if not db_tel:
#         raise HTTPException(status_code=400, detail="ce telephone n'existe pas") 

#     return bfcrud.delete_tel(id_tel)
