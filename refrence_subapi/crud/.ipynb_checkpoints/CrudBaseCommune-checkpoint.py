from sqlalchemy.orm import Session
from sqlalchemy import delete


from ..models import ModelBaseCommune as mod
from ..schemas import SchemaBaseCommune as schem

#############################################################commune#####################################################################################
def get_commune(db: Session, id_commune: int):
    return db.query(mod.commune).filter(mod.commune.id_commune == id_commune).first()


def create_commune(db: Session, my_commune:schem.CreateCommune):
    db_commune = mod.commune(id_commune =my_commune.id_commune,
    nom_commune =my_commune.nom_commune,
    nom_zone =my_commune.nom_zone,
    num_zone =my_commune.num_zone,
    taille_commune =my_commune.taille_commune,
    milieu =my_commune.milieu)
    db.add(db_commune)
    db.commit()
    db.refresh(db_commune)
    return db_commune


def update_commune(db: Session,id_commune : int, my_updates:dict):
    stored_commune = db.query(mod.commune).filter(mod.commune.id_commune == id_commune).first()
    for key, value in my_updates.items():
         setattr(stored_commune, key, value)
    db.commit()
    db.refresh(stored_commune)
    return  stored_commune


def delete_commune(db: Session, id_commune: int):
    del_commune=get_commune(db=db ,id_commune=id_commune)
    db.delete(del_commune)
    db.commit()
    return " commune  deleted"

#############################################################ville#####################################################################################
def get_ville(db: Session, id_ville: int):
    return db.query(mod.ville).filter(mod.ville.id_ville == id_ville).first()


def create_ville(db: Session, my_ville:schem.Createville):
    db_ville = mod.ville(
    id_ville =my_ville.id_ville,
    ville =my_ville.ville,
    id_commune =my_ville.id_commune)
    db.add(db_ville)
    db.commit()
    db.refresh(db_ville)
    return db_ville


def update_ville(db: Session,id_ville : int, my_updates:dict):
    stored_ville = db.query(mod.ville).filter(mod.ville.id_ville == id_ville).first()
    for key, value in my_updates.items():
         setattr(stored_ville, key, value)
    db.commit()
    db.refresh(stored_ville)
    return  stored_ville


def delete_ville(db: Session, id_ville: int):
    del_ville=get_ville(db=db ,id_ville=id_ville)
    db.delete(del_ville)
    db.commit()
    return " ville  deleted"
    
    
#############################################################region#####################################################################################
def get_region(db: Session, id_region: int):
    return db.query(mod.region).filter(mod.region.id_region == id_region).first()


def create_region(db: Session, my_region:schem.Createregion):
    db_region = mod.region(
    id_region =my_region.id_region,
    region =my_region.region,
    region_hab_pol =my_region.region_hab_pol,
    id_commune =my_region.id_commune)
    db.add(db_region)
    db.commit()
    db.refresh(db_region)
    return db_region


def update_region(db: Session,id_region : int, my_updates:dict):
    stored_region = db.query(mod.region).filter(mod.region.id_region == id_region).first()
    for key, value in my_updates.items():
         setattr(stored_region, key, value)
    db.commit()
    db.refresh(stored_region)
    return  stored_region


def delete_region(db: Session, id_region: int):
    del_region=get_region(db=db ,id_region=id_region)
    db.delete(del_region)
    db.commit()
    return " region  deleted"