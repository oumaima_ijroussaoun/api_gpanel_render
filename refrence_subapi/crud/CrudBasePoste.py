from sqlalchemy.orm import Session

from ...models import ModelBasePoste as mod
from ...schemas import SchemaBasePoste as schem

from . import get_db

################################################################poste#########################################################################


def get_poste(id_poste: int,db: Session = next(get_db())):
    return db.query(mod.Poste).filter(mod.Poste.id_poste == id_poste).first() 

def get_postes(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.Poste).filter(mod.Poste.f_num_con == id_foy).all() 

def create_poste(my_poste: schem.CreatePoste,db: Session = next(get_db())):
    db_poste = mod.Poste(id_poste=my_poste.id_poste,
    num_poste=my_poste.num_poste,
    etat_actuel=my_poste.etat_actuel,
    marque_tv =my_poste.marque_tv,
    model_tv=my_poste.model_tv,
    tv_non_installe =my_poste.tv_non_installe,
    motif_tv_non_installe=my_poste.motif_tv_non_installe,
    dimension_tv=my_poste.dimension_tv,
    type_ecran=my_poste.type_ecran,
    taille_ecran=my_poste.taille_ecran,
    anciennete_tv=my_poste.anciennete_tv,
    statut_poste=my_poste.statut_poste,
    mode_trait_poste=my_poste.mode_trait_poste,
    tv_online=my_poste.tv_online,
    f_num_con =my_poste.f_num_con,
    emplacement=my_poste.emplacement,
    date_installation_poste =my_poste.date_installation_poste)
    db.add(db_poste)
    db.commit()
    db.refresh(db_poste)
    return db_poste

def update_poste(id_poste: int, my_updates:dict,db: Session = next(get_db())):
    stored_poste = db.query(mod.Poste).filter(mod.Poste.id_poste == id_poste).first()
    for key, value in my_updates.items():
         setattr(stored_poste, key, value)
    db.commit()
    db.refresh(stored_poste)
    return  stored_poste

def delete_poste(id_poste: int,db: Session = next(get_db())):
    del_poste=get_poste(db=db ,id_poste=id_poste)
    db.delete(del_poste)
    db.commit()
    return " poste deleted"


####################################equipement_poste

def get_equipement_poste(id_poste: int,db: Session = next(get_db())):
    return db.query(mod.EquipementPoste).filter(mod.EquipementPoste.id_poste == id_poste).first() 

def create_equipement_poste(my_equip: schem.CreateEquipementPoste,db: Session = next(get_db())):
    db_equipement_poste = mod.EquipementPoste(id_poste=my_equip.id_poste,
    decodeur_sat_num=my_equip.decodeur_sat_num,
    decodeur_adsl=my_equip.decodeur_adsl,
    home_cinema=my_equip.home_cinema,
    antenne_parab=my_equip.antenne_parab,
    type_antenne_parab=my_equip.type_antenne_parab,
    reception_adsl=my_equip.reception_adsl,
    type_sonde_poste=my_equip.type_sonde_poste,
    equipe_tnt=my_equip.equipe_tnt)
    db.add(db_equipement_poste)
    db.commit()
    db.refresh(db_equipement_poste)
    return db_equipement_poste

def update_equipement_poste(id_poste: int, my_updates:dict,db: Session = next(get_db())):
    stored_equip = db.query(mod.EquipementPoste).filter(mod.EquipementPoste.id_poste == id_poste).first()
    for key, value in my_updates.items():
         setattr(stored_equip, key, value)
    db.commit()
    db.refresh(stored_equip)
    return  stored_equip        



####################################reception

def get_reception(id_poste: int,db: Session = next(get_db())):
    return db.query(mod.Reception).filter(mod.Reception.id_poste == id_poste).first() 

def create_reception(my_recep: schem.CreateReception,db: Session = next(get_db())):
    db_reception = mod.Reception(id_poste=my_recep.id_poste,type_reception=my_recep.type_reception,reception_chaine=my_recep.reception_chaine,reception_sat=my_recep.reception_sat)
    db.add(db_reception)
    db.commit()
    db.refresh(db_reception)
    return db_reception

def update_reception(id_poste: int, my_updates:dict,db: Session = next(get_db())):
    stored_recep = db.query(mod.EquipementPoste).filter(mod.Reception.id_poste == id_poste).first()
    for key, value in my_updates.items():
         setattr(stored_recep, key, value)
    db.commit()
    db.refresh(stored_recep)
    return  stored_recep    