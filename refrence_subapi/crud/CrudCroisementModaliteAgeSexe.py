from sqlalchemy.orm import Session


from ...models import ModelCroisementModaliteAgeSexe as mod
from ...schemas import SchemaCroisementModaliteAgeSexe as schem

from . import get_db

################################################################croisement_age_sexe#########################################################################
def get_croisement(label: str,db: Session = next(get_db())):
    return db.query(mod.CroisementAgeSexe).filter(mod.CroisementAgeSexe.label_crois == label).first()

def create_croisement(label: str,db: Session = next(get_db())):
    db_croisement= mod.CroisementAgeSexe(label_crois=label)
    db.add(db_croisement)
    db.commit()
    db.refresh(db_croisement)
    return db_croisement   

def update_croisement(croisement: int, my_updates:str,db: Session = next(get_db())):
    stored_croisement = db.query(mod.CroisementAgeSexe).filter(mod.CroisementAgeSexe.id_croisement==croisement).first()
    setattr(stored_croisement, 'label_croisement', my_updates)
    db.commit()
    db.refresh(stored_croisement)
    return  stored_croisement

def delete_croisement(id_croisement: int,db: Session = next(get_db())):
    del_croisement=db.query(mod.CroisementAgeSexe).filter(mod.CroisementAgeSexe.id_croisement == id_croisement).first()
    db.delete(del_croisement)
    db.commit()
    return f" {del_croisement.label_croisement}  deleted"    


################################################################modalite_age_sexe#########################################################################
def get_modalite(croisement: int,db: Session = next(get_db())):
    return db.query(mod.ModaliteAgeSexe).filter(mod.ModaliteAgeSexe.id_croisement_seq == croisement).all()

def create_modalite(my_modalite:schem.CreateModaliteAgeSexe,db: Session = next(get_db())):
    
    db_modalite= mod.ModaliteAgeSexe(age_max =my_modalite.age_max,
    age_min=my_modalite.age_min,
    sexe=my_modalite.sexe,
    num_mod_crois=my_modalite.num_mod_crois,
    id_crois_seq=my_modalite.id_crois_seq)
    db.add(db_modalite)
    db.commit()
    db.refresh(db_modalite)
    return db_modalite   

def update_modalite(id_mod: int, my_updates:dict,db: Session = next(get_db())):
    stored_modalite = db.query(mod.ModaliteAgeSexe).filter(mod.ModaliteAgeSexe.id_modalite == id_mod).first()
    for key, value in my_updates.items():
         setattr(stored_modalite, key, value)
    db.commit()
    db.refresh(stored_modalite)
    return  stored_modalite

def delete_modalite(id_mod: int,db: Session = next(get_db())):
    del_modalite=db.query(mod.ModaliteAgeSexe).filter(mod.ModaliteAgeSexe.id_modalite == id_mod).first()
    db.delete(del_modalite)
    db.commit()
    return f" {del_modalite.id_modalite}  deleted"    

###################################################tranche_view##########################################
def get_modalite_by_indiv(id_indiv: int,db: Session = next(get_db())):
    return db.query(mod.TrancheView).filter(mod.TrancheView.id_indiv == id_indiv).all()
