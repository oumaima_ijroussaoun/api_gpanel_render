from sqlalchemy.orm import Session

from ...models import ModelChefMenage as mod
from ...schemas import SchemaChefMenage as schem

from . import get_db

################################################################chef_menage#########################################################################
def get_chef_menage(id_ind: int,db: Session = next(get_db())):
    return db.query(mod.ChefMenage).filter(mod.ChefMenage.id_indiv == id_ind).first()

def create_chef_menage(my_chef_menage: schem.CreateChefMenage,db: Session = next(get_db())):
    db_chef_menage = mod.ChefMenage(id_indiv=my_chef_menage.id_indiv,
    profession_cm =my_chef_menage.profession_cm,
    csp_cm =my_chef_menage.csp_cm)
    db.add(db_chef_menage)
    db.commit()
    db.refresh(db_chef_menage)
    return db_chef_menage   

def update_chef_menage(id_ind: int, my_updates:dict,db: Session = next(get_db())):
    stored_chef_menage = db.query(mod.ChefMenage).filter(mod.ChefMenage.id_indiv == id_ind).first()
    for key, value in my_updates.items():
         setattr(stored_chef_menage, key, value)
    db.commit()
    db.refresh(stored_chef_menage)
    return  stored_chef_menage