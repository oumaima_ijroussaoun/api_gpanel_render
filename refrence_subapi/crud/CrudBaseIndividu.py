from sqlalchemy.orm import Session
from datetime import datetime
import random
import string

from sqlalchemy.sql.expression import null

from ...models import ModelBaseIndividu as mod
from ...schemas import SchemaBaseIndividu as schem
# from .CrudBaseFoyer import  random_foy

from . import get_db

###############################################individu###################################################################################

def create_indiv(my_ind: schem.CreateIndividu,db: Session = next(get_db())):
	
    try:
    		check=db.query(mod.Individu.id_indiv).filter(mod.Individu.id_indiv == my_ind.id_indiv).first()
    		if(check!=null):
    				db_individu = mod.Individu(id_indiv = my_ind.id_indiv+1, nom = my_ind.nom , prenom = my_ind.prenom ,
        date_naiss = my_ind.date_naiss,telephones_indiv=my_ind.telephones_indiv,
        touche_indiv = my_ind.touche_indiv, niveau_instruc_glob = my_ind.niveau_instruc_glob,

        categorie_indiv =my_ind.categorie_indiv,
        sexe =my_ind.sexe,
        etat =my_ind.etat,
        statut_indiv=my_ind.statut_indiv,
    
        categorie_inact =my_ind.categorie_inact,
        activite = my_ind.activite,
        enfant_touche_mere =my_ind.enfant_touche_mere,
        is_cm = my_ind.is_cm,
        is_menagere = my_ind.is_menagere,
        is_maman = my_ind.is_maman,
        is_epouse = my_ind.is_epouse,
        is_aide_domestique = my_ind.is_aide_domestique,
        voyage_etrg=my_ind.voyage_etrg,
        but_voy =my_ind.but_voy,
        f_num_con = my_ind.f_num_con,
        mode_trait_indiv=my_ind.mode_trait_indiv)
    				db.add(db_individu)
    		else:
        	 		db_individu = mod.Individu(id_indiv = my_ind.id_indiv, nom = my_ind.nom , prenom = my_ind.prenom ,date_naiss = my_ind.date_naiss,telephones_indiv=my_ind.telephones_indiv,
        touche_indiv = my_ind.touche_indiv, niveau_instruc_glob = my_ind.niveau_instruc_glob,

        categorie_indiv =my_ind.categorie_indiv,
        sexe =my_ind.sexe,
        etat =my_ind.etat,
        statut_indiv=my_ind.statut_indiv,
    
        categorie_inact =my_ind.categorie_inact,
        activite = my_ind.activite,
        enfant_touche_mere =my_ind.enfant_touche_mere,
        is_cm = my_ind.is_cm,
        is_menagere = my_ind.is_menagere,
        is_maman = my_ind.is_maman,
        is_epouse = my_ind.is_epouse,
        is_aide_domestique = my_ind.is_aide_domestique,
        voyage_etrg=my_ind.voyage_etrg,
        but_voy =my_ind.but_voy,
        f_num_con = my_ind.f_num_con,
        mode_trait_indiv=my_ind.mode_trait_indiv)
        	 		db.add(db_individu)        	 
    except:
        db.rollback()
        db.close()
        
        raise 
    else:        
        db.commit()
        # db.refresh(db_individu)
       
        return db_individu

def delete_indiv(id_ind: int,db: Session = next(get_db())):
    del_ind=get_indiv(db=db ,id_ind=id_ind)
    db.delete(del_ind)
    db.commit()
    db.close()
    return f"l'individu {del_ind.nom,del_ind.prenom}  a etait supprime"

def get_all_indiv_foy(id_foy: int,db: Session = next(get_db()) ):
    return db.query(mod.Individu).filter(mod.Individu.f_num_con == id_foy).all()

def get_indiv(id_ind: int,db: Session =  next(get_db())):
    return db.query(mod.Individu).filter(mod.Individu.id_indiv == id_ind).first()

# def random_indiv(id_foy:int,nb:int,db: Session = next(get_db())):
#     db_individu = mod.Individu(id_indiv = nb+id_foy+1000, nom = ''.join(random.choice(string.ascii_lowercase ) for i in range(10)) , prenom = ''.join(random.choice(string.ascii_lowercase ) for i in range(10)) ,
#     date_naiss = datetime(1990, 5, 17), touche_indiv = nb+1, niveau_instruc_glob = random.randrange(1, 2),
#     niveau_instruc =random.randrange(1, 2),
#     categorie_indiv =random.randrange(1, 2),
#     sexe =random.randrange(1, 2),
#     etat =1,
#     categorie_inact = None,
#     activite = 1,
#     enfant_touche_mere = None,
#     is_cm = False,
#     is_menagere = False,
#     is_maman = False,
#     is_epouse = False,
#     is_aide_domestique = False,
#     voyage_etrg=0,
    
#     f_num_con = id_foy,
#     mode_trait_indiv=1)
#     db.add(db_individu)
#     db.commit()
#     db.refresh(db_individu)
#     return db_individu

# def random_foy_indiv(id_mediamat:int,nbr:int,db: Session = next(get_db())):
#       random_foy(db=db, id_mediamat=id_mediamat)
#       for i in range(nbr):
#           random_indiv(db=db,id_foy=id_mediamat*100,nb=i)

def update_indiv(id_ind: int, my_updates:dict,db: Session = next(get_db())):
    try:
        stored_individu = db.query(mod.Individu).filter(mod.Individu.id_indiv == id_ind).first()
        for key, value in my_updates.items():
            setattr(stored_individu, key, value)
    except:
        db.rollback()
        db.close()
        raise
    else:        
        db.commit()
        db.refresh(stored_individu)
        return  stored_individu


def update_indiv_id_foy(id_foy: int, my_updates:dict,db: Session = next(get_db())):
    stored_individu = get_all_indiv_foy(id_foy,db=db)
    for ind in stored_individu:
        for key, value in my_updates.items():
            setattr(ind, key, value)
            
    
    db.commit()
    
    return  get_all_indiv_foy(id_foy,db=db)

####################################abs_indiv

def get_abs_indiv(id_ind: int,db: Session = next(get_db())):
    return db.query(mod.AbsIndiv).filter(mod.AbsIndiv.id_indiv == id_ind).all()

def get_abs(id_abs: int,db: Session = next(get_db())):
    return db.query(mod.AbsIndiv).filter(mod.AbsIndiv.id_abs == id_abs).first()

def create_abs_indiv(my_abs_ind: schem.CreatAbsIndividu,db: Session = next(get_db())):
    db_abs_indiv = mod.AbsIndiv( id_indiv=my_abs_ind.id_indiv,
    date_debut_abs=my_abs_ind.date_debut_abs,
    date_fin_abs =my_abs_ind.date_fin_abs,
    motif_abs =my_abs_ind.motif_abs)
    db.add(db_abs_indiv)
    db.commit()
    db.refresh(db_abs_indiv)
    return db_abs_indiv

def update_abs_indiv(id_abs: int, my_updates:dict,db: Session = next(get_db())):
    stored_abs_indiv = db.query(mod.AbsIndiv).filter(mod.AbsIndiv.id_abs_seq == id_abs).first()
    for key, value in my_updates.items():
         setattr(stored_abs_indiv, key, value)
         
    db.commit()
    db.refresh(stored_abs_indiv)
    return  stored_abs_indiv

def delete_abs_indiv(id_ind: int,db: Session = next(get_db())):
    del_abs_indiv=get_abs_indiv(id_ind)
    db.delete(del_abs_indiv)
    db.commit()
    return f" {del_abs_indiv.id_ind}  deleted"    

 


