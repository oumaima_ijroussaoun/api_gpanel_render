from sqlalchemy.orm import Session

from ...models import ModelBaseAudimetre as bamodels
from ...schemas import SchemaBaseAudimetre as baschemas

from . import get_db

#####################################################################audimetre################################################################################
def get_audimetre( id_meter: str,db: Session=next(get_db())):
    return db.query(bamodels.Audimetre).filter(bamodels.Audimetre.id_meter == id_meter).first() 

def create_audimetre(my_meter: baschemas.CreateAudimetre,db: Session = next(get_db())):
    db_audimetre = bamodels.Audimetre( id_meter=my_meter.id_meter,
    statut_audimetre=my_meter.statut_audimetre,
    type_meter=my_meter.type_meter)
    db.add(db_audimetre)
    db.commit()
    db.refresh(db_audimetre)
    db.close()
    return db_audimetre

def update_audimetre(id_meter: str, my_updates:dict,db: Session = next(get_db())):
    stored_meter = db.query(bamodels.Audimetre).filter(bamodels.Audimetre.id_meter == id_meter).first()
    if (stored_meter.statut_meter==15):
    		for key, value in {"statut_meter": 15,"etat_meter": 2,"emplacement_meter": 1}.items():
    				setattr(stored_meter, key, value)
    else:
    		for key, value in my_updates.items():
    				setattr(stored_meter, key, value)
    db.commit()
    db.refresh(stored_meter)
    return  stored_meter        

def delete_audimetre( id_meter: str,db: Session = next(get_db())):
    del_meter=get_audimetre(db=db ,id_meter=id_meter)
    db.delete(del_meter)
    db.commit()
    return "meter deleted"

###########################################################################audimetre_installe##############################################################

def get_audimetre_installe( id_meter: str,db: Session = next(get_db())):
    return db.query(bamodels.AudimetreInstalle).filter(bamodels.AudimetreInstalle.id_meter == id_meter).first() 

def get_audimetre_installe_by_id_poste( id_poste: str,db: Session = next(get_db())):
    return db.query(bamodels.AudimetreInstalle).filter(bamodels.AudimetreInstalle.id_poste == id_poste).first() 

def create_audimetre_installe(my_meter: baschemas.CreateAudimetreInstalle,db: Session = next(get_db())):
    db_audimetre_installe = bamodels.AudimetreInstalle( id_meter =my_meter.id_meter,
    id_poste=my_meter.id_poste,
    nom_installateur=my_meter.nom_installateur,
    sortie_audio=my_meter.sortie_audio,
    capture_audio =my_meter.capture_audio)
    db.add(db_audimetre_installe)
    db.commit()
    db.refresh(db_audimetre_installe)
    return db_audimetre_installe

def update_audimetre_installe(id_meter: str, my_updates:dict,db: Session = next(get_db())):
    stored_meter = db.query(bamodels.AudimetreInstalle).filter(bamodels.AudimetreInstalle.id_meter == id_meter).first()
    for key, value in my_updates.items():
         setattr(stored_meter, key, value)
    db.commit()
    db.refresh(stored_meter)
    return  stored_meter        

def delete_audimetre_installe(id_meter: str,db: Session = next(get_db())):
    del_meter=get_audimetre_installe(db=db ,id_meter=id_meter)
    db.delete(del_meter)
    db.commit()
    db.close()
    return "meter deleted"
