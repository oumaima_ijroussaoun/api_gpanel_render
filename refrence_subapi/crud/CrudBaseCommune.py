from sqlalchemy.orm import Session

from ...models import ModelBaseCommune as mod
from ...schemas import SchemaBaseCommune as schem

from . import get_db

#############################################################commune#####################################################################################
def get_commune(id_commune: int,db: Session = next(get_db())):
    return db.query(mod.Commune).filter(mod.Commune.id_commune == id_commune).first()


def create_commune(my_commune:schem.CreateCommune,db: Session = next(get_db())):
    db_commune = mod.Commune(id_commune =my_commune.id_commune,
    nom_commune =my_commune.nom_commune,
    nom_zone =my_commune.nom_zone,
    num_zone =my_commune.num_zone,
    taille_commune =my_commune.taille_commune,
    milieu =my_commune.milieu)
    db.add(db_commune)
    db.commit()
    db.refresh(db_commune)
    return db_commune


def update_commune(id_commune : int, my_updates:dict,db: Session = next(get_db())):
    stored_commune = db.query(mod.Commune).filter(mod.Commune.id_commune == id_commune).first()
    for key, value in my_updates.items():
         setattr(stored_commune, key, value)
    db.commit()
    db.refresh(stored_commune)
    return  stored_commune


def delete_commune(id_commune: int,db: Session = next(get_db())):
    del_commune=get_commune(db=db ,id_commune=id_commune)
    db.delete(del_commune)
    db.commit()
    return " commune  deleted"

#############################################################ville#####################################################################################
def get_ville(id_ville: int,db: Session = next(get_db())):
    return db.query(mod.Ville).filter(mod.Ville.id_ville == id_ville).first()


def create_ville(my_ville:schem.CreateVille,db: Session = next(get_db())):
    db_ville = mod.Ville(
    id_ville =my_ville.id_ville,
    ville =my_ville.ville,
    id_commune =my_ville.id_commune)
    db.add(db_ville)
    db.commit()
    db.refresh(db_ville)
    return db_ville


def update_ville(id_ville : int, my_updates:dict,db: Session = next(get_db())):
    stored_ville = db.query(mod.Ville).filter(mod.Ville.id_ville == id_ville).first()
    for key, value in my_updates.items():
         setattr(stored_ville, key, value)
    db.commit()
    db.refresh(stored_ville)
    return  stored_ville


def delete_ville(id_ville: int,db: Session = next(get_db())):
    del_ville=get_ville(db=db ,id_ville=id_ville)
    db.delete(del_ville)
    db.commit()
    return " ville  deleted"
    
    
#############################################################region#####################################################################################
def get_region(id_region: int,db: Session = next(get_db())):
    return db.query(mod.Region).filter(mod.Region.id_region == id_region).first()


def create_region(my_region:schem.CreateRegion,db: Session = next(get_db())):
    db_region = mod.Region(
    id_region =my_region.id_region,
    region =my_region.region,
    region_administrative =my_region.region_administrative,
    id_commune =my_region.id_commune)
    db.add(db_region)
    db.commit()
    db.refresh(db_region)
    return db_region


def update_region(id_region : int, my_updates:dict,db: Session = next(get_db())):
    stored_region = db.query(mod.Region).filter(mod.Region.id_region == id_region).first()
    for key, value in my_updates.items():
         setattr(stored_region, key, value)
    db.commit()
    db.refresh(stored_region)
    return  stored_region


def delete_region(id_region: int,db: Session = next(get_db())):
    del_region=get_region(db=db ,id_region=id_region)
    db.delete(del_region)
    db.commit()
    return " region  deleted"



