from sqlalchemy.orm import Session
import string
import random
from datetime import date

from ...models import ModelBaseFoyer as mod
from ...schemas import SchemaBaseFoyer as schem

from . import get_db

######################################################foyer################################################################################

def get_foy(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.Foyer).filter(mod.Foyer.f_num_con == id_foy).first()

def get_foy_medoiamat(id_mediamat: int,db: Session = next(get_db())):
    return db.query(mod.Foyer).filter(mod.Foyer.f_id_mediamat == id_mediamat).first()

# def random_foy(id_mediamat: int,db: Session = next(get_db())):
#     db_foyer = mod.Foyer(f_id_mediamat=id_mediamat, f_nom=''.join(random.choice(string.ascii_lowercase ) for i in range(10)),
#      f_etat=random.randrange(1, 4), f_csp=random.randrange(1, 4), num_recrut=random.randrange(100, 400), date_recrut=date.today(),
#       vague_recrut=random.randrange(1, 4), f_date_deb=date.today(), mode_trait_f=random.randrange(1, 2),
#        nom_installateur=''.join(random.choice(string.ascii_lowercase ) for i in range(10)))
#     db.add(db_foyer)
#     db.commit()
#     db.refresh(db_foyer)
#     return db_foyer

def create_foy(my_foyer:schem.CreateFoyer,db: Session = next(get_db())):
    db_foyer = mod.Foyer(f_id_mediamat=my_foyer.f_id_mediamat, f_nom=my_foyer.f_nom,
     f_etat=my_foyer.f_etat, f_csp=my_foyer.f_csp, num_recrut=my_foyer.num_recrut, date_recrut=my_foyer.date_recrut,
      vague_recrut=my_foyer.vague_recrut, f_date_deb=my_foyer.f_date_deb, mode_trait_f=my_foyer.mode_trait_f,
       nom_installateur=my_foyer.nom_installateur,telephones=my_foyer.telephones,motivation=my_foyer.motivation)
    db.add(db_foyer)
    db.commit()
    db.refresh(db_foyer)
    db.close()
    return db_foyer

def delete_foy(id_foy: int,db: Session = next(get_db())):
    del_foy=get_foy(db=db ,id_foy=id_foy)
    db.delete(del_foy)
    db.commit()
    db.close()
    return f" foyer {del_foy.f_nom,del_foy.f_num_con}  deleted"



def update_foy(id_foy: int, my_updates:dict,db: Session = next(get_db())):
    stored_foyer = get_foy(db=db, id_foy=id_foy)
    for key, value in my_updates.items():
         setattr(stored_foyer, key, value)
    db.commit()
    db.refresh(stored_foyer)
    return  stored_foyer



def get_contact_foy(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.ContactFoyer).filter(mod.ContactFoyer.f_num_con == id_foy).first()

def create_contact_foy(my_contact_foyer:schem.CreateContactFoyer,db: Session = next(get_db())):
    db_contact_foyer = mod.ContactFoyer(f_num_con=my_contact_foyer.f_num_con,
    date_install_telfix =my_contact_foyer.date_install_telfix,
    f_adr1 =my_contact_foyer.f_adr1,
    longitude =my_contact_foyer.longitude,
    latitude =my_contact_foyer.latitude,
    id_commune = my_contact_foyer.id_commune,
    f_adr2 =my_contact_foyer.f_adr2)
    db.add(db_contact_foyer)
    db.commit()
    db.refresh(db_contact_foyer)
    db.close()
    return db_contact_foyer

def delete_contact_foy(id_foy: int,db: Session = next(get_db())):
    del_contact_foy=get_contact_foy(db=db ,id_foy=id_foy)
    db.delete(del_contact_foy)
    db.commit()
    db.close()
    return f"contact foyer {del_contact_foy.f_num_con}  deleted"

def update_contact_foy(id_foy: int, my_updates:dict,db: Session = next(get_db())):
    stored_contact_foyer = db.query(mod.ContactFoyer).filter(mod.ContactFoyer.f_num_con == id_foy).first()
    for key, value in my_updates.items():
         setattr(stored_contact_foyer, key, value)
    db.commit()
    db.refresh(stored_contact_foyer)
    return  stored_contact_foyer

################effectif foyer

def get_effectif_foy(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.EffectifFoyer).filter(mod.EffectifFoyer.f_num_con == id_foy).first()

def create_effectif_foy(id_foy: int,db: Session = next(get_db())):
    db_effectif_foyer = mod.EffectifFoyer(f_num_con=id_foy)
    db.add(db_effectif_foyer)
    db.commit()
    db.refresh(db_effectif_foyer)
    db.close()
    return db_effectif_foyer

#############equipement

def get_equipement(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.Equipement).filter(mod.Equipement.f_num_con == id_foy).first()

def create_equipement(my_equipement:schem.CreateEquipement,db: Session = next(get_db())):
    db_equipement = mod.Equipement(f_num_con = my_equipement.f_num_con,
    nbr_clim =my_equipement.nbr_clim,
    nbr_app_chauf =my_equipement.nbr_app_chauf,
    nbr_refrig =my_equipement.nbr_refrig,
    nbr_congel =my_equipement.nbr_congel,
    nbr_four_micro =my_equipement.nbr_four_micro,
    nbr_cuisiniere =my_equipement.nbr_cuisiniere,
    nbr_lave_linge =my_equipement.nbr_lave_linge,
    nbr_lave_vaiss =my_equipement.nbr_lave_vaiss,
    nbr_aspi =my_equipement.nbr_aspi,
    poss_internet =my_equipement.poss_internet,
    poss_tnt=my_equipement.poss_tnt,
    source_electricite =my_equipement.source_electricite,
    nbr_tv =my_equipement.nbr_tv,
    nbr_vehicule =my_equipement.nbr_vehicule,
    nbr_tel_mob =my_equipement.nbr_tel_mob,
    nbr_tel_fix =my_equipement.nbr_tel_fix,
    nbr_ordi =my_equipement.nbr_ordi,
    nbr_radio =my_equipement.nbr_radio,
    nbr_ordi_emp =my_equipement.nbr_ordi_emp,
    nbr_home_cinema =my_equipement.nbr_home_cinema,
    nbr_deco_sat =my_equipement.nbr_deco_sat,
    nbr_console_jeux_se =my_equipement.nbr_console_jeux_se,
    nbr_console_jeux_ae =my_equipement.nbr_console_jeux_ae,
    nbr_tablette= my_equipement.nbr_tablette)
    db.add(db_equipement)
    db.commit()
    db.refresh(db_equipement)
    db.close()
    return db_equipement

def update_equipement(id_foy: int, my_updates:dict,db: Session = next(get_db())):
    stored_equipement = db.query(mod.Equipement).filter(mod.Equipement.f_num_con == id_foy).first()
    for key, value in my_updates.items():
         setattr(stored_equipement, key, value)
    db.commit()
    db.refresh(stored_equipement)
    return  stored_equipement

#############sociodemo

def get_sociodemo(id_foy: int,db: Session = next(get_db())):
    return db.query(mod.Sociodemo).filter(mod.Sociodemo.f_num_con == id_foy).first()

def create_sociodemo(my_sociodemo:schem.CreateSociodemo,db: Session = next(get_db())):
    db_sociodemo = mod.Sociodemo(f_num_con=my_sociodemo.f_num_con,
    f_type_offre=my_sociodemo.f_type_offre,
    f_tranche_revenu=my_sociodemo.f_tranche_revenu,
    type_hab=my_sociodemo.type_hab,
    standing_hab=my_sociodemo.standing_hab,
    pos_res_sec=my_sociodemo.pos_res_sec,
    langue_prin=my_sociodemo.langue_prin,
    stand_vie=my_sociodemo.stand_vie)
    db.add(db_sociodemo)
    db.commit()
    db.refresh(db_sociodemo)
    db.close()
    return db_sociodemo

def update_sociodemo(id_foy: int, my_updates:dict,db: Session = next(get_db())):
    stored_sociodemo = db.query(mod.Sociodemo).filter(mod.Sociodemo.f_num_con == id_foy).first()
    for key, value in my_updates.items():
         setattr(stored_sociodemo, key, value)
    db.commit()
    db.refresh(stored_sociodemo)
    return  stored_sociodemo
